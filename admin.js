(function($, window, document, undefined) {
  var $win = $(window);

  var i = 0;
  $(document).on("click", "#add_more", function() {
    if (i == 0) {
      $(".tablehideone").show();
    }
    if (i == 1) {
      $(".tablehidetwo").show();
    }
    if (i == 2) {
      $(".tablehidethree").show();
    }
    if (i == 3) {
      $(".tablehidefour").show();
    }
    if (i == 4) {
      $(".tablehidefive").show();
    }
    if (i == 5) {
      $(".tablehidesix").show();
    }
    if (i == 6) {
      $(".tablehideseven").show();
      $("#add_more").hide();
    }
    i++;
  });

  $(document).ready(function() {
    [ 'datepicker', 'datepicker-day' ].forEach( function(val) {
      if ($("#" + val ).length > 0) {
        $("#" + val).datepicker({
          dateFormat: "yy-mm-dd",
          firstDay: 1
        });
      }
    });
  });
  // Function to call the backend and reload the week view when the date is select from datepicker
  $(document).on("change", "#datepicker", function(e) {
    var mon = $(this).datepicker("getDate");
    mon.setDate(mon.getDate() + 1 - (mon.getDay() || 7));
    var gotoWeek = $.datepicker.formatDate("yy-mm-dd", mon);

    // prevent nav
    e.preventDefault();

    // get some values
    var location = $("select[name='location']").val();
    var className = $("select[name='class-name']").val();
    var age_group = $("select[name='age-group']").val();

    // run ajax
    $.ajax({
      url: "/wp-admin/admin-ajax.php",
      type: "post",
      data: {
        action: "admin_load_week_classes",
        location,
        className,
        gotoWeek,
        age_group
      },
      beforeSend: function() {
        // Show image container
        $("#loader").show();
      },
      success: function(response) {
        $("#loader").hide();
        $(".admin-week-table").html(response);
        $(".admin-week-table").css("opacity", "1");
      },
      failure: function(response) {
        alert("There was an error getting classes from the server");
        $(".admin-week-table").css("opacity", "1");
      }
    });
  });

  // Function to call the backend and reload the week view when the date is select from datepicker
  $(document).on("change", "#datepicker-day", function(e) {
    e.preventDefault();

    var date = moment( $(this).datepicker("getDate") ).format( "YYYY-MM-DD" );
    var location = $("#admin-location").val();
    var className = $("#admin-class-name").val();

    console.log(
      {
        action: "admin_load_day_classes",
        date: date,
        location: location,
        className: className
      }
    );

    // run ajax
    $.ajax({
      url: "/wp-admin/admin-ajax.php",
      type: "post",
      data: {
        action: "admin_load_day_classes",
        date: date,
        location: location,
        className: className
      },
      beforeSend: function() {
        // Show image container
        $("#loader").show();
      },
      success: function(response) {
        $("#loader").hide();
        $(".admin-week-table").html(response);
        $(".admin-week-table").css("opacity", "1");
      },
      failure: function(response) {
        alert("There was an error getting classes from the server");
        $(".admin-week-table").css("opacity", "1");
      }
    });
  });

})(jQuery, window, document);
