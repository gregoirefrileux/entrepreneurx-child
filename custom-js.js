/*
 *
 * THIS FILE IS TO USE FOR ADDING CUSTOM JS ACROSS THE SITE
 * It is particularly well suited to hold small snippets of code that don't really belong in a custom plugin
 *
 * Some best practices are to make sure that code added here won't clash with ecisting code
 * And the second thing to bear in mind is that the file will execute in all page, so any jQuery selector needs to be well thought out
 * in order not to catch any unwanted elements
 *
 * Initial publish: Greg Frileux
 *
 */

// custom script to close the HP video
jQuery(document).on("click", "div#close-video", function() {
  jQuery("#hp-video-section").hide(800);
});

// Add new child detail in My Account > Child Information
var i = 0;
jQuery(document).on("click", "#add_more", function() {
  if (i == 0) {
    jQuery(".tablehideone").show();
  }
  if (i == 1) {
    jQuery(".tablehidetwo").show();
    jQuery("#add_more").hide();
  }
  i++;
});

jQuery(document).ready(function() {
  var wrapper = jQuery(".input_fields_wrap"); //Fields wrapper
  var add_button = jQuery(".add_field_button"); //Add button ID

  jQuery(add_button).click(function(e) {
    //on add input button click
    event.preventDefault();
    jQuery(wrapper).show();
  });
});

// Home Page : hack to only display "LOG IN" on mobile and not "LOG IN / REGISTER"
jQuery(document).ready(function() {
  jQuery(
    ".top-nav-icon-blocks .icon-block .fa.fa-user"
  )[0].parentNode.innerText = jQuery(
    ".top-nav-icon-blocks .icon-block .fa.fa-user"
  )[0].parentNode.innerText.match(/(.*)\//)[0];
});

// Home - swapping the "LOGIN" link for "LOGOUT" if user is logged in
jQuery(document).ready(function() {
  if (jQuery("body").hasClass("logged-in")) {
    link = jQuery("p[data-hidden-gfr]").attr("data-link");
    if (link && link != "") {
      // we have a valid log out link
      jQuery('.icon-block:has(".fa.fa-user")').html(
        "<p><a href='" +
          link +
          "'><i class='fa fa-user' /><span>LOG OUT</span></a></p>"
      );
    }
  }
});
