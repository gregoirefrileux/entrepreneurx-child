<?php

function attendance_meta_get($order_id) {
  $meta = get_post_meta($order_id, '_attendance', true);
  return $meta === "" ? [] : $meta;
}

function attendance_meta_update($order_id, $class_id, $child_key, $did_attend) {
  $meta = attendance_meta_get($order_id);
  $meta[$child_key][$class_id] = $did_attend;
  update_post_meta($order_id, '_attendance', $meta);
}

function did_child_attend($order_id, $class_id, $child_key) {
  $meta = attendance_meta_get($order_id);
  if($meta || empty($meta[$child_key]) || empty($meta[$child_key][$class_id]))
    return false;
  return $meta[$child_key][$class_id] === true;
}

function save_attendance_action() {
	$data = $_POST['attendance_data'];
	$class_id = $_POST['class_id'];
  foreach( $data as $item ) {
    $did_attend = $item["did_attend"] == "1" ? true : false;
    attendance_meta_update( $item["order_id"], $class_id, $item["child_key"], $did_attend );
    $_order = wc_get_order( $item["order_id"] );
    if ( $did_attend ) {
      $class_date = get_class_date( $class_id );
      set_latest_attendance($_order->get_user_id(), $class_date, $item["child_key"]);
    }
  }
	wp_die();
}
add_action( 'wp_ajax_save_attendance_action', 'save_attendance_action' );
add_action( 'wp_ajax_nopriv_save_attendance_action', 'save_attendance_action' );

function get_class_date( $class_id ) {
  $terms = wc_get_product_terms( $class_id, 'pa_class-date', array( 'fields' => 'names' ) );
  return count( $terms ) == 0 ? "No Record Found" : $terms[0];
}

function get_latest_attendance_date($user_id, $child_key = -1) {
  $meta_array = get_user_meta( $user_id, "_latest_attendance_date", true );
  if(!$meta_array || empty($meta_array[$child_key]))
    return "No attendance found";
  if ( $child_key == -1 )
    return $meta_array;
  return $meta_array[ $child_key ];
}

function set_latest_attendance($user_id, $class_date, $child_key) {
  $meta_array = get_user_meta( $user_id, "_latest_attendance_date", true );
  $old_value = isset( $meta_array[$child_key] ) ? $meta_array[$child_key] : "0000-00-00";
  if ( $old_value > $class_date )
    return;
  update_user_meta( $user_id, "_latest_attendance_date", [ $child_key => $class_date ] );
}

function is_first_class($user_id, $child_key) {
  return get_latest_attendance_date($user_id, $child_key) == "No Record Found";
}
