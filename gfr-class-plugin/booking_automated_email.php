<?php
// This file is where the main functions to send automated daily emails to parents who have a class 'tomorrow'
require_once('includes/custom_email_class_setup.php');


// This links the main function to the hook declared in the CRON schedule
add_action( 'booking_send_email_reminders', 'gfr_cron_send_email_reminders_for_class_tomorrow' );

// This links the main function to the hook declared in the CRON schedule
add_action( 'booking_send_unpaid_order_email_reminders', 'gfr_cron_send_unpaid_order_emails' );

/*
 *
 * This is the main function that orchestrates the CRON job of sending emails for classes tomorrow
 *
 */
function gfr_cron_send_email_reminders_for_class_tomorrow() {
    $tomorrow = date('Y-m-d',strtotime('tomorrow'));
    // now let's fetch the classes occurring tomorrow
    $classes_tomorrow = get_single_classes_for_one_day($tomorrow);
    // we only need an array of product IDs

    // now let's get the orders that contain those products
    $orders_tomorrow = array();
    foreach($classes_tomorrow as $class) {
        // get the orders that include the current class
        $order_ids_for_class = get_orders_ids_by_product_id($class->get_id());
        if(sizeof($order_ids_for_class) > 0) {
            // add the array of order ids to the global array
            $orders_tomorrow = array_merge($orders_tomorrow, $order_ids_for_class);
        }
    }
    // we now have an array of order IDs that each contain a class occurring tomorrow
    // we now need to build a list of email addresses to contact
    $parents_emails = get_emails_from_order_ids($orders_tomorrow, $tomorrow);

    send_class_reminder_email_to_parents($parents_emails);
}

/*
 * Function to retrieve products corresponding to a class for a given date
 * args: DATE: the string format date YYYY-mm-dd to retrieve products from
 *
 * returns: array of classes in WC_PRODUCT format
 */

function get_single_classes_for_one_day($date, $location = 'all', $class_name = 'all' ) {

    // First build the query
	$args = array(
        'product_cat' => 'standard-class', 'holiday-class',// here we only load the "Term Products" that should live inside the Term Class Cat
        'posts_per_page' => -1,
        'status' => 'publish', //only published products
        'tax_query' => array(
            array(
                'taxonomy' 		=> 'pa_class-date',
                'terms' 		=> $date,
                'field' 		=> 'slug',
                'operator' 		=> 'IN'
                ),
        )
    );

    if ( $location != "all")
      array_push($args['tax_query'], array(
       'taxonomy' 	=> 'pa_location',
       'terms' 		=> $location,
       'field' 		=> 'slug',
       'operator' 	=> 'IN'
      ));

    if ( $class_name != "all")
      array_push($args['tax_query'], array(
       'taxonomy' 	=> 'pa_class-name',
       'terms' 		=> $class_name,
       'field' 		=> 'slug',
       'operator' 	=> 'IN'
      ));

    // Now we retrieve the products
    $products = wc_get_products($args);

    return $products;
}

/*
 * Function to extract the parents emails from a list of order IDs
 * We also pass the date we're searching for (usually it'll be "tomorrow")
 * args: array of order IDs: [12321,123812,21321]
 *
 * returns: array of order data :
 * array (
 *      1654 => (
 *              'order_id' => 1654,
 *              'parent_email' => 'test@me.com'
 *              )
 *      1676 => (
 *              'order_id' => 1676,
 *              'parent_email' => 'parents@hotmail.com'
 *              )
 *      )
 */

function get_emails_from_order_ids($array_order_ids, $date = null) {
    $parents_emails = array();

    foreach($array_order_ids as $order_id) {
        $order = wc_get_order($order_id);
        // get the parent's email
        $parent_email = $order->get_billing_email();
        $parent_first_name = $order->get_billing_first_name();
        // get some details on the class (provided a date was given - used in class reminder but not for unpaid order reminder)
        if ($date) {
            $class_detail = gfr_get_class_detail_for_order_and_date($order, $date);
        } else {
            $class_detail = [];
        }
        // build the overall result
        $parents_emails[$order_id] = array_merge(array(
            'order_id' => $order_id,
            'date_created' => $order->get_date_created()->format('Y-m-d H:m'),
            'parent_email' => $parent_email,
            'parent_first_name' => $parent_first_name), $class_detail);

    }

    return($parents_emails);
}

/*
 *
 * Function the find a class at the $date in the $order
 * An order could have several classes, so we want to pick out the one that is at $date
 * Returns an array with some key info:
 * [
 *      location => 'Notting Hill'
 *      class-name => 'Tarka Beats'
 * ]
 */

function gfr_get_class_detail_for_order_and_date($order, $date) {
    foreach ( $order->get_items() as  $item_id => $item ) {
        // Get the product (if any for this item)
        $class = $item->get_product();
        if($class && get_cust_att($class, 'pa_class-date') == $date ) {
            return array(
                'location'  => get_cust_att($class, 'pa_location'),
                'address'  => strip_tags(get_cust_att_description($class, 'pa_location')), // get the description without any html tag like line return at the end
                'name'      => $class->get_title(),
                'class-name'=> get_cust_att($class, 'pa_class-name'),
                'stock'     => $class->get_stock_quantity(),
                'term-class'=> get_cust_att($class, 'pa_term-class'),
                'start-time'=> get_cust_att($class, 'pa_start-time'),
                'finish-time'=> get_cust_att($class, 'pa_finish-time'),
                'day-of-the-week'=> get_cust_att($class, 'pa_day-of-the-week'),
                'term-name'=> get_cust_att($class, 'pa_term-name'),
                'class-date'=> get_cust_att($class, 'pa_class-date'),
                'price'     => $class->get_price(),
                'category'     => $class->get_category_ids(),
            );
        }
    }
    // if no class was found at this date for any reason, log it
    error_log("couldn't find class");
    error_log("here is more detail, class id is: " . $order->get_id() . " and date is: " .$date);
    return [];
}

/*
 * Function to actually send parents the reminder email for a class tomorrow
 * args: an array of parent data
 * array (
 *      1654 => (
 *              'order_id' => 1654,
 *              'date_created' => '2019-07-28 12:23',
 *              'parent_email' => 'test@me.com'
 *              'parent_first_name' => Karen
 *              )
 *
 */
function send_class_reminder_email_to_parents($parents_emails) {

    foreach($parents_emails as $parent_data) {
        // The below lines are necessary to register our custom classes properly with WooCommerce
        global $woocommerce;
        $mailer = WC()->mailer();
        // this will trigger an email to be sent
        do_action('gfr_send_reminder_email', $parent_data);
    }

}

/*
 *
 * Function that orchestrates the sending of emails reminders to parents for unpaid orders
 *
 */

function gfr_cron_send_unpaid_order_emails() {

    $unpaid_order_ids = wc_get_orders(array(
        'status' => 'pending',
        'return' => 'ids',
        'limit' => 50, // there should never really be more than 50 unpaid order on any given day ?!
    ));

    // Fetch unpaid orders
    $formatted_email_data_array = get_emails_from_order_ids($unpaid_order_ids);

    // Filter to return only the orders created in the last 5 days
   $formatted_orders_in_scope = filter_orders_in_date_range($formatted_email_data_array, 22);

   foreach($formatted_orders_in_scope as $email_data) {
        // The below lines are necessary to register our custom classes properly with WooCommerce
        global $woocommerce;
        $mailer = WC()->mailer();
        // this will trigger an email to be sent
        do_action('gfr_send_unpaid_order_reminder_email', $email_data);
    }

    // finally delete any pending order older than 5 days old
    // gfr_delete_old_orders($formatted_email_data_array, $formatted_orders_in_scope);
 }


 /*
  *
  * Filter order in a given date range
  * ARGS: $formatted_email_data_array : a formatted array (see above) of emails and email data
  * ARGS: Num, number of DAYS to look back. Defaults to 5. This means if we're the 6th August, it'll return orders
  * from the 1st -> 5th
  *
  * RETURNS: array of orders that fall in the scope provided
  *
  */
  function filter_orders_in_date_range($formatted_email_data_array, $scope_num_days = 5) {

    $yesterday = new DateTime("yesterday");

    $formatted_data_in_scope = [];

    foreach ($formatted_email_data_array as $order_data) {
        $order_date = new DateTime($order_data['date_created']);
        $diff = $yesterday->diff($order_date);
        if($diff && $diff->d < $scope_num_days) {
            array_push($formatted_data_in_scope, $order_data);
        }
    }

    return($formatted_data_in_scope);

  }

 /*
  *
  * Delete order older than 5 days
  * ARGS: $formatted_email_data_array : a list of all pending orders
  * ARGS: $order_in_scope : a list of orders NOT TO DELETE
  *
  * RETURNS: Bool - true or FALSE if there was an issue
  *
  */
function gfr_delete_old_orders($formatted_orders, $orders_in_scope) {

    foreach($formatted_orders as $order_data) {
        if(in_array($order_data, $orders_in_scope)) {
            // this order should not be deleted
        } else {
            // ok this one should be deleted
            $order = new WC_Order($order_data['order_id']);
            // delete all items
            gfr_copy_woocommerce_delete_all_items($order);

        }
    }

}

/*
 *
 * Function heavily copied from woocommerce/includes/admin/wc-admin-functions.php
 * It parses all items and deletes them as it would during a manual edit of an Order by an admin
 *
 *
 */

function gfr_copy_woocommerce_delete_all_items($order) {

    // Require the WC file so we can access its functions
    require_once(ABSPATH . 'wp-content/plugins/woocommerce/includes/admin/wc-admin-functions.php');

    // Set some vars
    $order_id = $order->get_id();
    $items = $order->get_items();
    $order_item_ids = [];
    foreach($items as $item) {
        array_push($order_item_ids,$item->get_id() );
    }
    //more setup
    $calculate_tax_args = array(
        'country'  => wc_clean( wp_unslash( $order->get_billing_country() ) ),
        'state'    => wc_clean( wp_unslash( $order->get_billing_state() ) ),
        'postcode' => wc_clean( wp_unslash( $order->get_billing_postcode() ) ),
        'city'     => wc_clean( wp_unslash( $order->get_billing_city() ) ),
    );

    if ( ! is_array( $order_item_ids ) && is_numeric( $order_item_ids ) ) {
        $order_item_ids = array( $order_item_ids );
    }

    // Start deleting - this is copied from woocommerce/includes/admin/wc-admin-functions.php
    if ( ! empty( $order_item_ids ) ) {
        $order_notes = array();

        foreach ( $order_item_ids as $item_id ) {
            $item_id = absint( $item_id );
            $item    = $order->get_item( $item_id );

            // Before deleting the item, adjust any stock values already reduced.
            if ( $item->is_type( 'line_item' ) ) {
                $changed_stock = wc_maybe_adjust_line_item_product_stock( $item, 0 );

                if ( $changed_stock && ! is_wp_error( $changed_stock ) ) {
                    /* translators: %1$s: item name %2$s: stock change */
                    $order->add_order_note( sprintf( __( 'Deleted %1$s and adjusted stock (%2$s)', 'woocommerce' ), $item->get_name(), $changed_stock['from'] . '&rarr;' . $changed_stock['to'] ), false, true );
                } else {
                    /* translators: %s item name. */
                    $order->add_order_note( sprintf( __( 'Deleted %s', 'woocommerce' ), $item->get_name() ), false, true );
                }
            }

            wc_delete_order_item( $item_id );
        }
    }

    $order = wc_get_order( $order_id );
    $order->calculate_taxes( $calculate_tax_args );
    $order->calculate_totals( false );

    // now update order status
    $order->update_status('cancelled', 'Order cancelled by automatic bot');
}


 ?>
