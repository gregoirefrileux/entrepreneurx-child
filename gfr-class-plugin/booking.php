<?php
if (!session_id()) {
    session_start();
}
// Global VAR
define( 'CHILD_THEME_PATH', get_stylesheet_directory() );
// load the rest of the plugin files
require_once('booking_automated_email.php');
require_once('child_detail_custom_field.php');
require_once('booking_admin.php');
require_once('add_child_class.php');
// Now for Helper functions
require_once('includes/booking_helper_functions.php');
require_once('includes/admin/admin_booking_helper_functions.php');
require_once('includes/cron-product-stock-correct.php');
require_once('includes/class-ics-calendar-files.php');
// Include the individual templates
require_once('templates/frontend-calendar/calendar_header.php');
require_once('templates/frontend-calendar/calendar_popup.php');
require_once('templates/frontend-calendar/single_class_box.php');
require_once('templates/woocommerce-tweak/helper_functions.php');
// CSS and JS File versioning
$css_front_booking      =  filemtime( CHILD_THEME_PATH .'/gfr-class-plugin/assets/css/booking.css' );
$css_front_booking_resp = filemtime( CHILD_THEME_PATH .'/gfr-class-plugin/assets/css/responsive.css' );
$js_front_booking       = filemtime( CHILD_THEME_PATH .'/gfr-class-plugin/assets/js/booking.js' );
define( 'CSS_FRONT_BOOKING_CSS_VER', $css_front_booking );
define( 'CSS_FRONT_BOOKING_RESP_CSS_VER', $css_front_booking_resp );
define( 'JS_FRONT_BOOKING_JS_VER', $js_front_booking );

// require the My Account Dashboard template
add_action('woocommerce_account_dashboard', 'gfr_my_account_dashboard');
function gfr_my_account_dashboard() {
    include("templates/my_account_dashboard.php");
}

// Declaring the main "Book a Term" shortcode
add_shortcode( 'book_a_term_desktop', 'gfr_book_term_class' );

// Declaring the ajax functions
add_action('wp_ajax_load_classes_for_week_with_params', 'gfr_load_classes_for_week_with_params');
add_action('wp_ajax_nopriv_load_classes_for_week_with_params', 'gfr_load_classes_for_week_with_params');
// add to cart function
add_action('wp_ajax_add_term_to_cart', 'gfr_custom_add_to_cart_term');
add_action('wp_ajax_nopriv_add_term_to_cart', 'gfr_custom_add_to_cart_term');

//Declare the shortcode style
function gfr_booking_shortcode_enqueue_style() {
    wp_register_style( 'gfr-booking-style', get_stylesheet_directory_uri() . '/gfr-class-plugin/assets/css/booking.css', array(), CSS_FRONT_BOOKING_CSS_VER );
    wp_register_style('gfr-booking-responsive', get_stylesheet_directory_uri() . '/gfr-class-plugin/assets/css/responsive.css', array(), CSS_FRONT_BOOKING_RESP_CSS_VER);
}

//Declare the shortcode style
function gfr_booking_shortcode_enqueue_js() {
    wp_register_script( 'gfr-booking-js', get_stylesheet_directory_uri() . '/gfr-class-plugin/assets/js/booking.js', array(), JS_FRONT_BOOKING_JS_VER);
    wp_enqueue_script('spin-js', get_stylesheet_directory_uri() . '/gfr-class-plugin/assets/js/spin.min.js', array(), '2.0.1');
    wp_enqueue_script('spin-jquery', get_stylesheet_directory_uri() . '/gfr-class-plugin/assets/js/spin.jquery.js', array(), '2.0.1');
}

// Enqueu both style and js file
add_action( 'wp_enqueue_scripts', 'gfr_booking_shortcode_enqueue_style' );
add_action( 'wp_enqueue_scripts', 'gfr_booking_shortcode_enqueue_js' );


function gfr_book_term_class( $atts ) {
    $params = shortcode_atts( array(
       'location' => 'all',
       'week-starting' => 'novalue',
       'class-name' => 'all',
       'age-group' => 'all'
    ), $atts );

    // Set the current date as default if none is passed
    if ($params['week-starting'] === 'novalue') {
        // we try and see if a value has been passed in the Query String
        $week_starting_from_param = isset($_GET['week-starting']) ? $_GET['week-starting'] : null;
        if ($week_starting_from_param && $week_starting_from_param != '') {
            $params['week-starting'] = date("Y-m-d", strtotime($week_starting_from_param));
        }
        else {
        //we default to the current week
        $local_time = current_time('timestamp');
        $current_day = date_i18n('Ymd',$local_time);

        $year = date_i18n('Y',$local_time);
        $month = date_i18n('m',$local_time);
        $day = date_i18n('d',$local_time);

        $params['week-starting'] = date("Y-m-d", strtotime('monday this week'));
        }

    }

    // enqueue the style on the page
    wp_enqueue_style( 'gfr-booking-style' );
    wp_enqueue_style( 'gfr-booking-responsive' );
    wp_enqueue_script( 'gfr-booking-js' );
    wp_enqueue_script( 'spin-js' );
    wp_enqueue_script( 'spin-jquery' );

    // Let's get all the classes we need
    $week_start = date('Y-m-d', strtotime($params['week-starting']));
    $week_end = date('Y-m-d', strtotime($params['week-starting'] .'+ 6 days'));

    // We'll need some dropdown menus with the list of locations, terms, etc. Let's get these arrays of values
    $all_locations = get_all_cust_att_values("pa_location");
    $all_class_names = get_all_cust_att_values("pa_class-name");
    $all_ages = ''; //TODO : get a new "age group" parameter added to all classes

    // Now we know the possible classes, it's a good time to check if a class name has been pased in the query string
    if(isset($_GET['class-name']) && $_GET['class-name'] !== '') {

        // a classname has been given in query string, most likely from a "BOOK" button from another page
        // we see if we can use it
        if (search_query_in_array($_GET['class-name'], $all_class_names)) {
            $params['class-name'] = $_GET['class-name'];
        }
    }

    // Get location if passed in URL
    if (isset($_GET['location']) && $_GET['location'] !== '') {
        $params['location'] = $_GET['location'];
        // TODO - verify that location is in array of possible locations
    }

    // Get age group if passed in URL
    if (isset($_GET['age-group']) && $_GET['age-group'] !== '') {
        $params['age-group'] = $_GET['age-group'];
        // TODO - verify that age group is in array of possible age groups
    }

    // Let's pull out all the classes IDs that are happening this week
    $classes = get_classes_for_week($week_start, $week_end, $params['location'], $params['class-name'], $params['age-group']);

    // Now let's actually build some HTML
    // First we build the header
    echo '<div id="term_view_wrapper" class="term_view_wrapper">';

    // Then build the select options
    echo '<div class="term_week_header">';
    echo build_header($all_locations, $all_class_names, $all_ages, $params['location'], $params['class-name'], $params['age-group'], $params['week-starting']);
    echo '</div>';
    // Second we build the week view
    echo '<div id="bookedTimeslotsWrap">';
    echo get_body_table($classes, $week_start);
    echo '</div></div>';

    // closing the final div
    echo '</div>';

    // return($html);
 }




// Function to generate the body of the Week view to book a term
// This function creates a table starting Monday to Sat and shows the classes

function get_body_table($classes_to_display, $week_start) {
    $day_loop = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');

    ?>

    <table class="booked-timeslots">
        <thead>
            <tr>
                <th colspan="6" class="week-navigator">
                    <a href="#" data-goto="<?php echo date('Y-m-d', strtotime($week_start . '- 7 day'))?>" class="page-left">< Previous week</a>
                    <span class="calendarSavingState" style="display: none;">
                        <i class="fas fa-spinner"></i>
                    </span>
                    <span class="weekName" data-date="<?php echo $week_start; ?>">
                        <?php echo( date('d F', strtotime($week_start)) . ' - ' . date('d F Y', strtotime($week_start . '+ 6 day')) ); ?>
                    </span>
                    <a href="#" data-goto="<?php echo date('Y-m-d', strtotime($week_start . '+ 7 day')) ?>" class="page-right">Next week ></a>
                </th>
            </tr>
        </thead>
		<tr>
			<?php
        foreach($day_loop as $key => $day):
          $current_date = date('Y-m-d', strtotime($week_start. '+' . $key . 'day' ));
          $date_str = date('jS', strtotime($week_start. '+' . $key . 'day' ));
          ?>
			<td id="column-<?php echo $day ?>" class="<?php if($day === 'Monday') {echo 'show-mobile-only';} else {echo 'hide-mobile-only';} ?>">
				<table>
					<thead>
						<tr>
							<?php
							echo '<th data-day="'.$day_loop[$key].'">';
								echo "<div class='data-day'>$day $date_str</div>";
							echo '</th>';
							?>
						</tr>
					</thead>
					<tbody>
						<tr>
                            <?php echo '<td class="dayTimeslots" data-day="'.$day_loop[$key].'">';
                                if (!empty($classes_to_display[$current_date])):
                                    ksort($classes_to_display[$current_date],1);
                                    echo '<div class="class-time-slot">';
                                    foreach($classes_to_display[$current_date] as $time => $locations):
                                        foreach($locations as $location):
                                                // Now create the block that will be displayed in the calendar
                                                display_classes_at_location($location);
                                        endforeach;
                                    endforeach;
                                    echo '</div>';
								else :
									echo '<p><small>'.esc_html__('No classes.','booked').'</small></p>';
								endif;
							echo '</td>'; ?>
						</tr>
					</tbody>
				</table>
			</td>
			<?php endforeach; ?>
		</tr>
	</table><?php

}


/*
 *
 * Function to retrieve Term products
 * args: LOCATION: the location to retrieve products from - returns all if not passed or null or == "all"
 * args: TERM: the term for which to pull products for
 *
 * returns: array of classes in format that the Week Calendar list will be able to display
 *
 */

function get_term_products($term, $location) {

    // First build the query
	$args = array(
        'product_cat' => 'term-class',// here we only load the "Term Products" that should live inside the Term Class Cat
        'posts_per_page' => -1,
        'status' => 'published',//only published products
        'tax_query' => array(
            array(
                'taxonomy' 		=> 'pa_term-name',
                'terms' 		=> $term,
                'field' 		=> 'slug',
                'operator' 		=> 'IN'
                ),
            ),
        );

    // Second add the location - if the user wants "all" we simply omit the location from the search
    if($location !== 'all') {
       // we need to add the specific location asked to the query
       array_push($args['tax_query'], array(
        'taxonomy' 		=> 'pa_location',
        'terms' 		=> $location,
        'field' 		=> 'slug',
        'operator' 		=> 'IN'
       ));
    }

    // Now we retrieve the products
    $products = wc_get_products($args);

    // So now we have a bunch of products - we need to createa readable array of them
    $formatted_prodcuts = format_term_product_list($products);

    return $formatted_prodcuts;
}


/*
 *
 * Function to retrieve Term products
 * args: products: an array of WooCommerce Products
 *
 *
 * returns: array of classes in format:
 *     $classes = array(
    [Tuesday] =&gt; Array
        (
            [9:00] =&gt; Array
                (
                    [Notting Hill] =&gt; Array
                        (
                            [Tarka x3 Hr Weekly Session] =&gt; Array
                                (
                                    [943] =&gt; Array
                                        (
                                            [id] => 943
                                            [location] => Notting Hill
                                            [name] => Tarka x3 Hr Weekly Session – Tuesday – 3H – 9AM – Notting Hill - Second half term (test)
                                            [stock] => 2
                                        )

                                    [1] =&gt; Array
                                        (
                                            [id] => 911
                                            [location] => Notting Hill
                                            [name] => Tarka x3 Hr Weekly Session – Tuesday – 3H – 9AM – Notting Hill - First half term
                                            [stock] => 4
                                        )

                                )

                        )

                )

        )

    [Thursday] =&gt; Array
        (
            [9:30] =&gt; Array
                (
                    [Notting Hill] =&gt; Array
                        (
                            [Tarka Younger Class (18month-3yrs)] =&gt; Array
                                (
                                    [939] =&gt; Array
                                        (
                                            [id] =&gt; 939
                                            [location] =&gt; Notting Hill
                                            [name] =&gt; Tarka x45min Weekly Session – Thursday – 45min – 9:30AM – Notting Hill - First half term
                                        )

                                )

                        )

                )

        )

)

 *
 */

function format_term_product_list($WCproducts) {

    // a simple list of all products
    $product_list = [];
    // now we initialize the final list
    $class_list = [];

    foreach($WCproducts as $WCproduct) {
        array_push($product_list, [
            get_cust_att($WCproduct, 'pa_day-of-the-week') => array (
                get_cust_att($WCproduct, 'pa_start-time') => array(
                    get_cust_att($WCproduct, 'pa_location') => array(
                        get_cust_att($WCproduct, 'pa_class-name') => array(
                            $WCproduct->get_id() => array(
                                'id'        => $WCproduct->get_id(),
                                'location'  => get_cust_att($WCproduct, 'pa_location'),
                                'name'      => $WCproduct->get_title(),
                                'class-name'=> get_cust_att($WCproduct, 'pa_class-name'),
                                'stock'     => $WCproduct->get_stock_quantity(),
                                'term-class'=> get_cust_att($WCproduct, 'pa_term-class'),
                                'start-time'=> get_cust_att($WCproduct, 'start-time'),
                                'finish-time'=> get_cust_att($WCproduct, 'finish-time'),
                                'price'=> get_cust_att($WCproduct, 'finish-time'),
                                'day-of-the-week'=> get_cust_att($WCproduct, 'pa_day-of-the-week'),
                                'term-name'=> get_cust_att($WCproduct, 'pa_term-name'),
                                'class-date'=> get_cust_att($WCproduct, 'pa_class-date')
                                )
                            )
                        )
                    )
                )
            ]
        );
    }

    for($i=0; $i < sizeof($product_list); $i++) {
        if ($i === 0) {
            $class_list = $product_list[0];
        } else {
            $class_list = array_merge_recursive($class_list, $product_list[$i]);
        }
    }

    return($class_list);

}

/*
 *
 * Function that displays a message if none of the half term, or full term products have any stock
 * Takes in a class_options which may well be three bundle products for "Half Term 1" , "Half Term 2" and "Full Term"
 * We parse each bundle, get its stock. If none at all left, we say it
 * The function will always return a bool and some HMTL
 *
 */

function display_stock_level_multiproducts($class_options) {

    // we take the view that all stocks are nill by initialising $stock_left to false
    // if 1 product has some stock we switch back to TRUE
    $stock_left = false;
    // Same for the message
    $html = '<span class="alert">All spaces have been booked for this class for this term</span>';


    foreach($class_options as $class) {
        if($class['stock'] > 0) {
            $stock_left = true;
        }
    }
    // now let's look if all the values are equal to 0
    if ($stock_left) {
        $html = '';
    }
    return(array($stockLeft, $html));
}

function is_class_suitable_for_age_groups( $class, $age_groups ) {
  $_product = wc_get_product( $class["id"] );

  $min_age_limit = floatval( $_product->get_attribute( 'pa_age-limit' ) );

  foreach ( $age_groups as $single_age_group ) {
    $age_inc = $single_age_group[ 'min' ];

    while ( $age_inc < $single_age_group[ 'max' ] ) {
      if ( $age_inc == $min_age_limit )
        return TRUE;

      $age_inc = $age_inc + 0.5;
    }

  }

  return FALSE;
}
/*
 *
 * Ajax function called when a use interacts with the filters in the week view
 * It essentially recalculates the week to display
 *
 */

 function gfr_load_classes_for_week_with_params($wtf = NULL, $die = TRUE) {

    $gotoWeek = (isset($_POST['gotoWeek']) ? $_POST['gotoWeek'] : 'novalue');

    if(!$gotoWeek || $gotoWeek === '') {
        //we can't display classes without knowing the date - return error
        $data = array(
            'error'       => true,
            'message' => "Please specify the week"
        );
        // return success or failure to the page
        wp_send_json( $data );

        wp_die();
    }

    $week_start = date('Y-m-d', strtotime($gotoWeek));
    $week_end = date('Y-m-d', strtotime($gotoWeek .'+ 6 days'));

    // get the other params
    $location = (isset($_POST['location']) ? $_POST['location'] : 'all');
    $className = (isset($_POST['className']) ? $_POST['className'] : 'all');
    $age_group = (isset($_POST['age_group']) ? $_POST['age_group'] : 'all');
    $min_age = (isset($_POST['min_age']) ? $_POST['min_age'] : 'all');


    $classes_to_render = get_classes_for_week($week_start, $week_end, $location, $className, $min_age);

    // error_log( print_r( [
    //   "function" => 'gfr_load_classes_for_week_with_params',
    //   "location" => $location,
    //   "die desc" => $die ? 'die' : 'dont die',
    //   '$classes_to_render' => $classes_to_render
    // ], true ) );

    if ( !$die )
      return $classes_to_render;

    echo get_body_table($classes_to_render, $week_start);

    if ( $die )
      wp_die();
}



/**
 * AJAX add to cart.
 * This function is called by the "Add to Cart" button inside the modal box
 * when viewing some detailed way of adding a class term to cart
 */

function gfr_custom_add_to_cart_term() {
        ob_start();

        $product_id = (isset($_POST['product_id']) ? $_POST['product_id'] : null);
        $quantity          = 1;
        $passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );
        $product_status    = get_post_status( $product_id );

        if ( $passed_validation && WC()->cart->add_to_cart( $product_id, $quantity ) && 'publish' === $product_status ) {

            do_action( 'woocommerce_ajax_added_to_cart', $product_id );

            wc_add_to_cart_message( $product_id );

        } else {

            // If there was an error adding to the cart, redirect to the product page to show any errors
            $data = array(
                'error'       => true,
                'product_url' => apply_filters( 'woocommerce_cart_redirect_after_error', get_permalink( $product_id ), $product_id )
            );

            wp_send_json( $data );

        }

        die();

}


function jdr_min_age_limit_for_class( $class_id ) {
  $val = array_shift( wc_get_product_terms( $class_id, 'pa_age-limit', array( 'fields' => 'names' ) ) );
  return is_null( $val ) ? "0" : $val;
}
