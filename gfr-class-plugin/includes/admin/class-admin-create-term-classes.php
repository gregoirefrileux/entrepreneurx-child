<?php
/**
 * GFR - January 2020
 * Performs all functions related to automatically creating Term Classes
 */
 
/**
 */
class AdminCreateTermClasses {
 
    /**
     * Initializes the function by registering the save function with the
     * admin_post hook so that we can save our options to the database.
     */
    public function init() {
        // add_action( 'admin_action_createTermClasses', array( $this, 'gfr_create_term_classes' ) );
        add_action('wp_ajax_createTermClasses', array( $this, 'gfr_create_term_classes' ));
    }
 
    /**
     * Validates the incoming nonce value, verifies the current user has
     * permission to save the value from the options page and saves the
     * option to the database.
     */
    public function gfr_create_term_classes() {
        // First, validate the nonce and verify the user as permission to save.
        if ( ! ( $this->has_valid_nonce() && current_user_can( 'edit_users' ) ) ) {
            // TODO: Display an error message.
            $return = [
                'error' => true,
                'message' => 'Invalid form'
            ];
            $this->redirect($return);
        }
 
        // If the above are valid, sanitize and save the option.
        if ( null !== wp_unslash( $_POST['term_name'] ) ) {
 
            $term_name = sanitize_text_field( $_POST['term_name'] );
            // create the term products
            $term_products = $this->create_term_products($term_name);
            // if no errors return success
            if($term_products && is_array($term_products) && sizeof($term_products) > 0) {
                $return = [
                    'error' => false,
                    'message' => 'Products created successully',
                    'ids' => $this->get_all_ids_for_terms_products($term_products)
                ];
                $this->redirect($return);
            } else {

            }
        }
    }

    /**
     * Creates the term products, givne a $term_name. Usually will be a slug such as 'spring-term-2020'
     * 
     * @eturn array of WC_PRODUCTS or false if an error occures
     */
    private function create_term_products($term_name) {
        // First we need to get all the classes for the term
        $array_individual_classes = $this->get_classes_for_term($term_name);

        if($array_individual_classes) {
            // format the classes in a simple way for the next step
            $classes_grouped_by_location_and_time = $this->format_array_individual_classes($array_individual_classes);
        } else {
            // no products found for the term - return an error
            $return = [
                'error' => true,
                'message' => 'No products found for term'
            ];
            $this->redirect($return);
        }

        // initialise array of term products
        $array_term_products = [];
        foreach($classes_grouped_by_location_and_time as $location => $classes_at_location) {
            // parsing all classes at location $locatiom
            foreach ($classes_at_location as $day_of_the_week => $classes_on_day_of_week) {
                // parsing classes at $location and on day $day_of_the_week
                foreach( $classes_on_day_of_week as $start_time => $classes_by_name) {
                    // parsing classes at $location, on $day $day_of_the_week and starting at $start_time

                    // now we should only have a list of names
                    foreach($classes_by_name as $name => $class_group) {
                        $new_term_product = $this->gfr_create_single_term_product($class_group);
                        if($new_term_product) {
                            array_push($array_term_products, $new_term_product);
                        } else {
                            // not much to do and error would have been logged already
                        }
                    }
                }
            }
        }
        // Finally return the list of created products
        return($array_term_products);
    }


    /**
     * Function to retrieve all the classes for a given 
     * @return array of WC_PRODUCTS or false if none found for the term
     */
    function get_classes_for_term($term_name) {

        // First build the query
        $args = array(
            'product_cat' => 'standard-class',// here we only load the "Term Products" that should live inside the Term Class Cat
            'posts_per_page' => -1,
            'status' => 'publish', //only published products
        );

        // Second add the term_name
        $args['tax_query'] = array();
        array_push($args['tax_query'], array(
            'taxonomy' 	=> 'pa_term-name',
            'terms' 		=> $term_name,
            'field' 		=> 'slug',
            'operator' 	=> 'IN'
        ));

        // Now we retrieve the products
        $products = wc_get_products($args);

        if(!$products || !is_array($products)) {
            return false;
        }

        return $products;
    }

     /**
     * Given an array of WC_PRODUCTS, returns a formatted array of classes, grouped by location then start time then age group'
     * 
     * @return array formatted classes or false if an error occurs (see bottom of the file for reference)
     */
    private function format_array_individual_classes($array_class) {
        // a simple list of all products
        $product_list = [];
        // now we initialize the final list
        $class_list = [];

        if(!is_array($array_class) || sizeof($array_class) < 1) {
            return false;
        }
    
        foreach($array_class as $WCproduct) {
            array_push($product_list, [
                get_cust_att($WCproduct, 'pa_location') => array (
                    get_cust_att($WCproduct, 'pa_day-of-the-week') => array(
                        get_cust_att($WCproduct, 'pa_start-time') => array(
                            get_cust_att($WCproduct, 'pa_class-name') => array(
                                'id'        => $WCproduct->get_id(),
                                'location'  => get_cust_att($WCproduct, 'pa_location'),
                                'name'      => $WCproduct->get_title(),
                                'class-name'=> get_cust_att($WCproduct, 'pa_class-name'),
                                'stock'     => $WCproduct->get_stock_quantity(),
                                'term-class'=> get_cust_att($WCproduct, 'pa_term-class'),
                                'start-time'=> get_cust_att($WCproduct, 'pa_start-time'),
                                'finish-time'=> get_cust_att($WCproduct, 'pa_finish-time'),
                                'day-of-the-week'=> get_cust_att($WCproduct, 'pa_day-of-the-week'),
                                'term-name'=> get_cust_att($WCproduct, 'pa_term-name'),
                                'class-date'=> get_cust_att($WCproduct, 'pa_class-date'),
                                'price'     => $WCproduct->get_price(),
                                'category'     => $WCproduct->get_category_ids(),
                                'age-group'=> get_cust_att($WCproduct, 'pa_class-date')
                                )
                            )
                        )
                    )
                ]
            );
        }
    
        for($i=0; $i < sizeof($product_list); $i++) {
            if ($i === 0) {
                $class_list = $product_list[0];
            } else {
                $class_list = array_merge_recursive($class_list, $product_list[$i]);
            }
        }
    
        return($class_list);
        
    }

    /**
     * Creates the term products
     * Takes in a formatted group of products (see bottom for reference)
     * 
     * @return WC_Product_Woosb or false if an error occures
     */
    private function gfr_create_single_term_product($class_group) {
        try {
            // Some init - let's get all the attributes we'll need
            $location   = $class_group['location'][0];
            $name       = $class_group['name'][0];
            $className  = $class_group['class-name'][0];
            $startTime  = $class_group['start-time'][0];
            $finishTime = $class_group['finish-time'][0];
            $dayOfWeek  = $class_group['day-of-the-week'][0];
            $price      = $class_group['price'][0];
            $ageGroup   = $class_group['age-group'][0];
            $termName   = $class_group['term-name'][0];
            // stock - TODO: we should really parse the list and get the smallest stock
            $stock      = $class_group['stock'][0];
            // some arrays
            $listClassDate  = $class_group['class-date'];
            $listIds        = $class_group['id'];

            // Start the creation
            $product = new WC_Product_Woosb();
            $product->set_name($className . "-" . $dayOfWeek . "-" . $startTime . "-" . $location . "-Full Term" . $termName);
            
            // Set some default attributes
            $this->create_default_attributes($product);
            
            $product->set_price($price * sizeof($class_group['id'])); // set product price - use the size of the id sub key array which should contain the number of products in the bundle
            $product->set_regular_price($price * sizeof($class_group['id'])); // set product regular price

            $product->set_stock_quantity($stock);

            // Save the product
            $product_id = $product->save(); // it will save the product and return the generated product id

            // Link the Bundle with the individual classes
            // by updating metas to create a correctly formatted bundle product.
            $metaListIds = $this->format_product_ids_string($listIds);
            update_post_meta( $product_id, 'woosb_ids', $metaListIds );

            // Set some more default related to price
            $this->add_more_default_attributes($product_id);

            // Add all the custom attributes
            $this->add_wc_attribute($product_id, $location, 'pa_location');
            $this->add_wc_attribute($product_id, $dayOfWeek, 'pa_day-of-the-week');
            $this->add_wc_attribute($product_id, $startTime, 'pa_start-time');
            $this->add_wc_attribute($product_id, $finishTime, 'pa_finish-time');
            $this->add_wc_attribute($product_id, 'Full term', 'pa_term-class');
            $this->add_wc_attribute($product_id, $termName, 'pa_term-name');
            // For the lists
            $this->add_wc_list_attribute($product_id, $listClassDate, 'pa_class-date');

            // we got this far
            return $product;
        } catch (Exception $error) {
            error_log("error creating Bundle for product: " . $class_group);
            error_log($error);

            return false;
        }

    }
 

    /**
     * Creates a string with all the Ids and the funny /1 needed for woosb
     * 
     * @return String 
     */
    private function format_product_ids_string($list_of_ids) {
        $string = '';
        foreach($list_of_ids as $id) {
            $string .= $id . "/1,";
        }
        // remove trailing comma
        $clean_string = rtrim($string, ",");

        return $clean_string;
    }
    /**
     * Determines if the nonce variable associated with the options page is set
     * and is valid.
     * @return boolean False if the field isn't set or the nonce value is invalid;
     *                 otherwise, true.
     */
    private function has_valid_nonce() {
 
        // If the field isn't even in the $_POST, then it's invalid.
        if ( ! isset( $_POST['_wpnonce'] ) ) { // Input var okay.
            return false;
        }
 
        $wpnonce  = wp_unslash( $_POST['_wpnonce'] );
        
        return wp_verify_nonce( $wpnonce, 'createTermClasses' );
 
    }
 
    /**
     * Redirect to the page from which we came (which should always be the
     * admin page. If the referred isn't set, then we redirect the user to
     * the login page.
     */
    private function redirect($result) {
 
        // To make the Coding Standards happy, we have to initialize this.
        if ( ! isset( $_POST['_wp_http_referer'] ) ) { // Input var okay.
            $_POST['_wp_http_referer'] = wp_login_url();
        }
 
        // Sanitize the value of the $_POST collection for the Coding Standards.
        $url = sanitize_text_field(
                wp_unslash( $_POST['_wp_http_referer'] ) // Input var okay.
        );
 
        // Finally, redirect back to the admin page.
        wp_send_json( $result );

        wp_die();
 
    }

    /**
     * Set a bunch of default attributes for terms products.
     * @param string $name | The name (slug).
     */
    private function create_default_attributes($product) {
        try {
            $product->set_status("publish");  // can be publish,draft or any wordpress post status
            $product->set_catalog_visibility('visible'); // add the product visibility status
            $product->set_description("");
    
            $product->set_manage_stock(true); // true or false
            $product->set_stock_status('instock'); // in stock or out of stock value
            $product->set_backorders('no');
    
            $product->set_reviews_allowed(false);
            $product->set_sold_individually(false);
            $product->set_category_ids(array(57)); // array of category ids, You can get category id from WooCommerce Product Category Section of Wordpress Admin
        
            return true;

        } catch(Exception $error) {
            error_log("Error setting default attrs for product: " . $product);
            error_log($error);
            return false;
        }
    }

    /**
     * Add more default attributes to our newly created product
     * @return true or false if error or no
     */
    private function add_more_default_attributes($product_id) {
        try {
            update_post_meta( $product_id, 'woosb_disable_auto_price', 'off' );
            // Add a 10% discount
            update_post_meta( $product_id, 'woosb_discount', sanitize_text_field( 10 ) );
            update_post_meta( $product_id, 'woosb_discount_amount', 0 );
        
            update_post_meta( $product_id, 'woosb_shipping_fee', sanitize_text_field( 0 ) );
            update_post_meta( $product_id, 'woosb_optional_products', 'off' );
        
            update_post_meta( $product_id, 'woosb_manage_stock', 'off' );
            delete_post_meta( $product_id, 'woosb_custom_price' );
            delete_post_meta( $product_id, 'woosb_before_text' );
            delete_post_meta( $product_id, 'woosb_after_text' );

            return true;
        } catch (Exception $error) {
            error_log("Error adding price related default attrs for product: " . $product);
            error_log($error);
            return false;
        }
    }



    /**
     * Get the product attribute ID from the name.
     * @arg value such as 'Notting Hill', and the taxonomy slug eg. 'pa_location'
     * @return boolean TRUE or FALSE if attribute was added or not .
     */
    private function add_wc_attribute($product_id, $name, $taxonomySlug) {
        try {
            // manually link the term and the product
            $result = wp_set_object_terms( $product_id, $name, $taxonomySlug, true );
            // update the product attribute meta
            $att = Array($taxonomySlug =>Array(
                   'name'=> $taxonomySlug,
                   'value'=> $name,
                   'is_visible' => '1',
                   'is_taxonomy' => '1'
                 ));
            // Get any previous value so they can be merged
            $previousAtts = get_post_meta($product_id, '_product_attributes', TRUE);
            // Do we add the attribute or merge it with existing ones ?
            if(is_array($previousAtts) && sizeof($previousAtts) > 0) {
                // merge with old ones
                $done = update_post_meta($product_id, '_product_attributes', array_merge($previousAtts, $att));
            } else {
                //Updating the Post Meta
                $done = update_post_meta($product_id, '_product_attributes', $att);
            }
            // return result
            return $done;

        } catch(Exception $error) {
            error_log("error creating WC Term while creating bundle product. Args were: product ID = " . $product_id . " att was: " .$name . " and " . $taxonomySlug );
            error_log($error);
            return false;
        }
    }


        /**
     * Get the product attribute ID from the name.
     * @arg value such as 'Notting Hill', and the taxonomy slug eg. 'pa_location'
     * @return boolean TRUE or FALSE if attribute was added or not .
     */
    private function add_wc_list_attribute($product_id, $listNames, $taxonomySlug) {
        try {
            // manually link each term and the product
            foreach($listNames as $name) {
                $result = wp_set_object_terms( $product_id, $name, $taxonomySlug, true );
            }
            // update the product attribute meta
            $att = Array($taxonomySlug =>Array(
                   'name'=> $taxonomySlug,
                   'value'=> $listNames,
                   'is_visible' => '1',
                   'is_taxonomy' => '1'
                 ));
            // Get any previous value so they can be merged
            $previousAtts = get_post_meta($product_id, '_product_attributes', TRUE);
            // Do we add the attribute or merge it with existing ones ?
            if(is_array($previousAtts) && sizeof($previousAtts) > 0) {
                // merge with old ones
                $done = update_post_meta($product_id, '_product_attributes', array_merge($previousAtts, $att));
            } else {
                //Updating the Post Meta
                $done = update_post_meta($product_id, '_product_attributes', $att);
            }
            // return result
            return $done;

        } catch(Exception $error) {
            error_log("error creating WC Term while creating bundle product. LIST Args were: product ID = " . $product_id . " att was: " .$name . " and " . $taxonomySlug );
            error_log($error);
            return false;
        }
    }

    /**
     * Helper function. 
     * @args list of WC_Products
     * 
     * @return Array if ids
     */
    private function get_all_ids_for_terms_products($productList) {
        $idList = [];

        foreach($productList as $product) {
            array_push($idList, $product->get_id());
        }

        return $idList;
    }
}



/* Format of the formatted object:

         * (
     *     [Knightsbridge] => Array
     *         (
     *             [Friday] => Array
     *                 (
     *                     [9:30] => Array
     *                         (
     *                             [Tarka Younger Class (18month-3yrs)] => Array
     *                                 (
     *                                     [id] => Array
     *                                         (
     *                                             [0] => 5744
     *                                             [1] => 5738
     *                                             [2] => 5739
     *                                         )
     *                                     [location] => Array
     *                                         (
     *                                             [0] => Knightsbridge
     *                                             [1] => Knightsbridge
     *                                             [2] => Knightsbridge
     *                                         ) 
     *                                     [name] => Array
     *                                         (
     *                                             [0] => Younger Class (18 months to 3yrs) - Friday - 9:30am - Knightsbridge - 13th December
     *                                             [1] => Younger Class (18 months to 3yrs) - Friday - 9:30am - Knightsbridge - 25th October
     *                                             [2] => Younger Class (18 months to 3yrs) - Friday - 9:30am - Knightsbridge - 8th November
     *                                         )
     *                                     [start-time] => Array
     *                                         (
     *                                             [0] => 9:30
     *                                             [1] => 9:30
     *                                             [2] => 9:30
     *                                         )
     *                                     [finish-time] => Array
     *                                         (
     *                                             [0] => 10:15
     *                                             [1] => 10:15
     *                                             [2] => 10:15
     *                                             [3] => 10:15
*/

/*
 Format of a term from the DB
 WP_Term Object
(
    [term_id] => 51
    [name] => Notting Hill
    [slug] => nottinghill
    [term_group] => 0
    [term_taxonomy_id] => 51
    [taxonomy] => pa_location
    [description] => St Francis of Assisi Church, Pottery Lane, W11 4NQ
    [parent] => 0
    [count] => 689
    [filter] => raw
)
*/

?>