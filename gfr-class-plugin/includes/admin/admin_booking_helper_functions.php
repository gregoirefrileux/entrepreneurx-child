<?php

/*
 *
 * Author : Gregoire Frileux
 * This file is intended to host "general functions" related to the admin interface
 * It is helpful for functions that are not immediatly related to a particular page, such as adding custom CSS
 * to the admin, or adding columns to certain screens
 *
 */


/*
 * Add new column to the WooCommerce Products Admin
 * It shows a "Class Date" column when viewing products
 */


add_filter('manage_edit-product_columns', 'gfr_show_class_date_column', 15);

function gfr_show_class_date_column($columns_array)
{
    // I want to display Class Date column just after the product name column
    return array_slice($columns_array, 0, 3, true)
    + array('classdate' => 'Class Date')
    + array_slice($columns_array, 3, null, true);
}

add_action('manage_product_posts_custom_column', 'gfr_populate_class_date_column', 10, 2);

function gfr_populate_class_date_column($column, $postid)
{
    if ($column == 'classdate') {
        $meta = get_the_terms(get_the_ID(), 'pa_class-date'); // taxonomy name
        if (!is_wp_error($meta) && $meta[0]) {
            echo $meta[0]->name;
        } else {
            echo 'no date';
        }
    }
}

// make sortable
add_filter('manage_edit-product_sortable_columns', 'gfr_class_date_sortable');
function gfr_class_date_sortable($a)
{
    return wp_parse_args(array('classdate' => 'taxonomy-pa_class-date'), $a);
}

// Now for the sorting function

add_filter('posts_clauses', 'gfr_orderby_taxonomy', 10, 2);

function gfr_orderby_taxonomy($clauses, $wp_query)
{
    if (!is_admin()) {
        return $clauses;
    }
    global $wpdb;

    if (isset($wp_query->query['orderby']) && (strpos($wp_query->query['orderby'], 'taxonomy-') !== false)) {
        $tax = preg_replace("/^taxonomy-/", "", $wp_query->query['orderby']);
        $clauses['join'] .= "LEFT OUTER JOIN {$wpdb->term_relationships} ON {$wpdb->posts}.ID={$wpdb->term_relationships}.object_id
LEFT OUTER JOIN {$wpdb->term_taxonomy} USING (term_taxonomy_id)
LEFT OUTER JOIN {$wpdb->terms} USING (term_id)";
        $clauses['where'] .= " AND (taxonomy = '" . $tax . "' OR taxonomy IS NULL)";
        $clauses['groupby'] = "object_id";
        $clauses['orderby']  = "GROUP_CONCAT({$wpdb->terms}.name ORDER BY name ASC) ";
        $clauses['orderby'] .= ('ASC' == strtoupper($wp_query->get('order'))) ? 'ASC' : 'DESC';
    }
    return $clauses;
}

/*
 *
 * Simple function to sort for;atted array of classes by start time
 *
 */

function gfr_compare_start_time($a, $b)
{
    return strnatcmp($a['start-time'], $b['start-time']);
}
