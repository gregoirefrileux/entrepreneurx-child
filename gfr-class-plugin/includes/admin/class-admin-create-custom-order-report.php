<?php
/**
 * GFR - March 2020
 * Performs all functions related to automatically creating a custom report of WooCommerce orders
 */
 
/**
 */
class AdminCreateCustomOrderReport {
 
    /**
     * No  constructor as we don't have to register a hook or action to WP.
     */
 
    /**
     * Validates the incoming nonce value, verifies the current user has
     * permission to generate the report.
     */
    public function create_custom_report() {
        // First, validate the nonce and verify the user as permission to save.
        if ( ! ( $this->has_valid_nonce() && current_user_can( 'edit_users' ) ) ) {
            $this->redirect('Invalid form');
        }
 
        // If the above are valid, sanitize and save the option.
        if ( null !== wp_unslash( $_GET['month'] ) ) {
 
            $selected_month = sanitize_text_field( $_GET['month'] );
            // create the report
            try {
                $this->create_csv($selected_month);
            } catch (Exception $error) {
                // Return error message
                $this->redirect('Could not generate order report');
            }
        } else {
            // issue with the posted form - return error
            $this->redirect('There was an error with the posted data');
        }
    }

    /**
     * Creates the header and the outputs the file to the browser.
     */
    private function create_csv($month) {

        ob_end_clean();

        $csv = $this->generate_csv_data($month);

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"custom_report.csv\";" );
        header("Content-Transfer-Encoding: binary");

        print $csv;
        exit;
    
    }

    /**
     * Creates the header and the outputs the file to the browser.
     */
    private function generate_csv_data($month) {
        // Generate header
        $content = "id,orderId,DatePaid,classDate,status,orderTotal,productAmount,classLocation,productName,classType,quantity\n";
        
        // Then let's get the orders
        $args = array(
            'limit' => 800,
            'orderby' => 'date',
            'order' => 'DESC',
            'date_paid' => date('Y-m-d', strtotime($month . '-01')) . '...' . date('Y-m-t', strtotime($month . '-31')),
        );
        $orders = wc_get_orders( $args );

        $i=1;
        foreach($orders as $order) {
            // we now need to loop each product
            foreach($order->get_items() as  $item_id => $item_product) {
                $product_id = $item_product->get_product_id();
                $product = wc_get_product($product_id);

                if ($product && $this->should_row_be_included($product)) {
                    $row = array(
                        $id             = $i,
                        $orderId        = $order->get_id(),
                        $datePaid       = $order->get_date_paid()->date('d/m/Y'),
                        $classDate      = date('d/m/Y', strtotime($product->get_attribute('pa_class-date'))),
                        $status         = $order->get_status(),
                        $order_total    = $order->get_total(),
                        $product_amount = $item_product->get_total(),
                        $classLocation  = $product->get_attribute('pa_location'),
                        $productName    = $item_product->get_name(),
                        $classType      = $product->get_attribute('pa_class-name'),
                        $quantity       = $item_product->get_quantity(),
                    );
                    // increment row;
                    $i++;
                    // add data to the file
                    $content .= implode(", ", $row);
                    // add a new line
                    $content .= "\n";
                } else {
                    // we must be on a discount or something else. we ignore
                }
            }
        }


        return $content;
    }

    /**
     * Determines if a row should be added to the report. Takes in a WC_Product.
     * @return boolean False if the field isn't set or the nonce value is invalid;
     *                 otherwise, true.
     */
    private function should_row_be_included($product) {
 
        if( in_array(57, $product->get_category_ids()) ) {
            // term classes should not be included since every individual class is already printed in the report
            return false;
        }

        return true;
    }

    /**
     * Determines if the nonce variable associated with the options page is set
     * and is valid.
     * @return boolean False if the field isn't set or the nonce value is invalid;
     *                 otherwise, true.
     */
    private function has_valid_nonce() {
 
        // If the field isn't even in the $_POST, then it's invalid.
        if ( ! isset( $_GET['_wpnonce'] ) ) { // Input var okay.
            return false;
        }
 
        $wpnonce  = wp_unslash( $_GET['_wpnonce'] );
        
        return wp_verify_nonce( $wpnonce, 'getCustomOrderReport' );
 
    }
 
    /**
     * Redirect to the page from which we came (which should always be the
     * admin page. If the referred isn't set, then we redirect the user to
     * the login page.
     */
    private function redirect($result) {
 
        // To make the Coding Standards happy, we have to initialize this.
        if ( ! isset( $_['_wp_http_referer'] ) ) { // Input var okay.
            $_GET['_wp_http_referer'] = wp_login_url();
        }
 
        // Sanitize the value of the $_POST collection for the Coding Standards.
        $url = sanitize_text_field(
                wp_unslash( $_GET['_wp_http_referer'] ) // Input var okay.
        );
 
        // Finally, redirect back to the admin page.
        wp_redirect( $url . '&error=' . strval($result) );

        wp_die();
 
    }


}


?>