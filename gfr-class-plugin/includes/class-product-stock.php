<?php

class Product_Stock {

  public function correct() {
    error_log("Running product stock correct...");
    $draft_orders =  $this->_find_draft_orders();
    $order_items = $this->_get_items_from_orders( $draft_orders );
    $this->_increase_stock( $order_items );
    $this->_delete_orders( $draft_orders );
  }

  private function _find_draft_orders() {
    global $wpdb;
    $fifteen_mins_ago_datetime = date('Y-m-d H:i:s',  time() - 900); // - 15 minutes
    // $fifteen_mins_ago_datetime = date('Y-m-d H:i:s',  time() - 1); // - 1 second for debugging
    $query = 'SELECT * FROM ' . $wpdb->posts . ' WHERE post_type = "shop_order" AND post_status = "auto-draft" AND post_date > "2019-11-24 00:00:00" AND post_date < "' . $fifteen_mins_ago_datetime . '"';
    error_log( $query );
    $results = $wpdb->get_results( $query );
    return $results;
  }

  private function _get_items_from_orders( $orders ) {
    $items = [];
    foreach ($orders as $post) {
      $_order = wc_get_order( $post->ID );
      $items = array_merge( $items, $_order->get_items() );
    }
    return $items;
  }

  private function _increase_stock( $order_items ) {
    foreach( $order_items as $item ) {
      $old_value = (int) get_post_meta( $item->get_product_id(), "_stock", true );
      $new_value = $old_value + (int) $item->get_quantity();
      error_log( "Updating " . $item->get_product_id() . " stock to " . $new_value );
      update_post_meta( $item->get_product_id(), "_stock", $new_value );
    }
  }

  private function _delete_orders( $draft_orders ) {
    error_log( "Deleting orders..." );
    foreach ( $draft_orders as $order )
      wp_delete_post( $order->ID, true );
  }

}

?>
