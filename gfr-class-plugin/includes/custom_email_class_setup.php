<?php

/**
 *  Add a custom email to the list of emails WooCommerce should load
 *
 * @since 0.1
 * @param array $email_classes available email classes
 * @return array filtered available email classes
 */
function gfr_custom_reminder_email_class_declaration( $email_classes ) {

    // include our custom email to remind parents about a class tomorrow
    require( 'class-wc-email-reminder-for-class-tomorrow.php' );
    // include our custom email reminder for parents class
    require( 'class-wc-email-unpaid-order-reminder.php' );

    // add the email class to the list of email classes that WooCommerce loads
    $email_classes['WC_Class_Reminder_Email'] = new WC_Class_Reminder_Email();
    $email_classes['WC_Class_UnpaidOrderReminder_Email'] = new WC_Class_UnpaidOrderReminder_Email();
    
    return $email_classes;

}
add_filter( 'woocommerce_email_classes', 'gfr_custom_reminder_email_class_declaration' );

?>