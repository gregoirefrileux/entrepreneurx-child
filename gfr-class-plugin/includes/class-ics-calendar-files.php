<?php

class GFR_ICS_CALENDAR_HELPER
{
    const DT_FORMAT = 'Ymd\THis';

    protected $properties = array();
    private $events = array();

    /**
     * Constructor
     */
    public function __construct()
    {
        add_action('wp_ajax_create_ics', array( $this, 'handle_form' ));
        add_action('wp_ajax_nopriv_create_ics', array( $this, 'handle_form' ));
    }

    public function handle_form()
    {
        // First get the class id from the URL param
        $class_id   = isset($_GET['class_id']) ? intval($_GET['class_id']) : null;

        if(!$class_id || $class_id == 0 || $class_id == 1) {
            // bad request - the class id was not a valid numeric entry
            error_log('[handle_form] Calendar invite download fail due to bad class_id: '.$class_id);
            wp_die();
            return;
        }

        $dates_array = get_class_dates($class_id);
        if(sizeof($dates_array) == 0) {
            // bad request - we need at least 1 date to create a calendar invite
            error_log('[handle_form] No date found for classs id = '.$class_id);
            wp_die();
            return;
        }

        // Get the details that are common across all dates
        $common_details = $this->get_common_detail_for_class($class_id);
        if(!$common_details) {
            // Could not retrive the class attributes
            error_log('[handle_form] Failed to create ics invite. Could not retrieve basic attributes for class id:'.$class_id);
            wp_die();
            return;
        }

        foreach ($dates_array as $date) {
            $class_details = $this->get_start_and_end_time_for_date($common_details, $date);
            $this->events[] = array(
                'location' => $class_details['address'],
                'description' => 'A Tarka class',
                'dtstart' => $class_details['date_start'],
                'dtend' => $class_details['date_end'],
                'summary' => 'TARKA - ' . $class_details['class_name'],
                'url' => 'https://tarkalondon.com'
            );
        }
        // return the file to the browser
        $this->return_ics();
    }

    /**
     * Retrieves the WooCommerce product and attributes for a given class ID
     * Creates an array of the needed attributes to create the iCal invite.
     * Returns null if class id is invalid
     */
    private function get_common_detail_for_class($product_id) {

        // get the WC Product
        $product        = wc_get_product($product_id);
        if(!$product) {
            error_log('[get_common_detail_for_class] Could not retrieve product for passed id = '.$product_id);
            return null;
        }
        // Get its attributes
        $time_start     = get_cust_att($product, 'pa_start-time');
        $time_end       = get_cust_att($product, 'pa_finish-time');
        $class_name     = get_cust_att($product, 'pa_class_name');
        $location       = get_cust_att($product, 'pa_location');
        $address        = strip_tags(get_cust_att_description($product, 'pa_location'));
        if($address == '') {
            // if no address has been set in WP Admin for this location - simply pass the location name
            $address = $location;
        }

        $class_detail = array(
            'time_start'    => $time_start,
            'time_end'      => $time_end,
            'class_name'    => $class_name,
            'location'      => $location,
            'address'       => $address,
        );

        return $class_detail;
    }

    /**
     * Creates the start and end time of the event for a given date
     * Appends teh extra params to the passed array
     */
    private function get_start_and_end_time_for_date($class_detail, $class_date) {
        $tz = new DateTimeZone('Europe/London');
        // then set up the data needed for the event
        $date_start = DateTime::createFromFormat('Y-m-d G:i', $class_date . ' ' . $class_detail['time_start'], $tz);
        $date_end = DateTime::createFromFormat('Y-m-d  G:i', $class_date . ' ' . $class_detail['time_end'], $tz);

        $class_detail = array(
            'date_start'    => $date_start,
            'date_end'      => $date_end,
        );

        return $class_detail;
    }

    public function return_ics()
    {
        header('Content-Type: text/calendar; charset=utf-8');
        header('Content-Disposition: attachment; filename=invite.ics');

        echo $this->to_string();
        wp_die();
    }
    /**
     * ICS CLASS CODE
     *
     */
  
  
    public function to_string()
    {
        $rows = $this->build_props();
        return implode("\r\n", $rows);
    }
  
    private function build_props()
    {
        // Build ICS properties - add header
        $ics_props = array(
        'BEGIN:VCALENDAR',
        'VERSION:2.0',
        'PRODID:-//hacksw/handcal//NONSGML v1.0//EN',
        'CALSCALE:GREGORIAN',
      );
  
        foreach ($this->events as $event) {
            $ics_props = $this->add_event($ics_props, $event);
        }
      
        $ics_props[] = 'END:VCALENDAR';

        return $ics_props;
    }

    private function add_event($ics_props, $event)
    {
        $ics_props[] = 'BEGIN:VEVENT';
        // Build ICS properties - add header
        $props = array();
        foreach ($event as $k => $v) {
            $v = $this->sanitize_val($v, $k);
            $props[strtoupper($k . ($k === 'url' ? ';VALUE=URI' : ''))] = $v;
        }
    
        // Set some default values
        $props['DTSTAMP'] = $this->format_date(new DateTime('now', new DateTimeZone('Europe/London')));
        $props['UID'] = uniqid();
        
        // Append properties
        foreach ($props as $k => $v) {
            $ics_props[] = "$k:$v";
        }
    
        // Build ICS properties - add footer
        $ics_props[] = 'END:VEVENT';
        
        return $ics_props;
    }
  
    private function sanitize_val($val, $key = false)
    {
        switch ($key) {
        case 'dtend':
        case 'dtstamp':
        case 'dtstart':
          $val = $this->format_date($val);
          break;
        default:
          $val = $this->escape_string($val);
      }
  
        return $val;
    }
  
    private function format_date($dateTime)
    {
        return $dateTime->format(self::DT_FORMAT);
    }
  
    private function escape_string($str)
    {
        return preg_replace('/([\,;])/', '\\\$1', $str);
    }
}

$ics = new GFR_ICS_CALENDAR_HELPER();
