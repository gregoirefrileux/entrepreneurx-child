<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class WC_Waiting_List_Admin_Screen {

	/**
	 * Constructor
	 */
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'admin_menu' ) );
	}

	/**
	 * Admin Menu
	 */
	public function admin_menu() {
		$page = add_submenu_page( 'woocommerce', __( 'Waiting List', 'woocommerce-waiting-list' ), __( 'Waiting List', 'woocommerce-waiting-list' ), 'manage_options', 'woocommerce_waiting_list', array( $this, 'output' ) );
	}

	/**
	 * Admin Screen output
	 */
	public function output() {

    //Create an instance of our package class...
    $testListTable = new Waiting_List_Table();
    //Fetch, prepare, sort, and filter our data...
    $testListTable->prepare_items();

    ?>
    <div class="wrap">
        <h2>Waiting List</h2>
        <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
        <form id="waiting-list-filter" method="get">
            <!-- For plugins, we also need to ensure that the form posts back to our current page -->
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
            <!-- Now we can render the completed list table -->
            <?php $testListTable->display() ?>
        </form>

    </div>
    <?php
	}

}

new WC_Waiting_List_Admin_Screen();
