<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


/**
 * A custom Expedited Order WooCommerce Email class
 *
 * @since 0.1
 * @extends \WC_Email
 */
class WC_Class_UnpaidOrderReminder_Email extends WC_Email {

    /**
     * Set email defaults
     *
     * @since 0.1
     */
    public function __construct() {

        // set ID, this simply needs to be a unique name
        $this->id = 'gfr_class_unpaid_order_reminder_email';

        // this is the title in WooCommerce Email settings
        $this->title = 'Tarka Class Unpiad Order reminder';

        // this is the description in WooCommerce email settings
        $this->description = 'This is the Unpaid order reminder email, sent to parents every day for 5 days after creating an order for them';

        // these are the default heading and subject lines that can be overridden using the settings
        $this->heading = 'Tarka Pending Order Reminder';
        $this->subject = 'Tarka Pending Order Reminder';
        $this->customer_email = true;

        // these define the locations of the templates that this email should use, we'll just use the new order template since this email is similar
        $this->template_html  = '/emails/gfr_unpaid_order_reminder_email.php';
        $this->template_plain = 'emails/plain/gfr_unpaid_order_reminder_email.php';
        $this->template_base = get_stylesheet_directory() . '/gfr-class-plugin/templates';

        // Trigger when the CRON runs
        add_action( 'gfr_send_unpaid_order_reminder_email', array( $this, 'trigger' ) , 10, 2);

        // Call parent constructor to load any other defaults not explicity defined here
        parent::__construct();

        // this sets the recipient to the settings defined below in init_form_fields()
        $this->recipient = $this->get_option( 'recipient' );

        // if none was entered, just use the WP admin email as a fallback
        if ( ! $this->recipient )
            $this->recipient = 'info@tarkalondon.com';
    }


    /**
     * Determine if the email should actually be sent and setup email merge variables
     *
     * @since 0.1
     * @param int $order_id
     */
    public function trigger( $parent_data ) {

        // TODO : improve on the basic validation rule below: $parent_data must not be null, must have key params, etc.
        if ( ! $parent_data['parent_email'] )
            return;

        $this->object = $parent_data;
        $this->parent_data = $parent_data;
        $this->recipient = $parent_data['parent_email'];

        if ( ! $this->is_enabled() || ! $this->get_recipient() ) {
			return;
		}

        // all ok - send the email!
        $this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
    }

        /**
     * get_content_html function.
     *
     * @since 0.1
     * @return string
     */
    public function get_content_html() {
		return wc_get_template_html( $this->template_html, array(
            'order'         => $this->object,
            'parent_data'   => $this->parent_data,
			'email_heading' => $this->get_heading(),
			'sent_to_admin' => false,
			'plain_text'    => false,
			'email'			=> $this
		), '', $this->template_base );
    }


    /**
     * get_content_plain function.
     *
     * @since 0.1
     * @return string
     */
    public function get_content_plain() {
		return wc_get_template_html( $this->template_plain, array(
			'order'         => $this->object,
            'parent_data'   => $this->parent_data,
			'email_heading' => $this->get_heading(),
			'sent_to_admin' => false,
			'plain_text'    => true,
			'email'			=> $this
		), '', $this->template_base );
    }


        /**
     * Initialize Settings Form Fields
     *
     * @since 0.1
     */
    public function init_form_fields() {

        $this->form_fields = array(
            'enabled'    => array(
                'title'   => 'Enable/Disable',
                'type'    => 'checkbox',
                'label'   => 'Enable this email notification',
                'default' => 'yes'
            ),
            'recipient'  => array(
                'title'       => 'Recipient(s)',
                'type'        => 'text',
                'description' => sprintf( 'Enter recipients (comma separated) for this email. Defaults to <code>%s</code>.', esc_attr( get_option( 'admin_email' ) ) ),
                'placeholder' => '',
                'default'     => ''
            ),
            'subject'    => array(
                'title'       => 'Subject',
                'type'        => 'text',
                'description' => sprintf( 'This controls the email subject line. Leave blank to use the default subject: <code>%s</code>.', $this->subject ),
                'placeholder' => '',
                'default'     => ''
            ),
            'heading'    => array(
                'title'       => 'Email Heading',
                'type'        => 'text',
                'description' => sprintf( __( 'This controls the main heading contained within the email notification. Leave blank to use the default heading: <code>%s</code>.' ), $this->heading ),
                'placeholder' => '',
                'default'     => ''
            ),
            'email_type' => array(
                'title'       => 'Email type',
                'type'        => 'select',
                'description' => 'Choose which format of email to send.',
                'default'     => 'html',
                'class'       => 'email_type',
                'options'     => array(
                    'plain'     => 'Plain text',
                    'html'      => 'HTML', 'woocommerce',
                    'multipart' => 'Multipart', 'woocommerce',
                )
            )
        );
    }

} 