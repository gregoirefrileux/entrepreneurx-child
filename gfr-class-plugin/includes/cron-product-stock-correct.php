<?php

require_once('class-product-stock.php');

// This links the main function to the hook declared in the CRON schedule
add_action( 'woocommerce_correct_admin_created_order_stock', 'gfr_cron_correct_class_stock' );

function gfr_cron_correct_class_stock() {
	$ps = new Product_Stock();
	$ps->correct();
}

?>