<?php

/*
 * HELPER Function to retrieve the custom attribute value of a WooCommerce Product
 * args: products: an array of WooCommerce Products
 * term: a string indicating which Custom Attribute to search
 *
 * returns : a string (hopefully) of the custom attribute value
 */

function get_cust_att($product, $attribute) {

    $value = $product->get_attribute( $attribute );

    return($value);

}

/*
 * HELPER Function to retrieve the custom attribute description of a WooCommerce Product
 * args: product: a WooCommerce Products
 * term: a string indicating which Custom Attribute to search
 *
 * returns : a string (hopefully) of the custom attribute description
 */

function get_cust_att_description($product, $attribute) {

    $values = wc_get_product_terms( $product->get_id(), $attribute, array( 'fields' =>  'all' ) );
    if( $values ){
        foreach ( $values as $term ){
            return term_description( $term->term_id );
        }
    }

    return($value);

}

/*
 * HELPER Function to retrieve the custom attribute value of a WooCommerce Product
 * term: a string indicating which Custom Attribute to search (for example Location, or Term Name)
 *
 * returns : an array strings of the custom attribute values
 */

function get_all_cust_att_values($attribute) {

    $raw_list = get_terms( $attribute );
    $list = [];
    foreach($raw_list as $term) {
        array_push($list, array("name" =>$term->name, "slug" =>$term->slug));
    }
    return($list);

}

/**
 *
 * Function to retrieve classes to display in the front end Week calendar
 * args: WEEK  PARAMETERS: date of the satrt of the week and the end of the week
 * args: LOCATION: the location to retrieve products from - returns all if not passed or null or == "all"
 * args: CLASSNAME: the CLASSNAMES for which to pull products for
 *
 * @return array of classes in format that the Week Calendar list will be able to display
 *
 */

function get_classes_for_week($week_start, $week_end, $location, $classname, $age_limit) {

    // First build the query
	$args = array(
        'product_cat' => 'standard-class,holiday-class',// here we only load the "Term Products" that should live inside the Term Class Cat
        'posts_per_page' => -1,
        'status' => 'publish', //only published products
    );

    // Second add the location - if the user wants "all" we simply omit the location from the search
    if($location && $location !== 'all') {
        // we need to add the specific location asked to the query
        $args['tax_query'] = array();
        array_push($args['tax_query'], array(
         'taxonomy' 	=> 'pa_location',
         'terms' 		=> $location,
         'field' 		=> 'slug',
         'operator' 	=> 'IN'
        ));
     }
     if($classname && $classname !== 'all') {
        // we need to add the specific location asked to the query
        if (! $args['tax_query'] ) {
            $args['tax_query'] = array();
        }
        array_push($args['tax_query'], array(
         'taxonomy' 	=> 'pa_class-name',
         'terms' 		=> $classname,
         'field' 		=> 'slug',
         'operator' 	=> 'IN'
        ));
     }
     if($age_limit && $age_limit !== 'all') {
        // we need to add the specific location asked to the query
        if (! $args['tax_query'] ) {
            $args['tax_query'] = array();
        }
        array_push($args['tax_query'], array(
         'taxonomy' 	=> 'pa_age-limit',
         'terms' 		=> $age_limit,
         'field' 		=> 'slug',
        ));
     }

    // Now we retrieve the products
    $products = wc_get_products($args);

    // we filter the products for this week only
    $filtered_products = filter_single_week($products, $week_start, $week_end);

    $formatted_products = format_single_class_product_list($filtered_products);

    return $formatted_products;
}


/*
 *
 * Function to retrieve Term products
 * args: products: an array of WooCommerce Products
 *
 *
 * returns: array of classes in format:
 * $classes = array(
    [2019-01-12] =&gt; Array
        (
            [9:00] =&gt; Array
                (
                    [Notting Hill] =&gt; Array
                        (
                            [Tarka x3 Hr Weekly Session] =&gt; Array
                                (
                                    [943] =&gt; Array
                                        (
                                            [id] => 943
                                            [location] => Notting Hill
                                            [name] => Tarka x3 Hr Weekly Session – Tuesday – 3H – 9AM – Notting Hill - Second half term (test)
                                            [stock] => 2
                                        )

                                    [1] =&gt; Array
                                        (
                                            [id] => 911
                                            [location] => Notting Hill
                                            [name] => Tarka x3 Hr Weekly Session – Tuesday – 3H – 9AM – Notting Hill - First half term
                                            [stock] => 4
                                        )

                                )

                        )

                )

        )

    [2019-01-22] => Array
        (
            [9:30] =&gt; Array
                (
                    [Notting Hill] =&gt; Array
                        (
                            [Tarka Younger Class (18month-3yrs)] =&gt; Array
                                (
                                    [939] =&gt; Array
                                        (
                                            [id] =&gt; 939
                                            [location] =&gt; Notting Hill
                                            [name] =&gt; Tarka x45min Weekly Session – Thursday – 45min – 9:30AM – Notting Hill - First half term
                                        )

                                )

                        )

                )

        )

)

 *
 */

function format_single_class_product_list($WCproducts) {

    // a simple list of all products
    $product_list = [];
    // now we initialize the final list
    $class_list = [];

    foreach($WCproducts as $WCproduct) {
        array_push($product_list, [
            get_cust_att($WCproduct, 'pa_class-date') => array (
                get_cust_att($WCproduct, 'pa_start-time') => array(
                    get_cust_att($WCproduct, 'pa_location') => array(
                        get_cust_att($WCproduct, 'pa_class-name') => array(
                            'id'        => $WCproduct->get_id(),
                            'location'  => get_cust_att($WCproduct, 'pa_location'),
                            'name'      => $WCproduct->get_title(),
                            'class-name'=> get_cust_att($WCproduct, 'pa_class-name'),
                            'stock'     => $WCproduct->get_stock_quantity(),
                            'term-class'=> get_cust_att($WCproduct, 'pa_term-class'),
                            'start-time'=> get_cust_att($WCproduct, 'pa_start-time'),
                            'finish-time'=> get_cust_att($WCproduct, 'pa_finish-time'),
                            'day-of-the-week'=> get_cust_att($WCproduct, 'pa_day-of-the-week'),
                            'term-name'=> get_cust_att($WCproduct, 'pa_term-name'),
                            'class-date'=> get_cust_att($WCproduct, 'pa_class-date'),
                            'price'     => $WCproduct->get_price(),
                            'category'     => $WCproduct->get_category_ids(),
                            'age-group'=> null // to be added later
                            )
                        )
                    )
                )
            ]
        );
    }

    for($i=0; $i < sizeof($product_list); $i++) {
        if ($i === 0) {
            $class_list = $product_list[0];
        } else {
            $class_list = array_merge_recursive($class_list, $product_list[$i]);
        }
    }

    return($class_list);

}


/*
 *
 * This simple function displays a form with the list of classes remaining for the term
 * This is used in the Calendar, when looking at a class, and clicking "Book Multiple Dates"
 * Input : remaining_classes is an array of WC_Product_Simple objects
 *
 * It basically display a list of checkboxes, one per class
 *
 */

function display_list_of_single_classes($remaining_classes) {

    $remaining_html = '';
    $remaining_html .= '<div class="remaining-classes-form" data-hj-whitelist>';

    $remaining_html .= "<p>Please select the dates you would like to book (showing the current week/term only) :</p>";

    if($remaining_classes && is_array($remaining_classes)) {
        foreach($remaining_classes as $class) {
            $remaining_html .= '
            <input
              data-hj-whitelist
              type="checkbox"
              id="remaining-class-checkbox"
              value="' . $class->get_id() . '"
              data-product-id="' . $class->get_id() . '"
              data-product-price="' . $class->get_price() . '"
               ' . (!$class->is_in_stock() ? 'disabled' : '') .
             '>';
            $remaining_html .= '
            <label class="' . (!$class->is_in_stock() ? 'disabled' : '') . '">' .
              date_i18n('dS M Y',strtotime(get_cust_att($class, 'class-date'))) . ' - £' . $class->get_price() .
            '</label>';
            if ( !$class->is_in_stock() )
              $remaining_html .= '<div data-product-id="' . $class->get_id() . '" class="join-waitlist-inner button"> JOIN WAITLIST</div>';
            $remaining_html .= '<br>';
        }
    }

    $remaining_html .= '<input data-hj-whitelist type="hidden" id="remaining-classes-total" data-product-id="" data-product-price="" data-product-count="">';
    $remaining_html .= '</div>';

    return $remaining_html;
}



/*
 * HELPER Function to display nicely all the dates included in a TERM product
 * product_id: the id of the WooCommerce Grouped product
 *
 * returns : some HTML showing the dates of the classes included in this grouped product
 */


function get_class_dates($product_id) {

    $woosb_arr  = array();
    $classes_dates = [];
    // Code nicked from the plugin itself
    if ( ( $woosb_ids = get_post_meta( $product_id, 'woosb_ids', true ) ) ) {
        $woosb_items = explode( ',', $woosb_ids );
        if ( is_array( $woosb_items ) && count( $woosb_items ) > 0 ) {
            foreach ( $woosb_items as $key => $woosb_item ) {
                // Every $woosb_item has a format of "prod_id/qty" which is weird.. Anyway we split that
                $woosb_item_arr = explode( '/', $woosb_item );
                // Get the actual individual WooCoomerce product for that class
                $product = wc_get_product($woosb_item_arr[0]);
                // Now its date
                if ($product) {
                    $date = get_cust_att( $product, 'pa_class-date');
                    $classes_dates[$key] = $date;
                }
            }
        }
    } else {
        // for a simple product, just try and ertrieve its date attribue
        $product = wc_get_product($product_id);
        if ($product) {
            $date = get_cust_att($product, 'pa_class-date');
            if($date && $date !== '') {
                $classes_dates[] = $date;
            }
        }
    }
    return($classes_dates);
}

/*
 * HELPER Function to return all the products IDs of classes included in a WOOSB Bundle
 * product_id: the id of the WooCommerce Grouped product
 *
 * returns : an array of IDs or null
 */


function get_class_ids_from_bundle($product_id) {

    $woosb_arr  = array();
    $product_ids = [];
    // Code nicked from the plugin itself
    if ( ( $woosb_ids = get_post_meta( $product_id, 'woosb_ids', true ) ) ) {
        $woosb_items = explode( ',', $woosb_ids );
        if ( is_array( $woosb_items ) && count( $woosb_items ) > 0 ) {
            foreach ( $woosb_items as $key => $woosb_item ) {
                // Every $woosb_item has a format of "prod_id/qty" which is weird.. Anyway we split that
                $woosb_item_arr = explode( '/', $woosb_item );
                // Get the actual individual WooCoomerce product for that class
                $product = wc_get_product($woosb_item_arr[0]);
                // Now its date
                if ($product) {
                    array_push($product_ids, $product->get_id());
                } else {
                    // this element of the bundle is not a product ??? do nothing
                }
            }
        } else {
            // no classes in this bundle
            return null;
        }
    } else {
        // the product given is not a bundle
        return null;
    }

    return($product_ids);
}



/*
 * HELPER Function to display nicely all the dates included in a TERM product
 * product_id: the id of the WooCommerce Grouped product
 *
 * returns : some HTML showing the dates of the classes included in this grouyped product
 */


function display_class_dates($product_id) {

    $date_array = get_class_dates($product_id);

    return( implode(', ', $date_array));

}


/*
 *
 * This Function is a helper that will get the TERM CLASS linked to a single class
 * The idea is to fetch a TERM CLASS which has the same day, start time, and contains the term date
 *
 * It returns a class ID or False (false if there is no associated class)
 *
 */
function get_term_class_for_single_class($class) {

    // First build the query
	$args = array(
        'product_cat' => 'term-class,holiday-bundle-classes',// here we only load the "Term Products" that should live inside the Term Class Cat
        'posts_per_page' => -1,
        'status' => 'publish',    //only published products
        'tax_query' => array(
            array(
                'taxonomy' 		=> 'pa_day-of-the-week',
                'terms' 		=> $class['day-of-the-week'],
                'field' 		=> 'slug',
                'operator' 		=> 'IN'
                ),
            array(
                'taxonomy' 		=> 'pa_start-time',
                'terms' 		=> $class['start-time'],
                'field' 		=> 'slug',
                'operator' 		=> 'IN'
                ),
            array(
                'taxonomy' 		=> 'pa_location',
                'terms' 		=> $class['location'],
                'field' 		=> 'name',
                'operator' 		=> 'IN'
            ),
            array(
                'taxonomy' 		=> 'pa_class-date',
                'terms' 		=> $class['class-date'],
                'field' 		=> 'slug',
                'operator' 		=> 'IN'
                ),
            ),
        );


    // Now we retrieve the products
    $products = wc_get_products($args);

    // check that we retrieved products, and possibly filter only for the "Full Term" one
    if (sizeof($products) > 0) {
        // w got several products - but how many?
        if (sizeof($products) === 1) {
            // we have a single "Full term" product for this class - easy we simply return it
            return $products[0];
        } else {
            // we have several products , which mean we have a "Half term" and "Full term" version
            // we just need the "Full Term" product
            foreach ($products as $product) {
                if($product->get_attribute("pa_term-class") === 'Full term') {
                    return $product;
                }
            }
        }
    } else {
        // no product returned in the query
        return false;
    }


}

/*
 *
 * This Function is a helper that will retrieve the remaining classes in the term for a given single class
 * It takes the class we're interested in
 *
 * It returns false if this is the last date of the term, or an array of WC_Product single classes if not
 *
 */

function get_remaining_dates_in_term_for_single_class($class) {

    // If the current class is a holiday class, we need to build a query which takes all the classes at the same location
    // and time, but not necessarily the same day of the week

    if( in_array(37, $class['category'], true) ) { // searching for 37 which is the ID of the Holiday Class category
        // First build the query
        $args = array(
            'product_cat' => 'holiday-class',// here we only load the "Standard Products" and "Holiday Prodcuts"
            'posts_per_page' => -1,
            'status' => 'publish',    //only published products
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' 		=> 'pa_location',
                    'terms' 		=> $class['location'],
                    'field' 		=> 'name',
                    'operator' 		=> 'IN'
                    ),
                array(
                    'taxonomy' 		=> 'pa_class-name',
                    'terms' 		=> $class['class-name'],
                    'field' 		=> 'name',
                    'operator' 		=> 'IN'
                    ),
                array(
                    'taxonomy' 		=> 'pa_term-name',
                    'terms' 		=> $class['term-name'],
                    'field' 		=> 'name',
                    'operator' 		=> 'IN'
                    ),
                array(
                    'taxonomy' 		=> 'pa_start-time',
                    'terms' 		=> $class['start-time'],
                    'field' 		=> 'slug',
                    'operator' 		=> 'IN'
                    ),
                ),
            );


    } else {

        // First build the query
        $args = array(
            'product_cat__in' => 'standard-class', // here we only load the "Standard Products" and "Holiday Prodcuts"
            'posts_per_page' => -1,
            'status' => 'publish',    //only published products
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' 		=> 'pa_location',
                    'terms' 		=> $class['location'],
                    'field' 		=> 'name',
                    'operator' 		=> 'IN'
                    ),
                array(
                    'taxonomy' 		=> 'pa_class-name',
                    'terms' 		=> $class['class-name'],
                    'field' 		=> 'name',
                    'operator' 		=> 'IN'
                    ),
                array(
                    'taxonomy' 		=> 'pa_term-name',
                    'terms' 		=> $class['term-name'],
                    'field' 		=> 'name',
                    'operator' 		=> 'IN'
                    ),
                array(
                    'taxonomy' 		=> 'pa_day-of-the-week',
                    'terms' 		=> $class['day-of-the-week'],
                    'field' 		=> 'slug',
                    'operator' 		=> 'IN'
                    ),
                array(
                    'taxonomy' 		=> 'pa_start-time',
                    'terms' 		=> $class['start-time'],
                    'field' 		=> 'slug',
                    'operator' 		=> 'IN'
                    ),
                ),
            );


    }



    // Now we retrieve the products
    $products = wc_get_products($args);

    // check that we retrieved products, and possibly filter only for the "Full Term" one
    if (sizeof($products) > 0) {
        // the query got all the classes in the term, so we need to remove all the ones that are before the current class
        $remaining_products = filter_later_products($class, $products);
        if (sizeof($remaining_products) > 0) {
            return $remaining_products;
        } else {
            // we filtered out everything
            return false;
        }
    } else {
        // no product returned in the query
        return false;
    }
}

/*
 *
 * This Function is a helper for the helper function just above
 * It takes the class we're interested in and all the classes in the term that are on the same day / location and time
 * What we want in the feature though are the remaining classes in the term so we need to filter the later classes
 *
 * Formats: $class is formatted array : $class['class-date]
 *          $products is an array of WC_Product_Simple
 *
 * It returns an array of products (may be empty)
 *
 */

function filter_later_products($class, $products) {

    $filtered_products = [];

    foreach($products as $product) {
        if (get_cust_att($product, 'class-date') && get_cust_att($product, 'class-date') !== '' ) {
            // the current product has a valid date
            if (strtotime(get_cust_att($product, 'class-date')) >= strtotime($class['class-date'])) {
                // the current product is at a later date than the current class - we add it to our list
                array_push($filtered_products, $product);
            }
        }
    }
    // now we ned to sort the filtered products by date

    usort($filtered_products, "cmp");


    return $filtered_products;
}

/*
 * A helper for the help
 * This lets use compare the class dates in the filter_later_products() function
 *
 */

function cmp($a, $b)
{
    return strcmp(strtotime(get_cust_att($a, 'class-date')), strtotime(get_cust_att($b, 'class-date')));
}


/*
 *
 * This Function is a helper that will display the children names associated to the class inside the shopping cart
 * It takes the current cart item being displayed
 *
 * It returns the html showing the child name
 *
 */
function gfr_display_child_detail($cart_item) {

    $html ='';

    if(isset($cart_item['child-detail'])) {
        foreach($cart_item['child-detail'] as $child) {
            $html .= "<br />Child detail: " . $child['firstname'] ." " . $child['lastname'] ;
        }
    }
    return $html;
}

/*
 *
 * This Function is a helper that will retrieve the children details saved in a user's meta data
 * It takes the user id needed and the meta name (basically 'child-detail')
 *
 * It returns the child_meta array
 *
 */
function gfr_get_meta($user_id, $meta_name) {

    $child_array_meta = get_user_meta( $user_id, 'child-detail' );

    if ($child_array_meta && $child_array_meta[0]) {
        $child_array = $child_array_meta[0];
    } else {
        // no kids saved for this user
        $child_array = null;
    }

    return $child_array;
}


/*
 *
 * Function to filter products for just 1 week.
 * Annoying but WP Query just doesn't cut it
 *
 */

function filter_single_week($products, $week_start, $week_end) {

    $filtered_products = [];

    foreach( $products as $product) {
        if (
            strtotime(get_cust_att($product, 'pa_class-date')) >= strtotime($week_start) &&
            strtotime(get_cust_att($product, 'pa_class-date')) <= strtotime($week_end) ) {
            array_push($filtered_products, $product);
        }
    }
    return($filtered_products);
 }

 /*
 *
 * Function to filter products for just 1 month.
 * Annoying but WP Query just doesn't cut it
 *
 */

function filter_single_month($products, $month, $year) {

    $filtered_products = [];

    foreach( $products as $product) {
        if ( strpos(get_cust_att($product, 'pa_class-date'), $year ."-". $month) === 0 ) {
            array_push($filtered_products, $product);
        }
    }
    return($filtered_products);
 }

 /*
 *
 * Function to search a value in an arry of [[name] => xx , [slug] => yyy].
 *
 */

 function search_query_in_array($value, $array) {
     $found = false;

     foreach( $array as $element) {
        if ( $element['slug'] === $value) {
            return true;
            break;
        }
    }

    return $found;
 }


 /*
 *
 * Function to return a colour code that helps to give a different background inside the calendar
 * ARGS: a single class array
 *
 * RETURNS: a string that will end in the css property
 *
 */
 function gfr_add_colour_code($single_class) {

    $term = get_term_by( 'name', $single_class['class-name'], 'pa_class-name' );

    $hex = get_field('front_calendar_color', 'pa_class-name_' . $term->term_id);

    return "style='background-color: $hex !important;'";

 }

/*
 *
 * Function to retrieve Term products
 * args: LOCATION: the location to retrieve products from - returns all if not passed or null or == "all"
 * args: TERM: the term for which to pull products for
 *
 * returns: array of classes in format that the Week Calendar list will be able to display
 *
 */

function get_single_classes_for_one_week($week_start, $week_end, $location, $classname, $format = 'single_class_for_admin') {

    // First build the query
	$args = array(
        'product_cat' => 'standard-class,holiday-class',// here we only load the "Term Products" that should live inside the Term Class Cat
        'posts_per_page' => -1,
        'status' => 'publish', //only published products
    );

    // we only search those products that are between $week_start and $week_end
    $period = new DatePeriod(
        new DateTime($week_start),
        new DateInterval('P1D'),
        new DateTime($week_end)
   );
    $dates_this_week = [];
    foreach ($period as $key => $value) {
        array_push($dates_this_week,$value->format('Y-m-d'));
    }

    $args['tax_query'] = array();
    array_push($args['tax_query'], array(
        'taxonomy' 	=> 'pa_class-date',
        'terms' 		=> $dates_this_week,
        'field' 		=> 'slug',
        'operator' 	=> 'IN'
    ));

    // Second add the location - if the user wants "all" we simply omit the location from the search
    if($location && $location !== 'all') {
        // we need to add the specific location asked to the query
        $args['tax_query'] = array();
        array_push($args['tax_query'], array(
         'taxonomy' 	=> 'pa_location',
         'terms' 		=> $location,
         'field' 		=> 'slug',
         'operator' 	=> 'IN'
        ));
     }
     if($classname && $classname !== 'all') {
        // we need to add the specific location asked to the query
        if (! $args['tax_query'] ) {
            $args['tax_query'] = array();
        }
        array_push($args['tax_query'], array(
         'taxonomy' 	=> 'pa_class-name',
         'terms' 		=> $classname,
         'field' 		=> 'slug',
         'operator' 	=> 'IN'
        ));
     }

    // Now we retrieve the products
    $products = wc_get_products($args);

    if ($format && $format === 'single_class_for_calendar_view') {
        $formatted_products = format_term_product_list($products);
    } else {
        $formatted_products = format_single_classes_for_admin($products);
    }
    return $formatted_products;
}


/*
 *
 * This simple function displays little table that allows to set the number of kids that should be booked
 * on a given class. This is used when viewing a class detail pop up in the modal window
 * in the Calendar view
 *
 */
function display_class_price_and_qty_form($price) {

    $qty_form_html = '
    <div class="quantity">
        How many children would you like to book?
        <div class="cart-summary-detail">
            <table>
                <tr>
                    <th>Price</th>
                    <th># of Children</th>
                    <th>Total Price</th>
                </tr>
                <tr>
                    <td id="product-price" data-product-price="' . $price . '" data-hj-whitelist>£' . $price . '</td>
                    <td>
                        <label class="screen-reader-text" for="quantity_5c39e89361ddb"></label>
                        <input type="number" id="single-class-quantity" class="input-text qty text" step="1" min="0" max="20" value="1" title="Qty" size="4" pattern="[0-9]*" inputmode="numeric" data-hj-whitelist>
                    </td>
                    <td id="total-price" data-total-price="' . $price . '" data-hj-whitelist>£ ' . $price . '</td>
                </tr>
            </table>
        </div>
    </div>';

    return $qty_form_html;

}

/*
 *
 * This simple function takes the list of locations, and will return some HTML "options" for the select filter
 * used at the top of the "Book Single Date" calendar
 *
 */

function build_location_filter($all_locations) {

    $filter_html='';

    foreach($all_locations as $location) {
        $filter_html .='<option class="level-0" value="' . $location['slug'] . '">' . $location['name'] . '</option>';
    }

    return $filter_html;
}

/*
 *
 * This simple function returns an arry of the next 13 weeks
 * used in the front end calendar, on mobile Only. Used the help people go straight to a week in the future
 * without having to click "Next week" many many times
 *
 * ARGS: STRING : the date of a monday to take as base to calculate forward. If null we take the Monday of the current week
 * Returns ARRAY [2019-09-02, 2019-09-09, ...]
 *
 */
function get_next_13_weeks($week_starting_date = null) {
    if(!$week_starting_date) {
        $monday_this_week = date("Y-m-d", strtotime('monday this week'));
    } else {
        $monday_this_week = date("Y-m-d", strtotime($week_starting_date));
    }
    $next_13_weeks = [];

    for ($i=1; $i < 14 ; $i++) {
        $days_in_future = 7 * $i;
        $monday_to_add = date("Y-m-d", strtotime($monday_this_week . '+ ' . $days_in_future . ' days'));
        array_push($next_13_weeks, $monday_to_add);
    }

    return $next_13_weeks;
}

/** 
 * Helper function to determine if a term class is a holiday bundle or a real term class
 * (as in a bundle of classes during the term, not during the holidays)
 * ARGS: WC_Product_Woosb object
 * Return BOOLEAN 
 */
function is_holiday_bundle($term_class)
{
    $array_slugs = gfr_get_product_category_slugs( $term_class);

    if( sizeof( $array_slugs ) > 0) {
        return in_array('holiday-bundle-classes', $array_slugs);
    } else {
        return false;
    }
    
}

/**
 * Simple function to return an array of slugs of all the categories of a WC_PRODUCT
 * 
 */

 function gfr_get_product_category_slugs($WCproduct)
 {
     $array_slugs = array();
     $cat_ids = $WCproduct->get_category_ids();
     if (! is_array($cat_ids) || sizeof($cat_ids) == 0) {
         return $array_slugs;
     }
     // we have some ids
     foreach ($cat_ids as $cat_id) {
         $term = get_term($cat_id, 'product_cat', ARRAY_A);

         if (isset($term['slug'])) {
             array_push($array_slugs, $term['slug']);
         }
     }
     return $array_slugs;
 }
?>
