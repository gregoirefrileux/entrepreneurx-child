<?php
   /*
    *
    * This template is shown in the My Account Section
    *
    * It shows three main boxes to
    *
    * It is displayed using the hook add_action to avoid too much reliance on custom WooCommerce files
    *
    *
    */
   
   ?>
<div class="elementor-widget-wrap">
   <section class="elementor-element elementor-element-f614e09 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="f614e09" data-element_type="section">
      <div class="elementor-container elementor-column-gap-default">
         <div class="elementor-row">
            <div class="elementor-element elementor-element-9a75e18 elementor-column elementor-col-33 elementor-inner-column" data-id="9a75e18" data-element_type="column">
               <div class="elementor-column-wrap  elementor-element-populated">
                  <div class="elementor-widget-wrap">
                     <div class="elementor-element elementor-element-08473ec elementor-view-default elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box" data-id="08473ec" data-element_type="widget" data-widget_type="icon-box.default">
                        <div class="elementor-widget-container">
                           <div class="elementor-icon-box-wrapper">
                              <div class="elementor-icon-box-icon">
                                 <span class="elementor-icon elementor-animation-">
                                    <a href="/my-account/orders"><i class="fa fa-wpforms" aria-hidden="true"></i></a>
                                 </span>
                              </div>
                              <div class="elementor-icon-box-content">
                                 <h3 class="elementor-icon-box-title">
                                 <a href="/my-account/orders">Pay for and view Orders</a>
                                 </h3>
                                 <p class="elementor-icon-box-description">See previous, current and pending payment orders </p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="elementor-element elementor-element-0291fa6 elementor-column elementor-col-33 elementor-inner-column" data-id="0291fa6" data-element_type="column">
               <div class="elementor-column-wrap  elementor-element-populated">
                  <div class="elementor-widget-wrap">
                     <div class="elementor-element elementor-element-56ebb04 elementor-view-default elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box" data-id="56ebb04" data-element_type="widget" data-widget_type="icon-box.default">
                        <div class="elementor-widget-container">
                           <div class="elementor-icon-box-wrapper">
                              <div class="elementor-icon-box-icon">
                                 <span class="elementor-icon elementor-animation-">
                                 <a href="/my-account/child-detail"><i class="fa fa-users" aria-hidden="true"></i></a>
                                 </span>
                              </div>
                              <div class="elementor-icon-box-content">
                                 <h3 class="elementor-icon-box-title">
                                 <a href="/my-account/child-detail"><span>Child Information</span></a>
                                 </h3>
                                 <p class="elementor-icon-box-description">Enter and manage information on your children to speed up future orders</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="elementor-element elementor-element-ef78f1b elementor-column elementor-col-33 elementor-inner-column" data-id="ef78f1b" data-element_type="column">
               <div class="elementor-column-wrap  elementor-element-populated">
                  <div class="elementor-widget-wrap">
                     <div class="elementor-element elementor-element-9a14f9d elementor-view-default elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box" data-id="9a14f9d" data-element_type="widget" data-widget_type="icon-box.default">
                        <div class="elementor-widget-container">
                           <div class="elementor-icon-box-wrapper">
                              <div class="elementor-icon-box-icon">
                                 <span class="elementor-icon elementor-animation-">
                                 <a href="/my-account/approved-carer"><i class="fas fa-address-book"></i></a>
                                 </span>
                              </div>
                              <div class="elementor-icon-box-content">
                                 <h3 class="elementor-icon-box-title">
                                 <a href="/my-account/approved-carer"><span>Approved Carer</span></a>
                                 </h3>
                                 <p class="elementor-icon-box-description">Add or edit the name of your nanny or child minder</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="elementor-element elementor-element-f614e09 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="f614e09" data-element_type="section">
      <div class="elementor-container elementor-column-gap-default">
         <div class="elementor-row">
            <div class="elementor-element elementor-element-b91df0c elementor-column elementor-col-50 elementor-inner-column" data-id="b91df0c" data-element_type="column">
               <div class="elementor-column-wrap  elementor-element-populated">
                  <div class="elementor-widget-wrap">
                     <div class="elementor-element elementor-element-f416e67 elementor-view-default elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box" data-id="f416e67" data-element_type="widget" data-widget_type="icon-box.default">
                        <div class="elementor-widget-container">
                           <div class="elementor-icon-box-wrapper">
                              <div class="elementor-icon-box-icon">
                                 <span class="elementor-icon elementor-animation-">
                                 <a href="/my-account/edit-address"><i class="fa fa-address-card" aria-hidden="true"></i></a>
                                 </span>
                              </div>
                              <div class="elementor-icon-box-content">
                                 <h3 class="elementor-icon-box-title">
                                 <a href="/my-account/customer-logout"><span>My Addresses</span></a>
                                 </h3>
                                 <p class="elementor-icon-box-description">View and manage your billing details</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="elementor-element elementor-element-b91df0c elementor-column elementor-col-50 elementor-inner-column" data-id="b91df0c" data-element_type="column">
               <div class="elementor-column-wrap  elementor-element-populated">
                  <div class="elementor-widget-wrap">
                     <div class="elementor-element elementor-element-f416e67 elementor-view-default elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box" data-id="f416e67" data-element_type="widget" data-widget_type="icon-box.default">
                        <div class="elementor-widget-container">
                           <div class="elementor-icon-box-wrapper">
                              <div class="elementor-icon-box-icon">
                                 <span class="elementor-icon elementor-animation-">
                                 <a href="/my-account/payment-methods"><i class="fas fa-credit-card"></i></a>
                                 </span>
                              </div>
                              <div class="elementor-icon-box-content">
                                 <h3 class="elementor-icon-box-title">
                                 <a href="/my-account/payment-methods"><span>Payment Methods</span></a>
                                 </h3>
                                 <p class="elementor-icon-box-description">Add a credit or debit card to your account, or manage your saved card details.</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="elementor-element elementor-element-5bd6d0f elementor-column elementor-col-50 elementor-inner-column" data-id="5bd6d0f" data-element_type="column">
               <div class="elementor-column-wrap  elementor-element-populated">
                  <div class="elementor-widget-wrap">
                     <div class="elementor-element elementor-element-b37496e elementor-view-default elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box" data-id="b37496e" data-element_type="widget" data-widget_type="icon-box.default">
                        <div class="elementor-widget-container">
                           <div class="elementor-icon-box-wrapper">
                              <div class="elementor-icon-box-icon">
                                 <span class="elementor-icon elementor-animation-">
                                 <a href="/my-account/edit-account"><i class="fa fa-cog" aria-hidden="true"></i></a>
                                 </span>
                              </div>
                              <div class="elementor-icon-box-content">
                                 <h3 class="elementor-icon-box-title">
                                 <a href="/my-account/edit-account"><span>Account Detail</span></a>
                                 </h3>
                                 <p class="elementor-icon-box-description">View and manage your email and password details</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- final row with log out section -->
   <section class="elementor-element elementor-element-f614e09 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="f614e09" data-element_type="section">
      <div class="elementor-container elementor-column-gap-default">
         <div class="elementor-row">
            <div class="elementor-element elementor-element-b91df0c elementor-column elementor-col-50 elementor-inner-column" data-id="b91df0c" data-element_type="column">
               <div class="elementor-column-wrap  elementor-element-populated">
                  <div class="elementor-widget-wrap">
                     <div class="elementor-element elementor-element-f416e67 elementor-view-default elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box" data-id="f416e67" data-element_type="widget" data-widget_type="icon-box.default">
                        <div class="elementor-widget-container">
                           <!-- empty block -->
                           </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="elementor-element elementor-element-b91df0c elementor-column elementor-col-50 elementor-inner-column" data-id="b91df0c" data-element_type="column">
               <div class="elementor-column-wrap  elementor-element-populated">
                  <div class="elementor-widget-wrap">
                     <div class="elementor-element elementor-element-f416e67 elementor-view-default elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box" data-id="f416e67" data-element_type="widget" data-widget_type="icon-box.default">
                        <div class="elementor-widget-container">
                        <div class="elementor-icon-box-wrapper">
                              <div class="elementor-icon-box-icon">
                                 <span class="elementor-icon elementor-animation-">
                                 <a href="<?php echo wc_logout_url() ?>"><i class="fa fa-mail-forward" aria-hidden="true"></i></a>
                                 </span>
                              </div>
                              <div class="elementor-icon-box-content">
                                 <h3 class="elementor-icon-box-title">
                                    <a href="<?php echo wc_logout_url() ?>"><span>Log out</span></a>
                                 </h3>
                                 <p class="elementor-icon-box-description">Log out of your current session</p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="elementor-element elementor-element-5bd6d0f elementor-column elementor-col-50 elementor-inner-column" data-id="5bd6d0f" data-element_type="column">
               <div class="elementor-column-wrap  elementor-element-populated">
                  <div class="elementor-widget-wrap">
                     <div class="elementor-element elementor-element-b37496e elementor-view-default elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-icon-box" data-id="b37496e" data-element_type="widget" data-widget_type="icon-box.default">
                        <div class="elementor-widget-container">
                           <!-- empty block -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>

</div>