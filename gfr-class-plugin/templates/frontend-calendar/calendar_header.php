<?php
/*
*
* Function to build the header of the term week view
* Takes in the possible terms and possible locations and returns a bit of html
* 
*/
function build_header($list_locations, $list_class_names, $list_age_groups, $location_selected, $class_selected, $age_group_selected, $week_starting_date) {

   $html ='';

   // First the select for locations
   $html .= '<div class="calendar-filter-header">';
       $html .= '<p class="header-filter-by">Filter by</p>';
       // Choose the location
       $html .= '<div><p class="filter-title"><span class="mobile-only" style="display: initial">filter by: </span>Location</p>';
           $html .= '<div class="booked-calendarSwitcher calendar">';
           $html .= '<p>';
               $html .= '<select name="location">';
               // we manually add an "all" value
               $html .= '<option value="all"'.('all' === $location_selected ? ' selected="selected"' : '').'>All locations</option>';
               foreach($list_locations as $location) {
                   $html.= '<option value="'.$location['slug'].'"'.($location['slug'] == $location_selected ? ' selected="selected"' : '').'>'. $location['name'].'</option>';
               } 
               $html .= '</select>';
   $html .= '   </p>
           </div>
       </div>';
   
   // then choose the class name
   $html .= '<div><p class="filter-title"><span class="mobile-only" style="display: initial">filter by: </span>Class</p>';
   $html .= '<div class="booked-calendarSwitcher calendar">';
   $html .= '<p>';
   $html .= '<select name="class-name">';
   // we manually add an "all" value
   $html .= '<option value="all"'.('all' == $class_selected ? ' selected="selected"' : '').'>All classes</option>';
   foreach($list_class_names as $class_name) {
       $html.= '<option value="'.$class_name['slug'].'"'.($class_name['slug'] == $class_selected ? ' selected="selected"' : '').'>'. $class_name['name'].'</option>';
   } 
   $html .= '</select>';
   $html .= '</p></div></div>';

   // And now the Mobile only Day Seletor
   $html .= '<div class="mobile-only">
                <p class="filter-title">
                    <span class="mobile-only" style="display: initial">filter by: </span>Day of the week
                </p>';
   $html .= '<div class="booked-calendarSwitcher calendar">';
   $html .= '<p>';

   $html .= '<select name="weekday">';
   // we manually add an "all" value
   $days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
   foreach($days as $day) {
       $html.= '<option value="column-'.$day.'"'.($day == 'Monday' ? ' selected="selected"' : '').'>'. $day.'</option>';
   } 
   $html .= '</select>';
   $html .= '</p></div></div>';

   // And finally the Mobile only "Navigate to week"
   $html .= '<div><p class="filter-title"><span class="mobile-only" style="display: initial">Navigate to: </span>Week</p>';
   $html .= '<div class="booked-calendarSwitcher calendar">';
   $html .= '<p>';

   $html .= '<select name="upcoming-week">';
   $html .= '<option value="'. $week_starting_date .'" selected="selected">Choose an upcoming week</option>';
   // we manually add an "all" value
   $weeks_to_come = get_next_13_weeks($week_starting_date);
   foreach($weeks_to_come as $week) {
       $html.= '<option value="'.$week.'">W/C '. date("l jS F",strtotime($week)).'</option>';
   } 
   $html .= '</select>';
   $html .= '</p></div></div>';

   // Final closing div of the "calendar-filter-header"
   $html .= '</div>';
   

   return $html;

}

?>