<?php
/*
 *
 * Function to display the classes at a given time and location
 * The key here is to look up the "Class name" and display each of them
 * If for a given "Class Name" there are sevral products, then that's where we gather them up
 *
 * returns: the html for this time slot, and location
 *
 */

function display_classes_at_location($location) {

    foreach($location as $class_name => $single_class) {

        // a single_class is an array of productID => array ([id]=> .., [location]=> .., )
        // but not all keys are known so this will be helpful
        $keys = array_keys( $single_class);

        // now build the html
        echo '<div class="timeslot" ' . gfr_add_colour_code($single_class) . '>
                <div class="location-and-time-block">
                    <p class="time">'. $single_class['start-time'] . ' - ' . $single_class['finish-time'] . '</p>
                    <div class="location-cal-wrapper">';
        $text_needs_wrapped = strlen( $single_class['location'] )  > 20;
        echo '<p class="location" ' . ( !$text_needs_wrapped ? '' : 'style="font-size:7px"') . '>' . $single_class['location'] . '</p>';
        echo '</div>
                </div>
                <div style="clear: both"></div>';

        // $pos = strpos( $class_name, " (" );
        // $class_name = $pos === -1 ? $class_name : substr( $class_name, 0, $pos );

        foreach ( [ "Monday ", "Tuesday ", "Wednesday ", "Thursday ", "friday ", "Saturday ", "Sunday " ] as $day )
          $class_name = str_replace( $day, "", $class_name );

        echo '<div class="class-title">
                    <p class="name">' . $class_name . '</p>
                </div>';
        if ($single_class['stock'] <= 0) {
            // no stock left
            echo '<div class="timeslot-people">
                    <button data-product-id="' . $single_class['id'] . '" class="join-waitlist button"> JOIN WAITLIST
                    </button>
                </div>';
        } else {
            // We are displaying some TERM products, which are linked to many individual products, representing a single class
            // We have stock on at least some of the products, so let's show the "Show options" button
            // we have several options so need to display a "show option" button

            echo '<div class="timeslot-people">
                    <button id="show-option" data-product-id="' . $single_class['id'] . '" class="new-appt button">
                        <span class="button-text"> BOOK NOW </span>';
                        // now we display the html that will appear in the modal when "show option" is clicked
                        echo '<div id="html_for_modal" style="display: none;">
                                <div class="booked-form booked-scrollable" style="max-height: 274px;">
                                    <div class="condition-block customer_choice default" id="condition-new">
                                        <div class="booked-appointments">';
                                            echo display_class_popup_detail($single_class);
                        // Closing the html_for modal elements after the loop
                        echo '          </div>
                                    </div>
                                </div>
                                <p class="booked-title-bar">BOOK TARKA</p>
                                <p class="booked-subtitle-bar">Single / Multiple classes / Whole term</p>
                                <a href="#" class="close"><i class="fa fa-close"></i></a>
                            </div>';
                // finally we close the button
                echo '</button></div>';
            }
        // And closing the entire thing
        echo '</div>';

    }

}
?>
