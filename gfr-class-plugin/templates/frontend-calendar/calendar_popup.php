<?php
/*
 *
 * Function to build the hidden div which is then shown inside the popup when clients click "Show options"
 * Takes in a class and returns a bit of html
 * Format of the class : the formattted product such as below:
* [Tarka Younger Class (18month-3yrs)] =&gt; Array
    (
        [939] =&gt; Array
            (
                [id] =&gt; 939
                [location] =&gt; Notting Hill
                [name] =&gt; Tarka x45min Weekly Session – Thursday – 45min – 9:30AM – Notting Hill - First half term
                etc.
            )

    )
 *
 */
function display_class_popup_detail($class_product) {

    // If the class has a nice short "class-name' attribute we use that instead of the WP Post Title
    $title = $class_product['name'];
    if ($class_product['class-name'] && $class_product['class-name'] !== '') {
        $title = $class_product['class-name'];
    }

    $html ='';

    // Build the big white div at the top of the modal
    $html .= build_white_header($class_product);

    // Now we will need to show all the dates available in the term
    $remaining_classes = get_remaining_dates_in_term_for_single_class($class_product);

    // Build the list of radio options
    $html .= '<div class="list-of-classes">
                '.
                display_list_of_single_classes($remaining_classes)
                .''.
                display_term_option_for_class($class_product)
                .'
            </div>';


    $html .= display_class_price_and_qty_form($class_product['price']);

    // and finally the NEXT button

    $html .='<div id="add_child_form" class="frm_forms  with_frm_style frm_style_formidable-style">
                <p class="ajax-status"></p>
                <p class="term-view-book-class">
                    <a id="add_term_to_cart" data-product-id="' . $class_product['id'] .'" class="modal-next-btn" data-href="' . get_permalink( wc_get_page_id( 'cart' ) ) . '">
                    Next - enter child detail</a>
                </p>
            </div>';


    return $html;
}

/*
 *
 * Function that displays the white header block of the Modal pop up that shows when you click "BOOK TERM/CLASS" in the calendar view
 * The function takes in a single classe and returns the title, age group, location, start and end time and stock left
 *
 */

function build_white_header($class_product) {

    $block_html = '';
    // GREG : reset the line 77 back to  $class_product['stock'] . ' places available after start of term
    $block_html .= '<div class="modal-white-header">
                        <p class="class-title" data-hj-whitelist>' . $class_product['class-name'] . ' - <span class="age-group">' . $class_product['age-group'] .'</span></p>
                        <p class="time-and-location" data-hj-whitelist>' . $class_product['start-time'] . ' - ' . $class_product['finish-time'] .' - ' . $class_product['location'] .'</p>
                        <p class="class-stock" data-hj-whitelist>' . ($class_product['stock'] > 0 ? ' Spaces available' : ' Unavailable ') . '</p>
                    </div>';

    return $block_html;
}

/*
 *
 * Function that displays the white block of for booking the whole term, when you click "BOOK TERM/CLASS" in the calendar view
 * The function takes in a single class, find its terms option, and returns the title, age group, location, start and end time and stock left
 *
 */


function display_term_option_for_class($class_product) {

    $term_html = '';

    $term_class = get_term_class_for_single_class($class_product);

    if(!$term_class || !is_object($term_class)) {
        // no term class option for this class we simply return
        return $term_html;
    }

    if(!$term_class->is_in_stock()) {
        // hide the block if the product is out of stock
        return $term_html;
    }
    $term_or_week = is_holiday_bundle($term_class) ? 'week' : 'term';
    $helper = new WPCleverWoosb_Helper();
    $term_html .= '<div class="modal-white-header">
                    <p>Or signup for the ' . $term_or_week . ' to secure a ' . $term_class->get_discount() . '% discount</p>';
                    $term_html .= '<input type="checkbox" id="term-class-checkbox" value="' . $term_class->get_id() . '" data-product-id="' . $term_class->get_id() . '" data-product-price="' . $term_class->get_sale_price() . '">'
                                    . $class_product['class-name'] . ' - <p class="original-price">£'
                                    . $term_class->get_regular_price() . '</p>'
                                    . '  £' . $term_class->get_sale_price() . '<br>';
    $term_html .='</div>';

    return $term_html;

}
