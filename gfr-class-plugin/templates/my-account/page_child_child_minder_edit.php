<style>
    @media(min-width: 1024px) {
        input#nanny {
            margin-left: 10px;
        }
    }
</style>

<h3><?php _e("Please enter the name of approved carer", "blank"); ?></h3>

<span class="description"><?php _e("Please enter the first name and last name of your approved carer."); ?></span>

<div class="frm_forms  with_frm_style frm_style_formidable-style">
    <form class="frm-show-form ">

        <div class="frm_form_fields ">
            <div class="frm_form_field form-field  frm_required_field frm_none_container">
                <label>Nanny / Child minder name (e.g. Carol Price)*</label>
                <input type="text" name="nanny" id="nanny" value="<?php echo esc_attr( get_the_author_meta( 'nanny', wp_get_current_user()->ID ) ); ?>" style="max-width: 600px" /><br />
            </div>
        </div>

        <div class="frm_submit" style="text-align: center">
            <p class="ajax-status"></p>
            <button class="button save-nanny-information">
                <span>Save</span>
            </button>
        </div>
    </form>
</div>

<script type = "text/javascript">

jQuery(document).on("click", "button.save-nanny-information", function(e) {
    e.preventDefault();

    // show that something is happening
    show_loader();

    var button = jQuery(this);
    // get the information entered in the form
    nanny = jQuery("input#nanny").val();

    // very basic validation
    if (nanny === "" || nanny === null) {
      // remove any previous error div
      jQuery("div.frm_error_style").remove();
      // show this error
      jQuery("div.frm_forms.with_frm_style.frm_style_formidable-style")
        .prepend(
          '<div class="frm_error_style">Please enter all mandatory fields.</div>'
        );
      button.css("display", "initial");
      jQuery("p.ajax-status").hide();
      return false;
    } else {
      // no errors, we hide any previously displayed errors
      jQuery("div.frm_error_style").remove();
    }

    // post data to backend
    jQuery.ajax({
      url: "/wp-admin/admin-ajax.php",
      method: "post",
      data: {
        action: "my_account_save_nanny_detail",
        nanny,
      }
    })
    .done(function(response) {
          console.log("success");
        if (response.error != "undefined" && response.error) {
          // display the error
          // TODO : get the error message from the request
          jQuery("div.frm_forms.with_frm_style.frm_style_formidable-style")
            .prepend(
              '<div class="frm_error_style">An error occured, we could not save this information.</div>'
            );
          //show the save button
          button.css("display", "initial");
          // hide loader
          hide_loader();
        } else {
          // remove the loader
          hide_loader();
          // show an OK message
          jQuery("div.frm_forms.with_frm_style.frm_style_formidable-style")
            .prepend(
              '<div class="frm_success_style">Thank you, this information has been saved.</div>'
            );
        }
    })
    .fail(function(response) {
        console.log("failed");
        jQuery("div.frm_forms.with_frm_style.frm_style_formidable-style")
          .prepend(
            '<div class="frm_error_style">An error occured, we could not save this information.</div>'
          );
        // remove the loader
        hide_loader();
        //show the save button
        button.show();
    });

  });

  function show_loader() {
    jQuery("p.ajax-status")
      .show()
      .html(
        '<p style="text-align: center"><i class="fa fa-spinner"></i>&nbsp;&nbsp;&nbsp; Please wait</p>'
      );
  }
  function hide_loader() {
    jQuery("p.ajax-status").hide();
  }

</script>