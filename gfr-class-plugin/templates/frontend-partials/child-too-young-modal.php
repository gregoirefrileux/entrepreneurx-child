<div id="html_for_modal_child_too_young" style="display: none;">
  <div class="booked-form booked-scrollable" style="max-height: 599px;">
    <p>Your child needs be over <span class='min-age-span'></span> years old for the selected class.</p>
  </div>
  <p class="booked-title-bar">Child is too young</p>
  <a href="#" class="close"><i class="fa fa-close"></i></a>
</div>
