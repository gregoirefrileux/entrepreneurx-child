<div id="html_for_modal_join_waitlist" style="display: none;">
  <div class="booked-form booked-scrollable" style="max-height: 599px;">
    <p>You will receive an email if a space opens up.</p>
    <form id="join-waitlist-form">
      <input
        class="join-waitlist-email"
        type="email"
        placeholder="Enter your email"
        required><br>
      <input type="hidden" class="product-id">
      <input class="modal-next-btn" type="submit" value="Submit">
    </form>
    <div class="server-response">
    </div>
  </div>
  <p class="booked-title-bar">Join Waitlist</p>
  <a href="#" class="close"><i class="fa fa-close"></i></a>
</div>
