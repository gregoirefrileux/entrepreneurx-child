<?php
    // reference the Dompdf namespace
    use Dompdf\Dompdf;
    use Dompdf\Options;

    //Let's do some init
    $class_id = isset($_GET['class-id']) ? $_GET['class-id'] : null;

    if (!$class_id || $class_id === '') {
        exit; // Exit if accessed without appropriate ID
    }

    // We get the WooCommerce Products
    $class = wc_get_product($class_id);

    if (!$class) {
        exit; // this is not a valid class
    }

    // Get orders for the class
    $orders_ids = get_orders_ids_by_product_id($class_id);

    if( isset($_POST['generate_posts_pdf'])){
        output_pdf($class);
        ob_end_flush(); 
    }

    function output_pdf($class) {
        // instantiate and use the dompdf class
        $options = new Options();
        $options->setIsHtml5ParserEnabled(true);
        $dompdf = new Dompdf($options);

        $html = '
        <style>
            table { width: 100% }
            table.fixed {
                table-layout: fixed;
            }
            .striped > tbody > :nth-child(odd), ul.striped > :nth-child(odd), .alternate {
                background-color: #f6f7f7;
            }
            .print-no { display: none}
        </style>
        <h2>' . get_cust_att($class, 'class-name') . ' - ' . get_cust_att($class, 'class-date') . ' - ' . get_cust_att($class, 'location') . ' - ' . get_cust_att($class, 'start-time') .'</h2>
        <br />
        <table class="admin-class-detail wp-list-table widefat fixed striped posts">
        <thead>
            <tr>
                <th class="manage-column column-order_number column-primary sortable desc" style="width: 20px;">
                    <span>id</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Attendance</span>
                    <input type="checkbox" id="toggle_attendance">
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Child Name</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>New ?</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Age</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc print-no">
                    <span>Gender</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Notes</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc print-no">
                    <span>Nursery</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc print-no">
                    <span>Paid ?</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Parent Name</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Parent Phone Number</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc print-no">
                    <span>Parent email address</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Approved Carer</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc print-no">
                    <span>Actions</span>
                </th>
            </tr>
        </thead>
        <tbody>
            ' . admin_show_booking_list_for_class($class) . '
        </tbody>
    </table>';

        $dompdf->loadHtml($html);

        // Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        ob_clean();
        $dompdf->stream("TARKA_class_list_" . get_cust_att($class, 'class-name') . "_" . get_cust_att($class, 'class-date') .".pdf");
        exit;
    }
    
?>

<div class="back-to-classes print-no">
    <a href="/wp-admin/admin.php?page=classes">Back to Class list</a>
</div>

<div class="print-no">
    <h1>Class Detail</h1>
    <p>Here, you should be able to see the detail of the bookings for the class <?php echo $class_id ?></p>

	<form method="post" id="as-fdpf-form">
        <button class="button button-primary" type="submit" name="generate_posts_pdf" value="generate">Generate PDF class list</button>
    </form>
    <br />
</div>

<?php
$orders_ids = array_unique($orders_ids, $sort_flags = SORT_STRING);

$all_emails = array();
foreach($orders_ids as $order_id) {
    // load the current order
    $order = wc_get_order($order_id);
    $order_email = $order->get_billing_email();

    array_push($all_emails, $order_email);
    }
$commaList = implode(', ', $all_emails); ?>
<form action="mailto:<?php echo $commaList; ?>" class="email_all print-no">
    <input type="submit" value="Email all parents" />
</form>
<form id="go_to_next_week" class="go-to-next-week">
    <input type="submit" value="Go To Next Week" />
</form>
<form id="save_attendance" class="save-attendance">
    <input type="submit" value="Save Attendance" />
</form>

<div class="admin-week-class-wrapper">
    <div class="booked-calendarSwitcher calendar">
        <h2><?php echo get_cust_att($class, 'class-name') . ' - ' . get_cust_att($class, 'class-date'); ?></h2>
        <p><i class="fa fa-clock-o"></i> Start time :  <?php echo get_cust_att($class, 'start-time') ; ?></p>
        <p><i class="fa fa-map-marker"></i> Location:  <?php echo get_cust_att($class, 'location') ; ?></p>
    </div>
    <!-- now for the table -->


    <table class="admin-class-detail wp-list-table widefat fixed striped posts">
        <thead>
            <tr>
                <th class="manage-column column-order_number column-primary sortable desc" style="width: 20px;">
                    <span>id</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Attendance</span>
                    <input type="checkbox" id="toggle_attendance">
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Child Name</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>New ?</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Age</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc print-no">
                    <span>Gender</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Notes</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc print-no">
                    <span>Nursery</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc print-no">
                    <span>Paid ?</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Parent Name</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Parent Phone Number</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc print-no">
                    <span>Parent email address</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Approved Carer</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc print-no">
                    <span>Actions</span>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php echo admin_show_booking_list_for_class($class); ?>
        </tbody>
    </table>
<div>
