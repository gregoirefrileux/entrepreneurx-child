<h1>Generate Holiday Classes Bundles</h1>
<div class="generate-term-intro">
    <p>
        Use this page to generate the bundled holiday classes products. Please only use this section when you have successfully created the individual classes for a given holiday week.
        <br />
        If the individual holiday classes do not exist, or if they are misconfigured, this section may not work.
        <br />
        Note that the classes need to be "active" for the terms to appear in the dropdown. This is to avoid seeing loads of old, unused terms. If you are still working on the classes, you could use Advance buld Edit to briefly activate all classes, then create the bundles, then mark them as private again once done.
        <br />
        It will need to have the classes setup with the correct attribute: location, term name, category, start time, end time, age group, etc.
        <br />
    </p>

    <h4>Please select a term for which to generate the Bundle Holiday classes</h4>

    <?php if ( current_user_can( 'edit_users' ) ) {
        // need to show the selector and button
        // First we load all the term names
        $term_names = get_all_cust_att_values('pa_term-name');
    ?>
    
    <div class="term-select">
        <form method="POST" id="gfr_create_holiday_class_form" action="<?php echo admin_url( 'admin.php' ); ?>">
            <div class="booked-calendarSwitcher calendar">
                <select name="term_name">
                <?php echo('<option value="all">Select term</option>'); ?>
                <?php foreach($term_names as $key => $term_name) {
                    echo('<option value="'.$term_name['slug'].'">'. $term_name['name'].'</option>');
                }?>
                </select>
            </div>
            <br />
            <?php wp_nonce_field('createHolidayClasses'); ?>
            <input type="hidden" name="action" value="createHolidayClasses" />
            <input id="submitBtn" type="submit" class="button" value="Create holiday bundles" />
        </form>
    </div>

    <div id="loader"></div>

    <div id="gfr_form_feedback"></div>

    <style>
        #loader {
            border: 8px solid #f3f3f3; /* Light grey */
            border-top: 8px solid #3498db; /* Blue */
            border-radius: 50%;
            width: 20px;
            height: 20px;
            animation: spin 2s linear infinite;
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>


    <script type="text/javascript">
        jQuery( document ).ready( function( $ ) {
            "use strict";

            $('#loader').hide();
            /**
            * The file is enqueued from inc/admin/class-admin.php.
            */        
            $( '#gfr_create_holiday_class_form' ).submit( function( event ) {
                // Prevent the default form submit.
                event.preventDefault();
                // lock button and show loader
                $('#loader').show();
                $('#submitBtn').prop('disabled', true);        
                // serialize the form data
                var ajax_form_data = $("#gfr_create_holiday_class_form").serialize();
                //add our own ajax check as X-Requested-With is not always reliable
                ajax_form_data = ajax_form_data+'&ajaxrequest=true&submit=Submit+Form';
                
                $.ajax({
                    url:    '<?php echo admin_url( 'admin-ajax.php' ); ?>',
                    type:   'post',                
                    data:   ajax_form_data
                })
                
                .done( function( response ) { // response from the PHP action
                    var html = "";
                    if(response && response.error) {
                        html += "<p>More information on this error: ";
                        html += response.message || 'No error message';
                        html += "</p>";
                        $(" #gfr_form_feedback ").html( "<h2>An error occured </h2>" + html );
                    } else if(response && response.ids) {
                        html += "<p>The list of bundle created is: ";
                        html += response.ids.join(" , ");
                        html += "</p>";
                        $(" #gfr_form_feedback ").html( "<h2>The request was successful </h2>" + html );
                    }
                    // hide loader and re-activate btn
                    $('#loader').hide();
                    $('#submitBtn').prop('disabled', false);        
                })
                
                // something went wrong  
                .fail( function() {
                    $(" #gfr_form_feedback ").html( "<h2>Something went wrong.</h2><br>" );                  
                    // hide loader and re-activate btn
                    $('#loader').hide();
                    $('#submitBtn').prop('disabled', false);        
                })

                // after all this time?
                .always( function() {
                    event.target.reset();
                });

            });

        });
    </script>

<?php } ?>


</div>

<?php
    // TODO : display success or error

?>