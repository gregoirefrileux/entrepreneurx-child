<?php
    if(isset($_GET['month'])) {
        $createReport = new AdminCreateCustomOrderReport();
        $createReport->create_custom_report();
        
        die();
        
    }
?>




<h1>Generate Order report</h1>
<div class="generate-term-intro">
    <p>
        Use this page to generate a custom report of WooCommerce orders.
        <br />
    </p>

    <h4>Please select a month for which to generate the report</h4>

    <?php if ( current_user_can( 'edit_users' ) ) {
        // need to show the selector and button
        // First we load the last 12 months
        $months=[];
        for ($i = 1; $i < 12; $i++) {
            $months[$i] = date('F Y', strtotime("-$i month"));
          }
    ?>
    
    <div class="term-select">
        <form method="GET" id="gfr_get_custom_order_report">
            <div class="booked-calendarSwitcher calendar">
                <select name="month">
                <?php echo('<option value="all">Select month</option>'); ?>
                <?php foreach($months as $key => $month) {
                    echo('<option value="'.$month.'">'. $month .'</option>');
                }?>
                </select>
            </div>
            <br />
            <input name="page" value="get-order-report" type="hidden"/>
            <?php wp_nonce_field('getCustomOrderReport'); ?>
            <input id="submitBtn" type="submit" class="button" value="Get Order Report" />
        </form>
    </div>

<?php } ?>


</div>

