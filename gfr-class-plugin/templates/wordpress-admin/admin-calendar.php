<h1>List of classes</h1>
<p>Here, you should be able to see the upcoming classes, and for each view the bookings related to this class</p>
<div>
  <button class="email-all-parents">
    Email all parents
  </button>
</div>
<?php
    //Let's do some init
    $local_time = current_time('timestamp');

    $current_day = date_i18n('Ymd',$local_time);

    $year = date_i18n('Y',$local_time);
    $month = date_i18n('m',$local_time);
    $day = date_i18n('d',$local_time);

    $week_start = date("Y-m-d", strtotime('monday this week'));

    // Get some stuff for the Select Dropdown options
    // Let's get all the possible locations - useful for the select down below
    $all_locations = get_all_cust_att_values("pa_location");
    // Let's get all the possible locations - useful for the select down below
    $all_class_names = get_all_cust_att_values("pa_class-name");

?>

<div class="admin-week-class-wrapper">
    <div class="booked-calendarSwitcher calendar">
        <p class="calendar-selector">
            <i class="fa fa-map-marker"></i>
            <select name="admin-location" id="admin-location" class="booked_calendar_chooser">
                <option value="all">All locations</option>
                <?php echo build_location_filter($all_locations); ?>
            </select>
        </p>
        <p class="calendar-selector margin-left">
            <i class="fa fa-map-marker"></i>
            <select name="admin-class-name" id="admin-class-name" class="booked_calendar_chooser">
                <option value="all">All Classes</option>
                <?php echo build_location_filter($all_class_names); ?>
            </select>
        </p>
        <p class="calendar-selector margin-left">
            <i class="fa fa-calendar"></i>
            <input type="text" id="datepicker" name="admin-datepicker" placeholder="Pick a week">
        </p>
        <p class="calendar-selector margin-left">
            <i class="fa fa-calendar"></i>
            <input type="text" id="datepicker-day" name="admin-datepicker-day" placeholder="Pick a day">
        </p>
        <p id='loader' class="calendar-selector margin-left" style='display: none;'>
            <i class="fa fa-spinner fa-spin"></i>
        </div>
    </div>

    <!-- now for the table -->

    <?php echo gfr_build_admin_list_view_classes_for_week($week_start); ?>

<div>
