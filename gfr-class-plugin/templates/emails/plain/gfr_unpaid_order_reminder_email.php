<?php
/**
 * Admin new order email (plain text)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/plain/admin-new-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails/Plain
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

echo '= ' . esc_html( $email_heading ) . " =\n\n";

/* translators: %s: Customer billing full name */
echo '= Dear ' . $parent_data['parent_first_name']  . " =\n\n";

echo '= Just a reminder that payment for your Tarka order placed on the ' . date('l jS F Y', strtotime($parent_data['date_created'])) . " is still outstanding. =\n\n";

echo '= It can be paid by visiting www.tarkalondon.com and navigating to My Account. =\n\n';

echo '= If you do not think you have any outstanding payments, please contact the office at info@tarkalondon.com =\n\n';

echo '= Thank you =\n\n';

echo '= The TARKA London Team =\n\n';

echo "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n\n";

echo esc_html( apply_filters( 'woocommerce_email_footer_text', get_option( 'woocommerce_email_footer_text' ) ) );
