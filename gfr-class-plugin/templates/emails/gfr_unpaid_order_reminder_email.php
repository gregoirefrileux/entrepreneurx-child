<?php
/**
 * Customer completed order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-completed-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', "Tarka Class Reminder", '' ); ?>

<p>Dear <?php echo $parent_data['parent_first_name'] ?>,</p>


<div>   This is a reminder that payment for your Tarka order placed on the <?php echo date('l jS F Y', strtotime($parent_data['date_created'])) ; ?> is still outstanding.
        <br />
        To pay for this order, please click <a href="<?php echo get_site_url(null, '/my-account/view-order/' . $parent_data['order_id'] , 'https' ); ?>" >this link</a>.
        <br /><br />
        If you have any issues, or questions, please don't hesitate to get in touch with us by replying to this email!
        <br /><br />
        Thank you,<br />
        The TARKA London Team<br />
        Web: <a href="https://www.tarkalondon.com">http://www.tarkalondon.com/</a>
        <br />
</div>
<?php
/*
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );

