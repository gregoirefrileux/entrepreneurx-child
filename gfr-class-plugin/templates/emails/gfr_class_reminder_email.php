<?php
/**
 * Customer completed order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-completed-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', "TARKA Class Reminder", '' ); ?>

<p>Dear <?php echo $parent_data['parent_first_name'] ?>,</p>

<div>   This is a reminder for your reservation of your TARKA Class tomorrow (<?php echo date('l jS F Y', strtotime('tomorrow')) ?>) at <?php echo $parent_data['start-time'] ?>.
        <br />The class will take place at our <?php echo $parent_data['location'] ?> location (<?php echo $parent_data['address'] ?>).
        <br />Please arrive at least five minutes before class. We require 24 hours notice of cancellation.
        <br />
        TARKA is a nut free zone.
        <br /><br />
        If you are not collecting your child from the class then please update the name of your 
        Approved Carers on your account page (<a href="https://www.tarkalondon.com/my-account/approved-carer/">please click here) 
        <br /><br />
        Thank you!<br />
        The TARKA London Team<br />
        Web: <a href="https://www.tarkalondon.com">http://www.tarkalondon.com/</a>
        Tel: 0207 164 6281
        <br />
</div>
<?php
/*
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );

