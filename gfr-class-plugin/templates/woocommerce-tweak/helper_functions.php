<?php

/*
 *
 * Customising the way dates are shown when someone has a TERM product in their cart:
 * takes all the linked products
 * display all the dates
 *
 */
 
function gfr_display_term_dates( $product_id ) {
    // retrieve all the dates
    $dates_array    = get_class_dates($product_id);

    $link = admin_url('admin-ajax.php?action=create_ics&class_id='.$product_id);
    
    $html =''; 
    foreach($dates_array as $date) {
        $formatted_date = DateTime::createFromFormat('Y-m-d', $date);
        $html .= $formatted_date->format('jS M Y') . ', ';
    }

    $html .= '<a class="class_ics" href="' . $link . '">Download to your calendar</a>';

    return $html;
}

add_filter( 'woocommerce_cart_display_term_dates' , 'gfr_display_term_dates' );


/*
 *
 * Customising the quantity input in teh cart todisable it
 * This is because classes are added via the calendar, and kids need to be added, so it's
 * awkward to be able to change quantity in the cart
 *
 */
function gfr_remove_editable_qty_from_cart($product_quantity, $cart_item_key) {
    $cart_item = WC()->cart->cart_contents[ $cart_item_key ];

    $qty_html = '<span class="woocommerce-Price-amount amount">
                    <span style="font-size: 16px;">
                        ' . $cart_item['quantity'] . ' 
                    </span>
                </span>';
    echo $qty_html;
}

add_filter( 'woocommerce_cart_item_quantity',  'gfr_remove_editable_qty_from_cart', 10, 2);


?>