<?php

// Add admin menu entries
add_action('admin_menu', 'gfr_add_menu');
// Include admin helper classes
require_once('includes/admin/class-admin-create-term-classes.php');
require_once('includes/admin/class-admin-create-holiday-classes.php');
require_once('includes/admin/class-admin-create-custom-order-report.php');
require_once('includes/admin/dompdf/autoload.inc.php');

// enqueue styles
$gfr_booking_screens =['toplevel_page_classes', 'classes_page_class-detail'];

// enqueue styles
function gfr_enqueue_admin_scripts($hook)
{
    // enqueue scripts
    global $gfr_booking_screens;

    if (in_array($hook, $gfr_booking_screens)) {
        wp_enqueue_script('gfr-admin-js', get_stylesheet_directory_uri() . '/gfr-class-plugin/assets/js/admin-functions.js', array(), '1.0');
        wp_enqueue_script('spin-js', get_stylesheet_directory_uri() . '/gfr-class-plugin/assets/js/spin.min.js', array());
        wp_enqueue_script('spin-jquery', get_stylesheet_directory_uri() . '/gfr-class-plugin/assets/js/spin.jquery.js', array(), '2.0.1');
        // useful for date picker on admin calendar
        wp_enqueue_script('jquery-ui-datepicker');

        wp_enqueue_style('gfr-admin', get_stylesheet_directory_uri() . '/gfr-class-plugin/assets/css/admin-styles.css', array(), '1.0');
    }
}
add_action('admin_enqueue_scripts', 'gfr_enqueue_admin_scripts');

// Declaring the ajax functions
add_action('wp_ajax_admin_load_week_classes', 'gfr_ajax_load_classes_for_week');
add_action('wp_ajax_admin_reload_admin_table', 'gfr_ajax_reload_admin_table');
// now the ones for creating the term classes - we do this by initialising our class that handles all this
$createTermClasses = new AdminCreateTermClasses();
$createTermClasses->init();
// now the ones for creating the term classes - we do this by initialising our class that handles all this
$createHolidayClasses = new AdminCreateHolidayClasses();
$createHolidayClasses->init();

// Add menu pages in the admin interface
function gfr_add_menu()
{
    add_menu_page('Classes', 'Classes', 'manage_options', 'classes', 'gfr_admin_calendar', 'dashicons-calendar-alt', 58);
    add_submenu_page('classes', 'Class Detail', 'Class Detail', 'manage_options', 'class-detail', 'gfr_admin_class_detail', 58);
    add_submenu_page('classes', 'Generate Term Classes', 'Generate Term Classes', 'manage_options', 'generate-term-products', 'gfr_admin_generate_term_classes', 59);
    add_submenu_page('classes', 'Generate Holiday Classes', 'Generate Holiday Classes', 'manage_options', 'generate-holiday-products', 'gfr_admin_generate_holiday_classes', 59);
    add_submenu_page('classes', 'Download order report', 'Download order report', 'manage_options', 'get-order-report', 'gfr_admin_get_order_report', 60);
}


// Show the list of classes inside the main page

function gfr_admin_calendar()
{
    // if(!current_user_can('edit_classes')) {
    //     wp_die(esc_html__('You do not have sufficient permissions to access this page.', 'booked'));
    // }

    include("templates/wordpress-admin/admin-calendar.php");
}

// Show the list of classes inside the main page

function gfr_admin_class_detail()
{
    include("templates/wordpress-admin/admin-calendar-class-detail.php");
}


// Show the screen to generate automated term bundles

function gfr_admin_generate_term_classes()
{
    include("templates/wordpress-admin/admin-calendar-generate-term-classes.php");
}

// Show the screen to generate automated holiday class bundles

function gfr_admin_generate_holiday_classes()
{
    include("templates/wordpress-admin/admin-calendar-generate-holiday-classes.php");
}

// Show the page where a user can generate the Order Report
function gfr_admin_get_order_report()
{
    include("templates/wordpress-admin/admin-woocommerce-custom-order-report.php");
}

//////////////////

// NOW FOR SOME FUNCTIONS TO DISPLAY HTML

//////////////////


/*
 *
 * Function to display the table view which shows all the classes for a given week
 * Shown inside the main admin page
 * Takes in a week start Date in format date(d-M-Y) (10 Jan 2019)
 *
 * Returns some html
 *
 */

function gfr_build_admin_list_view_classes_for_week($week_start, $location = 'all', $classname = 'all')
{
    $table_html = '<table class="admin-week-table wp-list-table widefat fixed striped posts">
        <thead>
            <tr>
                <th colspan="5">
                    <a href="#" data-goto="' . date('Y-m-d', strtotime($week_start . '- 7 day')) . '" class="page-left"><i class="fa fa-arrow-left"></i></a>
                    <span class="calendarSavingState" style="display: none;">
                        <i class="fa fa-spinner fa-spin"></i>
                    </span>
                    <span class="weekName" data-date="' . $week_start . '">
                        Week commencing ' . date('dS M Y', strtotime($week_start)) .'
                        <a href="#" class="backToCurrentWeek" data-goto="' . date('Y-m-d', strtotime('monday this week')). '">Back to current week</a>
                    </span>
                    <a href="#" data-goto="' . date('Y-m-d', strtotime($week_start . '+ 7 day')) . '" class="page-right"><i class="fa fa-arrow-right"></i></a>
                </th>
            </tr>
            <tr>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Class Name</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Class Date</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Class Location</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Bookings</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Actions</span>
                </th>
            </tr>
        </thead>
        <tbody>';
    $table_html .= admin_show_classes_for_week($week_start, $location, $classname);
    $table_html .='</tbody>
    </table>';

    return $table_html;
}

/*
 *
 * Function to display the table view which shows all the classes for a given week
 * Shown inside the main admin page
 * Takes in a week start Date in format date(d-M-Y) (10 Jan 2019)
 *
 * Returns some html
 *
 */

function gfr_build_admin_list_view_classes_for_day($date, $location = "all", $class_name = "all")
{
    $table_html = '<table class="admin-week-table wp-list-table widefat fixed striped posts">
        <thead>
            <tr>
                <th colspan="5">
                    <span class="calendarSavingState" style="display: none;">
                        <i class="fa fa-spinner fa-spin"></i>
                    </span>
                    <span>
                        Date: ' . $date . '
                    </span>
                </th>
            </tr>
            <tr>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Class Name</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Class Date</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Class Location</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Bookings</span>
                </th>
                <th class="manage-column column-order_number column-primary sortable desc">
                    <span>Actions</span>
                </th>
            </tr>
        </thead>
        <tbody>';
    $table_html .= admin_show_classes_for_day($date, $location, $class_name);
    $table_html .='</tbody>
    </table>';

    return $table_html;
}

/* Total Number of bookings */
function admin_show_total_booking_for_week($week_start_date, $location = 'all', $classname = 'all', $week_class_list)
{

    // Some init
    $week_start = date('Y-m-d', strtotime($week_start_date));
    $week_end = date('Y-m-d', strtotime($week_start_date .'+ 6 days'));
    $bookingarray = [];
    $html ='';

    // Start building some HTML
    foreach ($week_class_list as $date => $class_list) {
        // We have a class list to display for a particular day
        foreach ($class_list as $class) {
            $booking=explode("/", gfr_admin_get_bookings($class['id']));
            $value = array($class['location'] =>  $booking[0]);
            $bookingarray[] = $value;
        }
    }
    $newbookings = array();
    foreach ($bookingarray as $value) {
        foreach ($value as $key => $value) {
            if (isset($newbookings[$key])) {
                $newbookings[$key] += $value;
            } else {
                $newbookings[$key] = $value;
            }
        }
    }
    $totalbooking = array_sum($newbookings);
    $html .= '<div class="total_bookings">';
    foreach ($newbookings as $key => $value) {
        $html .= '<b> Total booking in '. $key .': '. $value.'&nbsp &nbsp &nbsp</b>';
    }
    $html .='<b>Total Booking for the Week: '.$totalbooking.'</b></div>';

    return $html;
}

/*
 *
 * Function to display the list of classes and the relevant admin buttons
 * Shown inside the main admin page
 * Takes in a week start Date in format date(d-M-Y) (10 Jan 2019)
 *
 * Returnsd some html
 *
 */

function admin_show_classes_for_week($week_start_date, $location = 'all', $classname = 'all')
{

    // Some init
    $week_start = date('Y-m-d', strtotime($week_start_date));
    $week_end = date('Y-m-d', strtotime($week_start_date .'+ 6 days'));
    $html ='';

    // Let's pull out all the classes IDs that are happening this week
    $week_class_list = get_single_classes_for_one_week($week_start, $week_end, $location, $classname);

    // Start building some HTML
    foreach ($week_class_list as $date => $class_list) {
        // We have a class list to display for a particular day
        // bizarely the classes are in the wrong order, with classes late in the day shown first which is confusing
        // sort by start-time
        usort($class_list, 'gfr_compare_start_time');

        $html .= "<tr><td><h2>$date</h2></td></tr>";

        foreach ($class_list as $class) {
            $html .= '<tr class="admin-class-row" data-class-id=' . $class['id'] . '>
                        <td>
                            <div class="class-name">' . $class['class-name'] . ' - ' . $class['start-time'] . '</div>
                        </td>
                        <td>
                            <div class="class-date">' . date('d M Y', strtotime($class['class-date'])) . '</div>
                        </td>
                        <td>
                            <div class="class-location">' . $class['location'] . '</div>
                        </td>
                        <td>
                            <div class="class-date">' . gfr_admin_get_bookings($class['id'], true) . '</div>
                        </td>
                        <td>
                            <div class="class-date">
                                <a href="/wp-admin/admin.php?page=class-detail&class-id=' . $class['id'] . '">View Detail</a>
                            </div>
                        </td>
                    </tr>';
        }
    }
    $html .= '<tr><td colspan="5">'.admin_show_total_booking_for_week($week_start, $location, $classname, $week_class_list).'</td></tr>';

    return $html;
}

function admin_show_classes_for_day($date, $location, $class_name)
{
    $html ='';

    $products = get_single_classes_for_one_day($date, $location, $class_name);

    $class_list = format_single_classes_for_admin($products)[$date];

    // Start building some HTML
    foreach ($class_list as $class) {
        $html .= '<tr class="admin-class-row" data-class-id=' . $class['id'] . '>
                  <td>
                      <div class="class-name">' . $class['class-name'] . ' - ' . $class['start-time'] . '</div>
                  </td>
                  <td>
                      <div class="class-date">' . date('d M Y', strtotime($class['class-date'])) . '</div>
                  </td>
                  <td>
                      <div class="class-location">' . $class['location'] . '</div>
                  </td>
                  <td>
                      <div class="class-date">' . gfr_admin_get_bookings($class['id'], true) . '</div>
                  </td>
                  <td>
                      <div class="class-date">
                          <a href="/wp-admin/admin.php?page=class-detail&class-id=' . $class['id'] . '">View Detail</a>
                      </div>
                  </td>
              </tr>';
    }

    return $html;
}


/*
 *
 * Function to display the number of kids booked in a class
 * Shown inside the main admin page
 * Takes in a $class product which looks like this
 *
 *
Array
(
    [id] =&gt; 1085
    [location] =&gt; Chelsea
    [name] =&gt; Tarka (3yrs-4.5yrs)     - wednesday - 1600 - chelsea - 2019-01-30
    [class-name] =&gt; Tarka (3yrs-4.5yrs)
    [stock] =&gt; 20
    [term-class] =&gt;
    [start-time] =&gt; 16:00
    [finish-time] =&gt; 17:00
    [day-of-the-week] =&gt; Wednesday
    [term-name] =&gt; Spring 2019
    [class-date] =&gt; 2019-01-30
    [price] =&gt; 29.99
)


 *
 * and then it searches for orders related to that product
 *
 * Returns a number
 *
 */
function gfr_admin_get_bookings($class_id, $showMiscount = false)
{
    $_product = wc_get_product($class_id);

    $remaining_stock        = $_product->get_stock_quantity();
    // The below are the orders where the product appears
    $total_sales            = gfr_total_product_bought_regardless_stock_counted($class_id);
    // This one are orders where the product appears AND the stock was correctly accounted for
    // (not always the same as above) when manual operators make mistake in the Admin
    $total_counted_sales    = jdr_qty_sold_by_product_id($class_id);
    $initialStock           = $total_counted_sales + $remaining_stock;

    if(!$showMiscount) {
        // This is used to produce the overall week count so no fine detail
        return $total_sales . "/" . $initialStock;
    } else {
        // this is used in the HTML table
        $html = $total_sales . "/" . $initialStock;
        if($total_sales > $total_counted_sales) {
            $html = $html . " Unaccounted but sold spots: " . ($total_sales - $total_counted_sales) ;
        }
        return $html;
    }
}

function jdr_qty_sold_by_product_id($product_id)
{
  global $wpdb;

  $order_item_ids = get_order_item_ids_for_product( $product_id );

  $sql = "
  SELECT SUM(meta_value) FROM {$wpdb->prefix}woocommerce_order_itemmeta
    WHERE meta_key = '_reduced_stock' AND order_item_id IN (" . implode(',', $order_item_ids) . ")
  ";

  $results = $wpdb->get_var($sql);
  
  if(is_null($results)) {
    error_log("[jdr_qty_sold_by_product_id] Error on query : " . print_r($wpdb->last_query, true) );
  }

  return $results;
}

function gfr_total_product_bought_regardless_stock_counted($product_id)
{
    $quantity = 0;
    $all_order_ids = get_orders_ids_by_product_id( $product_id );
    // sometimes and order can be included twice. We don't need to parse it twice. If it has multiple time the same class that's already handled
    $unique_order_ids=array_unique($all_order_ids);
  
    foreach ($unique_order_ids as $order_id) {
        // load the current order
        $order = wc_get_order($order_id);
        if (!$order) {
            // we do nothing
        } else {
            // We need to retrieve all products in that order, and count only the current class
            foreach ($order->get_items() as $item_id => $item_product) {
                // what's the product sold in this item line ?
                $WCProduct_id = $item_product->get_product_id();
                if ($WCProduct_id === $product_id) {
                    $quantity = $quantity + $item_product->get_quantity();
                }
            }
        }
    }
    return $quantity;
  }

function jdr_get_orders_ids_and_item_ids_from_product_id($product_id, $orders_statuses = [ 'wc-completed' ])
{
    global $wpdb;

    $orders_statuses = "'" . implode("', '", $orders_statuses) . "'";

    return $wpdb->get_results(
        "
        SELECT DISTINCT woi.order_id, woi.order_item_id
        FROM {$wpdb->prefix}woocommerce_order_itemmeta as woim,
             {$wpdb->prefix}woocommerce_order_items as woi,
             {$wpdb->prefix}posts as p
        WHERE  woi.order_item_id = woim.order_item_id
        AND woi.order_id = p.ID
        AND p.post_status IN ( $orders_statuses )
        AND woim.meta_key IN ( '_product_id', '_variation_id' )
        AND woim.meta_value LIKE '$product_id'
        ORDER BY woi.order_item_id DESC"
    );
}

/**
 * Get All orders IDs for a given product ID.
 *
 * @param  integer  $product_id (required)
 * @param  array    $order_status (optional) Default is 'wc-completed'
 *
 * @return array
 */
function get_orders_ids_by_product_id($product_id, $order_status = array( 'wc-pending', 'wc-processing', 'wc-completed' ))
{
    global $wpdb;

    $results = $wpdb->get_col("
        SELECT order_items.order_id
        FROM {$wpdb->prefix}woocommerce_order_items as order_items
        LEFT JOIN {$wpdb->prefix}woocommerce_order_itemmeta as order_item_meta ON order_items.order_item_id = order_item_meta.order_item_id
        LEFT JOIN {$wpdb->posts} AS posts ON order_items.order_id = posts.ID
        WHERE posts.post_type = 'shop_order'
        AND posts.post_status IN ( '" . implode("','", $order_status) . "' )
        AND order_items.order_item_type = 'line_item'
        AND order_item_meta.meta_key = '_product_id'
        AND order_item_meta.meta_value = '$product_id'
    ");

    return $results;
}


/*
 *
 * Function to display the list of kids booked for a specific class
 * Shown inside the admin page showing the bookings for a class
 *
 * Takes in a WC_Product_simple class and a list of related order IDs
 *
 * Returns some html
 *
 */

function admin_show_booking_list_for_class($class)
{
  global $wpdb;

  $all_order_ids = get_orders_ids_by_product_id( $class->get_id() );
  // sometimes and order can be included twice. We don't need to parse it twice. If it has multiple time the same class that's already handled
  $unique_order_ids=array_unique($all_order_ids);

  $row_id=1;

  $html = '';

  // Start building some HTML
  foreach ($unique_order_ids as $order_id) {
      // load the current order
      $order = wc_get_order($order_id);

      if (!$order) {
          // this is not a valid order ID
          // we do nothing
      } else {
          // We need to retrieve all products in that order, and display the ones related to the current class
          foreach ($order->get_items() as $item_id => $item_product) {
              // what's the product sold in this item line ?
              $product_id = $item_product->get_product_id();
              // was this product refunded ?
              if ($product_id === $class->get_id()) {
                  // this product is the class our admin user is currently viewing
                  // Is it part of a bundle ?
                  $parent_bundle_id = $item_product->get_meta('_woosb_parent_id');
                  // now we can form the $prod_id we'll use later to get the child meta
                  // if the class was part of a bundle, the child detail will be stored in the Bundle, not the individual class
                  $product_id_for_meta = $parent_bundle_id && $parent_bundle_id != '' ? gfr_get_item_id_from_product_id($order, $parent_bundle_id) : $item_id;

                  $quantity = $item_product->get_quantity();

                  for ($i = 0; $i < $quantity ; $i++) {
                      // someone may have ordered severals spots for the same class
                      $child_array = wc_get_order_item_meta($product_id_for_meta, 'child-detail');

                      if (!is_array($child_array) || (is_array($child_array) && sizeof($child_array) === 0)) {
                          $child_detail = gfr_initialize_empty_child_array();
                      } else {
                          $child_keys = array_keys($child_array);
                          $child_detail = $child_array[$child_keys[$i]];
                      }

                      // calculate the age, which is more complicated than it should be
                      // Unfortunately, the data is not clean: some birthdates are in proper 21/05/2016, others are missing the "/"  : 21052916
                      $birthDate = array();
                      if (strpos($child_detail['dob'], "/") === false) {
                          $birthDate[0] = (int)substr($child_detail['dob'], 0, 2);
                          $birthDate[1] = (int)substr($child_detail['dob'], 2, 2);
                          $birthDate[2] = (int)substr($child_detail['dob'], 4, 4);
                      } else {
                          $birthDate = explode("/", $child_detail['dob']);
                      }
                      //get age from date or birthdate
                      $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
                      ? ((date("Y") - $birthDate[2]) - 1)
                      : (date("Y") - $birthDate[2]));

                      // init empty keys to avoid warning
                      if (!isset($child_detail['gender'])) {
                          $child_detail['gender'] = '';
                      }
                      if (!isset($child_detail['nursery'])) {
                          $child_detail['nursery'] = '';
                      }
                      // see if the the child is new (first order for this parent)
                      $new = is_first_class($order->get_customer_id(), $i);

                      // see if the order has been paid or if it's still in pending status
                      $order->get_status() === 'pending' ? $paid = 'pending' : $paid = 'paid';

                      $did_attend = did_child_attend($order_id, $class->get_id(), $i);

                      $html .= '<tr class="admin-class-row" data-order-id="' . $order_id . '" data-child-key="' . $i . '">
                      <td>
                          <div class="class-name">' . ($row_id) . '</div>
                      </td>
                      <td>
                          <div class="class-name"><div class="print-only"><input type="checkbox" id="attendance" name="attendance" ' . ($did_attend ? "checked" : "") . '></div></div>
                      </td>
                      <td>
                          <div class="class-name">' . $child_detail['firstname'] . ' ' . $child_detail['lastname'] . '</div>
                      </td>
                      <td>
                          <div class="class-icon">' . ($new ? '<i class="fa fa-plus-circle"></i>' : '<i class="fa fa-minus"></i>')  . '</div>
                      </td>
                      <td>
                          <div class="class-name">' . $age . ' <span class="class-name-dob print-no">(' . $child_detail['dob'] . ')</span></div>
                      </td>
                      <td class="print-no">
                          <div class="class-name">' . $child_detail['gender'] .'</div>
                      </td>
                      <td>
                          <div class="class-name">' . $child_detail['notes'] .'</div>
                      </td>
                      <td class="print-no">
                          <div class="class-name">' . $child_detail['nursery'] . '</div>
                      </td>
                      <td class="print-no">
                          <div class="class-icon">' . ($paid === 'pending' ? '<i class="fa fa-exclamation-circle"></i>' : '<i class="fa fa-check"></i>')  . '</div>
                      </td>
                      <td>
                          <div class="class-date">' . $order->get_billing_first_name().' '. $order->get_billing_last_name() . '</div>
                      </td>
                      <td>
                          <div class="class-location">' . $order->get_billing_phone() . '</div>
                      </td>
                      <td class="print-no">
                          <div class="class-location print-no">' . $order->get_billing_email() . '</div>
                      </td>
                      <td>
                          <div class="class-location">' . get_the_author_meta('nanny', $order->get_user_id()) . '</div>
                      </td>
                      <td class="print-no">
                          <p><a href="/wp-admin/post.php?post='. $order->get_id() .'&action=edit" class="print-no">View Order</a></p>
                          <p><a href="mailto:' . $order->get_billing_email() . '" class="print-no">Email parent</a></p>
                      </td>
                  </tr>';

                      // increment the row
                      $row_id +=1;
                  }
              }
          }
      }
  }
  return $html;
}

/*
 * Function to get the WC_Order_Item Id based on a WC_PRODUCT id
 */
function gfr_get_item_id_from_product_id($order, $parent_bundle_id)
{
    foreach ($order->get_items() as $item_id => $item_product) {
        if ($item_product->get_product_id() == $parent_bundle_id) {
            return $item_id;
        }
    }
    // if we get here we must have found nothing
    return null;
}


/*
 *
 * Function to check if a specific item in an order has been refunded or not
 *
 * ARGS:
 * $order: WC_Order
 * $item_id: number, the id of the item to be checked
 * $item: WC_ORDER_ITEM, the item to be checked
 *
 * Returns TRUE if the item has been refunded
 *
 */

function gfr_check_item_refunded_in_order($order, $product_id)
{
    $result = false;

    // Get the Order refunds (array of refunds)
    $order_refunds = $order->get_refunds();

    // Loop through the order refunds array
    foreach ($order_refunds as $refund) {
        // Loop through the order refund line items
        foreach ($refund->get_items() as $refunded_item_id => $refunded_item) {
            // if a specific product has been refunded, then the refunded_item should have a porduct_id associated with it
            if ($refunded_item->get_product_id() === $product_id) {
                return true;
            }
        }
    }


    return $result;
}



/*
 *
 * Function to load the classes for the week when called in AJAX
 * when someone clicks on "previous week" or "next week" in the admin
 *
 * Takes in a a date and location
 *
 * Returns some html
 *
 */
function gfr_ajax_load_classes_for_week()
{

    // TODO add functions to calculate the default value of Term dynalmically with the date
    $location = (isset($_POST['location']) ? $_POST['location'] : 'all');

    $gotoWeek = (isset($_POST['gotoWeek']) ? $_POST['gotoWeek'] : 'noval');

    echo gfr_build_admin_list_view_classes_for_week($gotoWeek, $location);

    wp_die();
}

/*
 *
 * Function to load the classes for the day when called in AJAX
 *
 * Takes in a a date and location
 *
 * Returns some html
 *
 */
function gfr_ajax_load_classes_for_day()
{
    $location = (isset($_POST['location']) ? $_POST['location'] : 'all');

    $class_name = (isset($_POST['className']) ? $_POST['className'] : 'all');

    echo gfr_build_admin_list_view_classes_for_day($_POST[ "date" ], $location, $class_name);

    wp_die();
}
add_action('wp_ajax_admin_load_day_classes', 'gfr_ajax_load_classes_for_day');


/*
 *
 * Function to load the classes for the Admin week when called in AJAX
 * when someone clicks on "Change Location" or "change Term" in the admin
 *
 * Takes in a a date and location
 *
 * Returns some html
 *
 */
function gfr_ajax_reload_admin_table()
{

    // TODO add functions to calculate the default value of Term dynalmically with the date
    $location = (isset($_POST['location']) ? $_POST['location'] : 'all');

    $classname = (isset($_POST['classname']) ? $_POST['classname'] : 'all');

    $gotoWeek = (isset($_POST['gotoWeek']) ? $_POST['gotoWeek'] : 'noval');

    $date = $_POST['date'] == "" ? null : $_POST['date'];

    if (is_null($date)) {
        echo gfr_build_admin_list_view_classes_for_week($gotoWeek, $location, $classname);
    } else {
        echo gfr_build_admin_list_view_classes_for_day($date, $location, $classname);
    }

    wp_die();
}

/*
 * Simple helper to return an empty array
 * Helps to avoid errors and warnings when an admin order has been entered with no details
 *
 */

function gfr_initialize_empty_child_array()
{
    $child_array = array();
    $child_array['firstname'] = 'no child detail';
    $child_array['lastname'] = '';
    $child_array['dob'] = '';
    $child_array['gender'] = '';
    $child_array['nursery'] = '';
    $child_array['notes'] = '';

    return $child_array;
}

/*
 *
 * Helper functions to format classes in an easily manageable format to build
 * Admin class list pages
 *
 */

function format_single_classes_for_admin($WCproducts)
{

    // a simple list of all products
    $product_list = [];
    // now we initialize the final list
    $class_list = [];

    foreach ($WCproducts as $WCproduct) {
        array_push(
            $product_list,
            [
            get_cust_att($WCproduct, 'pa_class-date') => array(
                $WCproduct->get_id() => array(
                    'id'        => $WCproduct->get_id(),
                    'location'  => get_cust_att($WCproduct, 'pa_location'),
                    'name'      => $WCproduct->get_title(),
                    'class-name'=> get_cust_att($WCproduct, 'pa_class-name'),
                    'stock'     => $WCproduct->get_stock_quantity(),
                    'term-class'=> get_cust_att($WCproduct, 'pa_term-class'),
                    'start-time'=> get_cust_att($WCproduct, 'pa_start-time'),
                    'finish-time'=> get_cust_att($WCproduct, 'pa_finish-time'),
                    'day-of-the-week'=> get_cust_att($WCproduct, 'pa_day-of-the-week'),
                    'term-name'=> get_cust_att($WCproduct, 'pa_term-name'),
                    'class-date'=> get_cust_att($WCproduct, 'pa_class-date'),
                    'price'     => $WCproduct->get_price()
                    )
                )
            ]
        );
    }

    for ($i=0; $i < sizeof($product_list); $i++) {
        if ($i === 0) {
            $class_list = $product_list[0];
        } else {
            $class_list = array_merge_recursive($class_list, $product_list[$i]);
        }
    }

    // order the classes in order of date
    ksort($class_list, 0);

    return($class_list);
}

/*
 *
 * Helper functions to determine if the class requested is the first ever class for a given child
 * Args:
 *  WC_Order the order that contains the class and child we want to check
 *  WC_Product: the Product instance of the class we're checking
 *  Child_array: the child detail we want to check
 *
 * Returns true or false
 *
 */
function gfr_is_this_first_ever_class_for_child($order, $product, $child_detail)
{

    // Some init
    $result = false;
    $parent_id = $order->get_user_id();
    // 1. Retrieve all the orders for this parent
    $all_orders_for_parent = gfr_get_all_orders_for_parent($parent_id);

    // 2. Filter the orders that have classes for the child we're currently interested in
    // This returns an array of formatted classes
    $classes_for_child = gfr_return_classes_for_child($all_orders_for_parent, $child_detail, true);

    // 3. OK, so now we have all the class that a child attends. And we have the Id of the class we want to check
    // 3.1 Let's get the date of the class we want to check
    $class_date = get_cust_att($product, 'pa_class-date');
    // 3.2 Now let's check if this date is the first ever in the list of class
    $result = gfr_check_class_is_first_ever($classes_for_child, $class_date);

    return $result;
}


/*
 *
 * Helper functions to get all order given a user_id
 * Takes an Int : parent_id
 *
 * Returns array of orders or null
 *
 */
function gfr_get_all_orders_for_parent($parent_id)
{
    // Get all customer orders
    $customer_orders = get_posts(array(
        'numberposts' => -1,
        'meta_key' => '_customer_user',
        'orderby' => 'date',
        'order' => 'DESC',
        'meta_value' => $parent_id,
        'post_type' => wc_get_order_types(),
        'post_status' => array_keys(wc_get_order_statuses()), 'post_status' => array('wc-pending', 'wc-processing', 'wc-completed'),
    ));

    $order_array = []; //
    foreach ($customer_orders as $customer_order) {
        $WC_Order = wc_get_order($customer_order);
        array_push($order_array, $WC_Order);
    }

    return (sizeof($order_array) > 0) ? $order_array : null;
}


/*
 *
 * Helper functions to filter and format all the classes for a child from a list of orders given
 * Usually the use case is that the function receives all the orders for a parent, and would return all the classes for a child
 * Say Jane has two kids, Tim and Jake, we would pass all Jane's orders and return all the classes that Jake is signed up to
 * Args
 *  Takes an Array of WC_Order object, usually would be all the orders for the parent
 *  Child detail array ['firstname' => "Jake", 'lastname' => "Test", 'dob' => "30/06/2019"]
 *  Bool: formatted : whether the result should be formatted or not
 *
 * Returns array of formatted classes or null if no classes are found
 *
 */
function gfr_return_classes_for_child($order_array, $child_detail_to_check, $formatted = true)
{
    if (sizeof($order_array) == 0 || !is_array($order_array)) {
        // we need a valid array with at least 1 order
        return null;
    }

    $product_list = [];

    foreach ($order_array as $order) {
        $items = $order->get_items();
        foreach ($items as $item_id => $item_data) {
            // get the child meta
            $child_array = wc_get_order_item_meta($item_id, 'child-detail');

            if (!is_array($child_array) || (is_array($child_array) && sizeof($child_array) === 0)) {
                // this item doesn't have anything we're interested in
            } else {
                $child_keys = array_keys($child_array);
                // we might have several kids booked on the same class (i.e. the same $item)
                for ($i=0; $i < sizeof($child_keys); $i++) {
                    $child_detail = $child_array[$child_keys[$i]];
                    if ($child_detail['firstname'] == $child_detail_to_check['firstname']) {
                        // we found a class that our child is going
                        array_push($product_list, $item_data->get_product());
                    } else {
                        // the child for this item is not the one we want - do nothing
                    }
                }
            }
        }
    }

    // ok we should now have a list of WC_Products

    // if the result should be formatted we do it now
    if ($formatted) {
        return format_single_classes_for_admin($product_list);
    }

    // otherwise we just return a list of WC_Products
    return $product_list;
}

/*
 *
 * Helper functions to determine if the date that is given is the first ever from teh list of class
 * Args
 *  Array of $classes formatted as per format_single_classes_for_admin function
 *  string: class-date, smple string such as 2019-09-02
 *
 *
 * Returns Bool : true if date given is the first ever. False if not
 *
 */
function gfr_check_class_is_first_ever($classes_for_child, $class_date)
{
    $result = true;
    // start from true - if we end up finding an earlier date we'll set to false
    foreach ($classes_for_child as $class) {
        $class_detail = array_pop($class);
        if (strtotime($class_detail['class-date']) < strtotime($class_date)) {
            $result = false;
            return $result;
        }
    }

    return $result;
}

function get_db_friendly_title( $post_id ) {
  $title = htmlspecialchars( get_the_title( $post_id ) );
  $title = str_replace( "8211;", "-", $title );
  $title = str_replace( "#", "", $title );
  $title = str_replace( "&", "", $title );
  $title = str_replace( "amp;", "", $title );
  return $title;
}

function get_order_item_ids_for_product( $product_id ) {
  global $wpdb;

  $title = get_db_friendly_title( $product_id );

  $order_item_ids = $wpdb->get_results(
      "
      SELECT {$wpdb->prefix}woocommerce_order_items.order_item_id FROM {$wpdb->prefix}woocommerce_order_items
        INNER JOIN {$wpdb->prefix}posts
          ON {$wpdb->prefix}posts.ID = {$wpdb->prefix}woocommerce_order_items.order_id
      WHERE order_item_name = '$title'
        AND {$wpdb->prefix}posts.post_type = 'shop_order'
        AND {$wpdb->prefix}posts.post_status IN ( 'wc-completed', 'wc-pending' )
      "
  );

  return array_map(function ($o) { return $o->order_item_id; }, $order_item_ids);
}

function get_valid_order_ids_for_product( $product_id ) {
  global $wpdb;

  $order_item_ids = get_order_item_ids_for_product( $product_id );

  //remove refunded items
  $sql = "
    SELECT order_item_id FROM {$wpdb->prefix}woocommerce_order_itemmeta
    WHERE order_item_id IN (" . implode(',', $order_item_ids) . ")
      AND meta_key = '_reduced_stock'
      AND meta_value > 0
  ";
  $order_item_ids = $wpdb->get_results($sql);
  $order_item_ids = array_map(function ($o) { return $o->order_item_id; }, $order_item_ids);

  $sql = "
    SELECT order_id FROM {$wpdb->prefix}woocommerce_order_items
    WHERE order_item_id IN (" . implode(',', $order_item_ids) . ")
  ";
  $order_ids = $wpdb->get_results($sql);
  return array_map(function ($o) { return $o->order_id; }, $order_ids);
}
