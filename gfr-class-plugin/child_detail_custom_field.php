<?php
/*
 *
 * Function to save the children details added during an order process onto the client's user profile
 * 
 */
function gfr_save_child_detail_to_profile( $order_id ) {
     
    $order = new WC_Order( $order_id );
    $user_id = $order->get_user_id();
    $child_array = array();

    foreach ( $order->get_items() as  $item_id => $item ) {
        // Get the child array for this particular class
        $child_array_for_product = wc_get_order_item_meta( $item_id, 'child-detail', true );
        
        // we may have 2 kids in the same class, so we add them to our array one by one
        if( $child_array_for_product ) {
            foreach($child_array_for_product as $child) {
                array_push($child_array, $child );
            }
        }
    }
    // Before saving, we clean up the array to avoid duplicate (happens when parents place orders without logging in first)
    $clean_child_array = gfr_cleanup_duplicate_child_in_meta($child_array);

    // Now we add this array to our client profile
    update_user_meta( $user_id, 'child-detail', $clean_child_array );

    
}
add_action( 'woocommerce_thankyou', 'gfr_save_child_detail_to_profile');



/**
 * Add our custom scripts and style to the WooCommerce Edit Order page
 * to allow Admins to add child to class when editing orders
 * @since 1.0.0
 * 
 */
function add_admin_scripts( $hook ) {

    global $post;

    if ( $hook === 'post.php' || $hook === 'post-new.php' ) {

        if ( 'shop_order' === $post->post_type ) {     

            wp_enqueue_style( 'gfr-booking-style', get_stylesheet_directory_uri() . '/gfr-class-plugin/assets/css/booking.css', array(), '1.0.0', 'all' );
            wp_enqueue_style('gfr-booking-responsive', get_stylesheet_directory_uri() . '/gfr-class-plugin/assets/css/responsive.css', '1.0.0', 'all');
            wp_enqueue_style('formidable-css', '/wp-content/plugins/formidable/css/formidableforms.css');
            wp_enqueue_style( 'gfr-admin-style', get_stylesheet_directory_uri() . '/gfr-class-plugin/assets/css/admin-styles.css', array(), '1.0.0', 'all' );

            //Declare the shortcode style
            wp_enqueue_script( 'gfr-booking-js', get_stylesheet_directory_uri() . '/gfr-class-plugin/assets/js/booking.js', array(), JS_FRONT_BOOKING_JS_VER, 'all' );
            wp_enqueue_script('spin-js', get_stylesheet_directory_uri() . '/gfr-class-plugin/assets/js/spin.min.js', array(), '2.0.1');
            wp_enqueue_script('spin-jquery', get_stylesheet_directory_uri() . '/gfr-class-plugin/assets/js/spin.jquery.js', array(), '2.0.1');

                

        }
    }
}
add_action( 'admin_enqueue_scripts', 'add_admin_scripts', 10, 1 );



/**
 * Add the text field as item data to the cart object
 * @since 1.0.0
 * @param Array     $cart_item_data Cart item meta data.
 * @param Integer   $product_id     Product ID.
 * @param Integer   $variation_id   Variation ID.
 * @param Boolean   $quantity           Quantity
 */
function gfr_add_child_detail_data( $cart_item_data, $product_id, $variation_id, $quantity ) {

    if( ! empty( $_SESSION) && ! empty($_SESSION['child-detail']) ) {
        // Let's parse the children and add them to their respective product
        // If we have a single product with qty = 2 or 3, it means we have a parent booking a single class for 2 kids
        // And so because of this, we have to structure the cart_item_data as an array of arrays
        foreach ($_SESSION['child-detail'] as $key => $child) {
            $product_id_array = explode(",", $child['product_id']);
            if (array_search( $product_id, $product_id_array ) !== FALSE) {
                // add the current child to the cart_item_data
                $cart_item_data['child-detail'][$key] =  $child ;
            }
        }
    }
    return $cart_item_data;
}

add_filter( 'woocommerce_add_cart_item_data', 'gfr_add_child_detail_data', 10, 4 );
   
/**
 * Add children information to the order
 */
function gfr_save_child_detail_to_order( $item, $cart_item_key, $values, $order ) {
    foreach( $item as $cart_item_key=>$values ) {
        if( isset( $values['child-detail'] ) ) {
            // add the array )1 or more kids) to the item
            $item->add_meta_data( "child-detail", $values['child-detail'], true );
        }
    }
}

add_action( 'woocommerce_checkout_create_order_line_item', 'gfr_save_child_detail_to_order', 10, 4 );
  


/**
 * Display children information in My Account, and Order Detail page (client facing)
 * 
 * This is needed because we store an object and not a simple string and so WooCommerce can't display it
 * 
 */

 function gfr_show_child_detail_data($html, $item ) {

    $html_to_return = null;
    
    if( isset( $item['child-detail'] ) ) {
        $html_to_return ='<br />';
        //we have a child detail to display here
        $child_array = $item['child-detail'];
        foreach($child_array as $child) {
            // init empty keys to avoid warning
            if(!isset($child['nursery'])) {
                $child['nursery'] = '';
            }
            // build html
            $html_to_return .= '<span class="meta-child-detail">Child detail: ' . $child['firstname'] . ' ' . $child['lastname'] . ' - ' . $child['dob'] .'</span><br />';
            $html_to_return .= '<span class="meta-child-detail">Nursery: ' . $child['nursery'] .'</span><br />';
            if( $child['notes'] && $child['notes'] !== '' ) {
                $html_to_return .= '<span class="meta-child-detail-notes">Notes: ' . $child['notes'] . '</span><br />';
            }
        }
    }

    if ($html_to_return) {
        return $html_to_return;
    }
    return $html;

 }
add_filter( 'woocommerce_display_item_meta', 'gfr_show_child_detail_data', 10, 4 );

/**
 * 
 * Display children information in the Admin section, in the Order detail page
 * 
 * If no info exiss, it also adds the HTML to allow an admin to add some details
 * 
 */

function gfr_admin_show_child_detail_data($item_id, $item, $null ) {

    // get the quantity ordered
    $quantity = $item->get_quantity();
    // get the child details attached to this item (returns false if none are entered)
    $child_added_to_item = wc_get_order_item_meta($item_id, 'child-detail', true);
    // Now there might be a few cases:
    //    1. we have as many kids as products booked
    //    2. we have no kid detail entered yet
    //    1. we have some kid details but not as many as we need
    // so we may need to both display some info and also display the boxes that allow inputing new detail
    $number_of_kids_entered = $child_added_to_item ? sizeof($child_added_to_item) : 0;
    $number_of_kids_to_be_entered = $quantity - $number_of_kids_entered;

    $html_to_return = '';

    if( $child_added_to_item ) {
        $html_to_return .='<br />';
        //we have a child detail to display here
        $child_array = $item['child-detail'];
        foreach($child_array as $child) {
            // init potentially empty vars
            $gender = isset($child['gender']) ? $child['gender'] : 'no DOB';
            $nursery = isset($child['nursery']) ? $child['nursery'] : 'not provided';
            // build html
            $html_to_return .= '<span class="meta-child-detail">Child detail: ' . $child['firstname'] . ' ' . $child['lastname'] . ' - ' . $child['dob'] .'</span><br />';
            $html_to_return .= '<span class="meta-child-detail">Gender: ' . $gender . '  - Nursery: ' . $nursery . '</span><br />';
            if( $child['notes'] && $child['notes'] !== '' ) {
                $html_to_return .= '<span class="meta-child-detail-notes">Notes: ' . $child['notes'] . '</span><br />';
            }
        }
    } 
    if($number_of_kids_to_be_entered > 0 ) {
        // we need to show the input boxes to fill in kids details
        // we need some key details like product id and child present in the User Account
        $product_id = $item->get_product_id();
        // Does this parent already have kids saved in their profil ?
        $order = new WC_Order($item->get_order_id());
        $parent_user_id = $order->get_user_id();
        $child_array_on_account = gfr_get_meta($parent_user_id, 'child-detail');

        $html_to_return .= '<br />';
        for ($i=0; $i < $number_of_kids_to_be_entered; $i++) {
            // we may have for example a quantity of 3 seats booked, and 1 child entered
            // So we need to enter the detail for child 2 and 3 (not 1 and 2)
            $index = $i + $number_of_kids_entered;
            // now put the html of the little block to add child to order
            if(is_array($child_array_on_account)) {
                $html_to_return .= show_add_child_logged_in([$item_id], $index, $child_array_on_account, 'admin-select-child-for-order-item','admin-add-child-for-order-item','admin-remove-child-for-order-item', $order->get_id());
            } else {
                $html_to_return .= show_add_child_not_logged_in([$item_id], $index, 'admin-add-child-for-order-item');
            }
            $html_to_return .= '<br />';
        }
        // now add the HTML of the modal required to enter child detail
        $html_to_return .= show_child_edit_form_modal();
        // we also load the kids detail in a global window object so that the popup can access it
        $js_child_array = json_encode($child_array_on_account);
        $html_to_return .= '<script type="text/javascript">
            var savedChildDetail = window.savedChildDetail || [];
            window.savedChildDetail = ' . $js_child_array .';
            </script>';
        
    }

    if ($html_to_return) {
        echo $html_to_return;
    }

 }
add_action( 'woocommerce_before_order_itemmeta', 'gfr_admin_show_child_detail_data', 10, 4 );


/**
 * 
 * Save the Child Detail to a user profile when the order has successfully been placed
 * 
 */
function gfr_save_child_detail_to_profile_on_order( $order_id ) {
     
    $order = new WC_Order( $order_id );
    $user_id = $order->get_user_id();
    $child_array = array();

    foreach ( $order->get_items() as  $item_id => $item ) {
        // Get the child array for this particular class
        $child_array_for_product = wc_get_order_item_meta( $item_id, 'child-detail', true );
        // we may have 2 kids in the same class, so we add them to our array one by one
        if( $child_array_for_product ) {
            foreach($child_array_for_product as $child) {
                array_push($child_array, $child );
            }
        }
    }
    // Before saving, we clean up the array to avoid duplicate (happens when parents place orders without logging in first)
    $child_array = gfr_cleanup_duplicate_child_in_meta($child_array);
    // Now we add this array to our client profile
    update_user_meta( $user_id, 'child-detail', $child_array );

    
}
add_action( 'woocommerce_thankyou', 'gfr_save_child_detail_to_profile_on_order');


// NOW TO HANDLE THE WHOLE "MY ACCOUNT" AREA


/**
 * Add endpoint
 */
function gfr_add_my_account_endpoint() {
 
    add_rewrite_endpoint( 'child-detail', EP_PAGES );
 
}
add_action( 'init', 'gfr_add_my_account_endpoint' );


 /**
  * Edit my account menu order
  */

  function my_account_menu_order() {
    $menuOrder = array(
        'dashboard'          => __( 'Dashboard', 'woocommerce' ),
        'orders'             => __( 'Orders', 'woocommerce' ),
        'child-detail'      => 'Children Information',
        // 'downloads'          => __( 'Download', 'woocommerce' ),
        'edit-address'       => __( 'Addresses', 'woocommerce' ),
        'edit-account'      => __( 'Account Details', 'woocommerce' ),
        'customer-logout'    => __( 'Logout', 'woocommerce' ),
    );
    return $menuOrder;
}
add_filter ( 'woocommerce_account_menu_items', 'my_account_menu_order' );

/**
 * Now to display what will be shown in the nnew page
 * For Now we simply display a header and the saved info if any
 * 
 * TODO: add forms to allow parents to add children and also add buttons to edit info, and delete if need be
 * 
 * 
 */
function gfr_show_my_account_child_detail_section($user_id) {

    // enqueue the style on the page
    wp_enqueue_style( 'gfr-booking-style' );
    wp_enqueue_style( 'gfr-booking-responsive' ); 
    wp_enqueue_script('gfr-booking-js');
    wp_enqueue_script( 'spin-js');
    wp_enqueue_script( 'spin-jquery');


    echo '<h3 class="no-margin">Edit your children detail</h3>';

    $current_user = wp_get_current_user();
    $user_id = $current_user->ID;

    $html ='';
    $html .='<div class=my-account-child-detail>';

    if (! $current_user) {
        // user is not logged in and tried accessing the page - we exit
    } else {
        //retrieve the children details if any
        $child_array = gfr_get_meta($user_id, 'child-detail') ;

        if ($child_array && is_array($child_array) && sizeof($child_array) > 0) {
            // There are saved children from previous bookings - we display them here

            // The Child Array might need cleaning - typically when parents place order without loggin in first
            if (sizeof($child_array) !== sizeof(gfr_cleanup_duplicate_child_in_meta($child_array))) {
                update_user_meta( $user_id, 'child-detail', gfr_cleanup_duplicate_child_in_meta($child_array) );
                $child_array = gfr_cleanup_duplicate_child_in_meta($child_array);
            }

            // end play greg
            $html .= '<div class="saved-child-detail">
                        <p class="subtitle">Here are the details we have on file for your children.<br />
                        Please feel free to edit/add any new information.</p>';
            foreach ( $child_array as $key => $child) {
                // fill any empty keys
                if(!isset($child['gender'])) {
                    $child['gender'] = '';
                }  
                if( !isset($child['nursery'])) {
                    $child['nursery'] = '';
                }
                // build html
                $html .= '<div class="child-detail-row">
                            <div id="child-detail" data-child-id="' . $key . '">
                                <p id="child-name" data-child-id="' . $key . '" style="width: 60%">' . $child['firstname'] . ' ' . $child['lastname'] . ' - ' . $child['dob']
                                . '<br />Gender: ' . $child['gender'] . ' - Nursery: ' . $child['nursery']
                                . '<br />Notes: ' . $child['notes'] . '</p>
                                <a id="edit-child-detail" data-child-id="' . $key . '" data-my-account="true"><i class="fa fa-edit"></i>  Edit</a>
                            </div>';
                    // insert the modal form to edit a child's detail
                    $html .= show_child_edit_form_modal();
                    $html .= ' </div>';
                // we also set the window.childDetail var so it links up with how the booking.js expects the data to be handled
                $html .= '<script type="text/javascript">
                                jQuery(document).ready(function(){
                                    window.childDetail = window.childDetail || [];
                                    window.childDetail[' . $key .'] = {
                                        product_id: "",
                                        firstname: "' . $child['firstname'] . '",
                                        lastname: "' . $child['lastname'] .'" ,
                                        dob: "' . $child['dob'] .'" ,
                                        gender: "' . $child['gender'] .'" ,
                                        nursery: "' . $child['nursery'] .'" ,
                                        notes: "' . $child['notes'] . '"
                                      };
                                });
                            </script>';
            }
                $i = $key+1;
                $html .= '<div class="child-detail-row">
                            <div id="child-detail" data-child-id="'.$i.'">
                                <p id="child-name" data-child-id="'.$i.'" style="width: 60%">Click the button to add a child</p>
                                        <a id="add-child-detail" data-ajax-action="my_account_save_child_detail" data-my-account="true" data-child-id="'.$i.'" class="add-child-manually">Enter details</a>';
                // insert the modal form to add a child's detail
                $html .= show_child_edit_form_modal();
                $html.= '</div>';
                // we also set the window.childDetail var so it links up with how the booking.js expects the data to be handled
                $html .= '<script type="text/javascript">
                                jQuery(document).ready(function(){
                                    window.childDetail = window.childDetail || [];
                                    window.childDetail['.$i.'] = {
                                        product_id: "",
                                        firstname: "",
                                        lastname: "" ,
                                        dob: "" ,
                                        gender: "" ,
                                        nursery: "" ,
                                        notes: ""
                                      };
                                });
                            </script>';
                // close the div
                $html .= '</div>';
        } else {
            // no children saved yet - we show the form to add a child
            $html .= '<div class="saved-child-detail">
                        <p class="subtitle">We do not currently hold any detail for your children.<br />
                        Please feel free to add your children details to your account for faster booking later.</p>';
                        $html .= '<div class="child-detail-row">
                                    <div id="child-detail" data-child-id="0">
                                        <p id="child-name" data-child-id="0" style="width: 70%">Click the edit button to add a child</p>
                                        <a id="add-child-detail" data-ajax-action="my_account_save_child_detail" data-child-id="0" class="add-child-manually">Enter details</a>
                                    </div>';
                        // insert the modal form to edit a child's detail
                        $html .= show_child_edit_form_modal();
                        $html .= '</div>';
            // we also set the window.childDetail var so it links up with how the booking.js expects the data to be handled
            $html .= '<script type="text/javascript">
                            jQuery(document).ready(function(){
                                window.childDetail = window.childDetail || [];
                                window.childDetail[0] = {
                                    product_id: "",
                                    firstname: "",
                                    lastname: "" ,
                                    dob: "" ,
                                    gender: "" ,
                                    nursery: "" ,
                                    notes: ""
                                  };
                            });
                        </script>';
            // close the div
            $html .= '</div>';
        }

    }

    $html .= '</div>';
    echo $html;

}
 
add_action( 'woocommerce_account_child-detail_endpoint', 'gfr_show_my_account_child_detail_section' );

function save_additional_child_field() {

    global $current_user;

    // Get New User Meta
    if(isset($_POST['update'])) {
    $child_detail = $_POST['child-detail'];
    
    // Update/Create User Meta
    update_user_meta( $current_user->ID, 'child-detail', $child_detail);    
}
}

/*
 *
 * HELPER FUNCTION
 * Takes in the child_array in a format that looks like this:
 * array(3) { 
 *      [0]=> array(5) { [“firstname”]=> string(5) “James” [“lastname”]=> string(8) “Gleacher” [“dob”]=> string(10) “25/11/2016” [“product_id”]=> string(14) “3225,3235,3237” [“notes”]=> string(6) “NoneSS” }
 *      [1]=> array(5) { [“firstname”]=> string(5) “James” [“lastname”]=> string(8) “Gleacher” [“dob”]=> string(10) “25/11/2016” [“product_id”]=> string(14) “3225,3235,3237” [“notes”]=> string(4) “None” }
 *      [2]=> array(5) { [“firstname”]=> string(5) “James” [“lastname”]=> string(8) “Gleacher” [“dob”]=> string(10) “25/11/2016” [“product_id”]=> string(14) “3225,3235,3237” [“notes”]=> string(4) “None” } } 

 * It creates a unified and clean $child_array in return
 * 
 */


function gfr_cleanup_duplicate_child_in_meta($child_array) {

    if(sizeof($child_array) < 2 ) {
        // there are either no children or just 1 child so no need to clean up
        return $child_array;
    }
    $clean_child_array = array();

    for ($i=0; $i < sizeof($child_array) ; $i++) { 
        if($i === 0) {
            // this is the first child, we simply push it in
            array_push($clean_child_array, $child_array[$i]);
        } else {
            // we must now be on the second or more array element
            // first we search to see if we find this child in the clean array
            $is_child_in_clean_array_already = gfr_find_child_in_array($clean_child_array, $child_array[$i]);

            if($is_child_in_clean_array_already !== False) {
                // What could happen is that one child has notes attached and maybe not the other
                $notes = isset($clean_child_array[$is_child_in_clean_array_already]['notes']) ? $clean_child_array[$is_child_in_clean_array_already]['notes'] : '';
                $nursery = isset($clean_child_array[$is_child_in_clean_array_already]['nursery']) ? $clean_child_array[$is_child_in_clean_array_already]['nursery'] : '';
        
                $clean_notes = $notes;
                $clean_nursery = $nursery ;
                
                // test if $clean notes need to be replaced ?
                if(($clean_notes === '' || $clean_notes === 'None') && $child_array[$i]['notes'] !== '') {
                    $clean_child_array[$is_child_in_clean_array_already]['notes'] = $child_array[$i]['notes'];
                }
                // test if $clean nursery need to be replaced ?
                if($clean_nursery === '' && $child_array[$i]['nursery'] !== '') {
                    $clean_child_array[$is_child_in_clean_array_already]['nursery'] = $child_array[$i]['nursery'];
                }
            } else {
                // this is a clear second child that doesn't exist yet so we add that child to the clean array
                array_push($clean_child_array, $child_array[$i]);
            }


        }
    }
    return $clean_child_array;
}

/*
 *
 * HELPER FUNCTION
 * Takes in the child_array in a format that looks like this: and also a single array of a single child
 * array(3) { 
 *      [0]=> array(5) { [“firstname”]=> string(5) “James” [“lastname”]=> string(8) “Gleacher” [“dob”]=> string(10) “25/11/2016” [“product_id”]=> string(14) “3225,3235,3237” [“notes”]=> string(6) “NoneSS” }
 *      [1]=> array(5) { [“firstname”]=> string(5) “James” [“lastname”]=> string(8) “Gleacher” [“dob”]=> string(10) “25/11/2016” [“product_id”]=> string(14) “3225,3235,3237” [“notes”]=> string(4) “None” }
 *      [2]=> array(5) { [“firstname”]=> string(5) “James” [“lastname”]=> string(8) “Gleacher” [“dob”]=> string(10) “25/11/2016” [“product_id”]=> string(14) “3225,3235,3237” [“notes”]=> string(4) “None” } } 

 * It tries to find a matching firstname, lastname and DOB and returns true if it finds one
 * 
 */

function gfr_find_child_in_array($child_array, $child_detail) {
    $firstname_to_find = $child_detail['firstname'];
    $lastname_to_find = $child_detail['lastname'];
    $dob_to_find = $child_detail['dob'];

    for ($i=0; $i < sizeof($child_array); $i++) { 
        if ($child_array[$i]['firstname'] == $firstname_to_find && $child_array[$i]['lastname'] == $lastname_to_find && $child_array[$i]['dob'] == $dob_to_find) {
            return $i;
        }
    }

    return false;
}

/*
 *
 * AJAX Function add the child's detail to the session so it can be accessed later
 * Takes in the product_id, as well as all the detail about the children
 * It creates a SESSION object lining the child detail and the product ID (so we can later merge these things in the cart_meta)
 * 
 * returns: success or failure
 * 
 */
function gfr_my_account_save_child_detail() {

    // check that the user is logged in
    $current_user = wp_get_current_user();
    $user_id = $current_user->ID;
    if (!$user_id) {
        $data = array(
            'error'       => true,
            'message' => "Please log in"
        );
        // return success or failure to the page
        wp_send_json( $data );
        wp_die();

    }

    // Retrieve the data sent in the POST
    $child_id = (isset($_POST['child_id']) ? $_POST['child_id'] : null);
    $firstname = (isset($_POST['firstname']) ? $_POST['firstname'] : null);
    $lastname = (isset($_POST['lastname']) ? $_POST['lastname'] : null);
    $dob = (isset($_POST['dob']) ? $_POST['dob'] : null);
    $gender = (isset($_POST['gender']) ? $_POST['gender'] : null);
    $nursery = (isset($_POST['nursery']) ? $_POST['nursery'] : null);
    $notes = (isset($_POST['notes']) ? $_POST['notes'] : null);

    // Check that we have all the data
    if ($child_id === null || $firstname === null || $lastname === null || $dob === null) {
        // we're missing key data
        $data = array(
            'error'       => true,
            'message' => "Please provide all mandatory fields"
        );
    } else {
        // OK then, we save this info to the user's meta data
        $existing_child_array = gfr_get_meta( $user_id, 'child-detail' );
        $new_child_detail = array (
            'firstname' => $firstname,
            'lastname' => $lastname,
            'dob' => $dob,
            'gender' => $gender,
            'nursery' => $nursery,
            'product_id' => (int)$product_id,
            'notes' => $notes,
        );
        //now we merge the two
        $existing_child_array[$child_id] = $new_child_detail;
        //save the detail
        update_user_meta( $user_id, 'child-detail', $existing_child_array );
       
        // we send back the ok
        $data = array(
            'error'  => false,
        );

    }

    // return success or failure to the page
    wp_send_json( $data );

    wp_die();

}
// register the ajax function
add_action('wp_ajax_my_account_save_child_detail', 'gfr_my_account_save_child_detail');
add_action('wp_ajax_nopriv_my_account_save_child_detail', 'gfr_my_account_save_child_detail');



/*
 *
 * AJAX Function to save a child detail sent in the request to an order Item
 * Takes in the product_id, as well as all the detail about the children
 * It adds the child detail to the order item meta
 * 
 * returns: success or failure
 * 
 */
function gfr_my_account_admin_save_child_detail_to_order_item() {

    // TODO : verify that the user doing is is admin and has rights

    // Retrieve the data sent in the POST
    $child_id = (isset($_POST['child_id']) ? $_POST['child_id'] : null);
    $firstname = (isset($_POST['firstname']) ? $_POST['firstname'] : null);
    $lastname = (isset($_POST['lastname']) ? $_POST['lastname'] : null);
    $dob = (isset($_POST['dob']) ? $_POST['dob'] : null);
    $gender = (isset($_POST['gender']) ? $_POST['gender'] : null);
    $nursery = (isset($_POST['nursery']) ? $_POST['nursery'] : null);
    $notes = (isset($_POST['notes']) ? $_POST['notes'] : null);
    $item_id_to_edit= (isset($_POST['product_id']) ? $_POST['product_id'] : null);
    $order_id= (isset($_POST['order_id']) ? $_POST['order_id'] : null);

    // Check that we have all the data
    if ($child_id === null || $firstname === null || $lastname === null || $dob === null || $order_id === null) {
        // we're missing key data
        $data = array(
            'error'       => true,
            'message' => "Please provide all mandatory fields"
        );
        // return success or failure to the page
        wp_send_json( $data );

    } else {

        // OK we have all the data we need
        // let's fetch the order
        $order = new WC_Order($order_id);
        // parse the items, and find the one that we're adding info to
        $order_items = $order->get_items();
        foreach($order_items as $item_id => $order_item) {
            if($item_id == $item_id_to_edit) {
                // Get the product id from the item id
                $product_id = $order_item->get_product_id();
                // get the existing child (if any attached to that item)
                $child_array = wc_get_order_item_meta($item_id, 'child-detail', true) ?  wc_get_order_item_meta($item_id, 'child-detail', true) : array();
                // then add the child detail being sent 
                $child_array[$child_id] = array(
                    'firstname' => $firstname,
                    'lastname' => $lastname,
                    'dob' => $dob,
                    'gender' => $gender,
                    'nursery' => $nursery,
                    'product_id' => (int)$product_id,
                    'notes' => $notes,
                );
                // finally save the whole thing
                $successful_save = wc_update_order_item_meta( $item_id, 'child-detail', $child_array );

                // we also add this child detail to the parent's user_meta
                gfr_save_child_detail_to_profile( $order_id );

                if(!$successful_save) {
                    $data = array(
                        'error'       => true,
                        'message' => "We couldn't update the data in the Database"
                    );
                    // return success or failure to the page
                    wp_send_json( $data );
                }
            }

        }
        

        // we send back the ok
        $data = array(
            'error'  => false,
        );

    }

    // return success or failure to the page
    wp_send_json( $data );

    wp_die();
    
}

// register the ajax function to save a child detail to an order being created in the Admin
// the two below are when and
// admin uses the select dropdown and clicks "Select this child for class"
add_action('wp_ajax_admin-select-child-for-order-item', 'gfr_my_account_admin_save_child_detail_to_order_item');
// note : nothing happens if the user is logged out

// these two are when an admin enters brand new details using the modal window and then clicks ok
add_action('wp_ajax_admin-add-child-for-order-item', 'gfr_my_account_admin_save_child_detail_to_order_item');
// note : nothing happens if the user is logged out


?>