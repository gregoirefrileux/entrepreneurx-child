// Function to send the ajax request when select options are changed
(function ($, window, document, undefined) {
  var $win = $(window);

  $.fn.spin.presets.booked = {
    lines: 10, // The number of lines to draw
    length: 7, // The length of each line
    width: 5, // The line thickness
    radius: 11, // The radius of the inner circle
    corners: 1, // Corner roundness (0..1)
    rotate: 0, // The rotation offset
    direction: 1, // 1: clockwise, -1: counterclockwise
    color: "#555", // #rgb or #rrggbb or array of colors
    speed: 1, // Rounds per second
    trail: 60, // Afterglow percentage
    shadow: false, // Whether to render a shadow
    hwaccel: false, // Whether to use hardware acceleration
    className: "booked-spinner", // The CSS class to assign to the spinner
    // zIndex: 2e9, // The z-index (defaults to 2000000000)
    top: "50%", // Top position relative to parent
    left: "50%", // Left position relative to parent
  };

  // Function to call the backend and reload the week view when there is a change in the Location Select
  $("select[name='location']").change(function () {
    var gotoWeek = $("span.weekName").attr("data-date");

    // get some values
    var className = $("select[name='class-name']").val();
    var age_group = $("select[name='age-group']").val();

    // remove the current content and show a spinner
    $("#bookedTimeslotsWrap").css("opacity", "0.2");
    $("#bookedTimeslotsWrap").spin("booked");

    // run ajax
    $.ajax({
      url: "/wp-admin/admin-ajax.php",
      type: "post",
      data: {
        action: "load_classes_for_week_with_params",
        location: this.value,
        className,
        age_group,
        gotoWeek,
      },
      success: function (response) {
        $("#bookedTimeslotsWrap").html(response);
        $("#bookedTimeslotsWrap").css("opacity", "1");
        // if on mobile we also make sure to go back to the correct day
        if (!is_desktop()) {
          showDayOnMobile();
        }
      },
      failure: function (response) {
        alert("There was an error getting classes from the server");
        $("#bookedTimeslotsWrap").css("opacity", "1");
      },
    });
  });

  // Function to call the backend and reload the week view when there is a change in the Class Name Select
  $("select[name='class-name']").change(function () {
    var gotoWeek = $("span.weekName").attr("data-date");

    // get some values
    var location = $("select[name='location']").val();
    var age_group = $("select[name='age-group']").val();

    // remove the current content and show a spinner
    $("#bookedTimeslotsWrap").css("opacity", "0.2");
    $("#bookedTimeslotsWrap").spin("booked");

    // run ajax
    $.ajax({
      url: "/wp-admin/admin-ajax.php",
      type: "post",
      data: {
        action: "load_classes_for_week_with_params",
        className: this.value,
        location,
        age_group,
        gotoWeek,
      },
      success: function (response) {
        $("#bookedTimeslotsWrap").html(response);
        $("#bookedTimeslotsWrap").css("opacity", "1");
        // if on mobile we also make sure to go back to the correct day
        if (!is_desktop()) {
          showDayOnMobile();
        }
      },
      failure: function (response) {
        alert("There was an error getting classes from the server");
        $("#bookedTimeslotsWrap").css("opacity", "1");
      },
    });
  });

  // Function to call the backend and reload the week view when the Prev or Next week is clicked
  $("#bookedTimeslotsWrap").on(
    "click",
    ".page-right, .page-left, .monthName a",
    function (e) {
      // prevent nav
      e.preventDefault();

      // get the week to go to
      var $button = $(this),
        gotoWeek = $button.attr("data-goto");

      // get some values
      var location = $("select[name='location']").val();
      var className = $("select[name='class-name']").val();
      var age_group = $("select[name='age-group']").val();

      // remove the current content and show a spinner
      $("#bookedTimeslotsWrap").css("opacity", "0.2");
      $("#bookedTimeslotsWrap").spin("booked");

      // run ajax
      $.ajax({
        url: "/wp-admin/admin-ajax.php",
        type: "post",
        data: {
          action: "load_classes_for_week_with_params",
          location,
          className,
          gotoWeek,
          age_group,
        },
        success: function (response) {
          $("#bookedTimeslotsWrap").html(response);
          $("#bookedTimeslotsWrap").css("opacity", "1");
        },
        failure: function (response) {
          alert("There was an error getting classes from the server");
          $("#bookedTimeslotsWrap").css("opacity", "1");
        },
      });
    }
  );

  // Function to call the backend and reload the week view when the "Navigate to Week" dropdown is clicked
  $("select[name='upcoming-week']").change(function () {
    var gotoWeek = this.value;

    // get some values
    var location = $("select[name='location']").val();
    var className = $("select[name='class-name']").val();
    var age_group = $("select[name='age-group']").val();

    // remove the current content and show a spinner
    $("#bookedTimeslotsWrap").css("opacity", "0.2");
    $("#bookedTimeslotsWrap").spin("booked");

    // run ajax
    $.ajax({
      url: "/wp-admin/admin-ajax.php",
      type: "post",
      data: {
        action: "load_classes_for_week_with_params",
        location,
        className,
        gotoWeek,
        age_group,
      },
      success: function (response) {
        $("#bookedTimeslotsWrap").html(response);
        $("#bookedTimeslotsWrap").css("opacity", "1");
      },
      failure: function (response) {
        alert("There was an error getting classes from the server");
        $("#bookedTimeslotsWrap").css("opacity", "1");
      },
    });
  });

  showJoinWaitlistModal = function (e) {
    e.preventDefault();

    var htmlToDisplay = $("#html_for_modal_join_waitlist").html();

    var product_id = $(this).data("product-id");

    create_booked_modal();
    setTimeout(function () {
      $(".bm-window").html(
        '<div id="html_for_modal">' + htmlToDisplay + "</div>"
      );

      $(".bm-window .product-id").val(product_id);

      var bookedModal = $(".booked-modal");
      var bmWindow = bookedModal.find(".bm-window");
      bmWindow.css({ visibility: "hidden" });
      bookedModal.removeClass("bm-loading");
      $(document).trigger("booked-on-new-app");
      resize_booked_modal();
      bmWindow.hide();
      $(".booked-modal .bm-overlay").find(".booked-spinner").remove();
      setTimeout(function () {
        bmWindow.css({ visibility: "visible" });
        bmWindow.show();
        $(".bm-window #html_for_modal").css("display", "block");
      }, 50);
    }, 300);

    return false;
  };

  $(document).on("click", "button.join-waitlist", showJoinWaitlistModal);

  // Function to display the class options whenever a click on "Show options" is triggered
  // inside the Term Week view
  // New Appointment Click
  showClassOptions = function (e) {
    e.preventDefault();

    var $button = $(this),
      htmlToDisplay = $button.find("#html_for_modal").html();

    create_booked_modal();
    setTimeout(function () {
      $(".bm-window").html(
        '<div id="html_for_modal">' + htmlToDisplay + "</div>"
      );

      var bookedModal = $(".booked-modal");
      var bmWindow = bookedModal.find(".bm-window");
      bmWindow.css({ visibility: "hidden" });
      bookedModal.removeClass("bm-loading");
      $(document).trigger("booked-on-new-app");
      resize_booked_modal();
      bmWindow.hide();
      $(".booked-modal .bm-overlay").find(".booked-spinner").remove();
      setTimeout(function () {
        bmWindow.css({ visibility: "visible" });
        bmWindow.show();
        $(".bm-window #html_for_modal").css("display", "block");
      }, 50);
    }, 300);

    return false;
  };

  // Function to display the class options whenever a click on "Show options" is triggered
  // inside the Term Week view
  // New Appointment Click
  showChildTooYoungModal = function (e, min_age_limit) {
    e.preventDefault();

    var htmlToDisplay = $("#html_for_modal_child_too_young").html();

    create_booked_modal();
    setTimeout(function () {
      $(".bm-window").html(
        '<div id="html_for_modal">' + htmlToDisplay + "</div>"
      );

      $(".bm-window .min-age-span").text(min_age_limit);

      var bookedModal = $(".booked-modal");
      var bmWindow = bookedModal.find(".bm-window");
      bmWindow.css({ visibility: "hidden" });
      bookedModal.removeClass("bm-loading");
      $(document).trigger("booked-on-new-app");
      resize_booked_modal();
      bmWindow.hide();
      $(".booked-modal .bm-overlay").find(".booked-spinner").remove();
      setTimeout(function () {
        bmWindow.css({ visibility: "visible" });
        bmWindow.show();
        $(".bm-window #html_for_modal").css("display", "block");
      }, 50);
    }, 300);

    return false;
  };

  $(document).on("click", ".button.new-appt", showClassOptions);

  // Function to send custom to "Add child detail" page after having chosen a class in the calendar view
  $(document).on("click", "a#add_term_to_cart", function (e) {
    e.preventDefault(); // Prevent the click from going to the link

    // show that something is happening
    show_loader();

    var $href = $(this),
      product_id = $href.attr("data-product-id"),
      number_of_spots = $href
        .parent()
        .parent()
        .parent()
        .find("input#single-class-quantity")
        .val();

    $href.hide();

    // when adding multiple classes, we sometimes have an initial comma in the string - we remove it
    product_id = product_id.replace(/^,/, "");

    window.location.href =
      "/add-child-to-class/?child_detail=" +
      number_of_spots +
      "&product_id=" +
      product_id;
  });

  //Function to modify the Total Price when the qty input is changed
  // This is used in the Calendar View > select date > click "book class" in the list of class
  $(document).on("change", "input#single-class-quantity", function (e) {
    // Get the product price
    var $input = $(this),
      qty = $input.val(),
      product_price = $input
        .parent()
        .parent()
        .find("#product-price")
        .attr("data-product-price"),
      total_column = $input.parent().parent().find("#total-price");

    // now we set the Total Price column with the new total price
    var total_price = product_price * qty;
    // round to two decimals if need be
    total_price = Math.round(total_price * 100) / 100;
    // we insert the price in the Total Column
    total_column.html("£" + total_price);
    return true;
  });

  //Function to modify the Unit Price and class Qty in the input area when someone books multiple dates
  // This is used in the Calendar View > select date > click "book multiple dates" in the list of class
  $(document).on("change", "input#remaining-class-checkbox", function (e) {
    // Get the product price from the checked (or unchecked) input field
    var $input = $(this),
      checked = $input.is(":checked"),
      product_price = parseFloat($input.attr("data-product-price")),
      product_count = parseInt($input.attr("data-product-count")),
      product_id = $input.attr("data-product-id"),
      total_input = $input.parent().find("#remaining-classes-total");

    // now we amend the hidden input to add the class id and price to the total
    var previous_price =
      parseFloat(total_input.attr("data-product-price")) || 0;
    var previous_count = parseInt(total_input.attr("data-product-count")) || 0;
    if (checked) {
      total_input.attr("data-product-price", previous_price + product_price);
      // add the product id to the list - we simply add ",123" at the end
      total_input.attr(
        "data-product-id",
        total_input.attr("data-product-id") + "," + product_id
      );
      // update the count
      total_input.attr("data-product-count", previous_count + 1);
    } else {
      // we just unchecked the box - remove the price and id
      total_input.attr("data-product-price", previous_price - product_price);
      // remove the product id to the list
      total_input.attr(
        "data-product-id",
        total_input.attr("data-product-id").replace("," + product_id, "")
      );
      // update the count
      total_input.attr("data-product-count", previous_count - 1);
    }

    // we reflect these changes in the total column
    // called with the new total price, quantity and product id list
    reflect_choice_changes_qty_input(
      $input,
      parseFloat(total_input.attr("data-product-price")),
      parseInt(total_input.attr("data-product-count")),
      total_input.attr("data-product-id")
    );

    return true;
  });

  //Function to modify the Unit Price and class Qty in the input area when someone books a HOLE TERM
  // This is used in the Calendar View > select class > click "book multiple dates" in the list of class
  $(document).on("change", "input#term-class-checkbox", function (e) {
    // Get the product price from the checked (or unchecked) input field
    var $input = $(this),
      checked = $input.is(":checked"),
      product_price = parseFloat($input.attr("data-product-price")),
      product_id = $input.attr("data-product-id"),
      total_input = $input.parent().parent().find("#remaining-classes-total"),
      other_inputs_single_class = $input
        .parent()
        .parent()
        .find("#remaining-class-checkbox");

    // First we uncheck all the single class inputs to avoid bokoing multiple stuff at the same time
    other_inputs_single_class.each(function () {
      this.checked = false;
    });

    // now we overwrite hidden input to add the term class id and price to the total
    if (checked) {
      total_input.attr("data-product-price", product_price);
      // add the product id to the list - we simply add ",123" at the end
      total_input.attr("data-product-id", product_id);
      // update the count
      total_input.attr("data-product-count", 1);
    } else {
      // we just unchecked the box - remove the price and id
      total_input.attr("data-product-price", 0);
      // remove the product id to the list
      total_input.attr("data-product-id", "");
      // update the count
      total_input.attr("data-product-count", 0);
    }

    // we reflect these changes in the total column
    // called with the new total price, quantity and product id list
    reflect_choice_changes_qty_input(
      $input,
      parseFloat(total_input.attr("data-product-price")),
      parseInt(total_input.attr("data-product-count")),
      total_input.attr("data-product-id")
    );

    return true;
  });

  // Function to open a modal box and add a child's detail
  // Used on the "Add child to class" page
  showChildForm = function (e) {
    e.preventDefault();

    var $link = $(this),
      child_id = $link.attr("data-child-id"),
      product_id = $link.attr("data-product-id"),
      ajax_action = $link.attr("data-ajax-action"),
      my_account = $link.attr("data-my-account") || false,
      htmlToDisplay = $link
        .closest(".child-detail-row")
        .parent()
        .find("#html_for_modal")
        .html();

    create_booked_modal();
    setTimeout(function () {
      $(".bm-window").html(
        '<div id="html_for_modal">' + htmlToDisplay + "</div>"
      );

      var bookedModal = $(".booked-modal");
      var bmWindow = bookedModal.find(".bm-window");
      var doneButton = bookedModal.find("button.add-child-to-class");
      bmWindow.css({ visibility: "hidden" });
      bookedModal.removeClass("bm-loading");
      $(document).trigger("booked-on-new-app");
      resize_booked_modal();
      bmWindow.hide();
      $(".booked-modal .bm-overlay").find(".booked-spinner").remove();
      setTimeout(function () {
        doneButton.attr("data-edit", "false");
        doneButton.attr("data-child-id", child_id);
        doneButton.attr("data-product-id", product_id);
        doneButton.attr("data-ajax-action", ajax_action);
        if (my_account) {
          doneButton.attr("data-my-account", my_account);
        }
        bmWindow.css({ visibility: "visible" });
        bmWindow.show();
        $(".bm-window #html_for_modal").css("display", "block");
      }, 50);
    }, 300);

    return false;
  };

  $(document).on("touchstart, click", "a#add-child-detail", showChildForm);

  // Function to open a modal box and EDIT a child's detail
  // Used on the "Edit child to class" page
  showChildFormWithInfo = function (e) {
    e.preventDefault();

    var $link = $(this),
      child_id = $link.attr("data-child-id"),
      product_id = $link.attr("data-product-id"),
      ajax_action = $link.attr("data-ajax-action"),
      my_account = $link.attr("data-my-account") || false,
      htmlToDisplay = $link
        .closest(".child-detail-row")
        .parent()
        .find("#html_for_modal")
        .html();

    // retrieve the existing data
    if (window.childDetail && window.childDetail[child_id]) {
      child_to_edit = window.childDetail[child_id];
    } else {
      // there's no child to edit ? weird bug
      return false;
    }

    create_booked_modal();
    setTimeout(function () {
      $(".bm-window").html(
        '<div id="html_for_modal">' + htmlToDisplay + "</div>"
      );

      var bookedModal = $(".booked-modal");
      var bmWindow = bookedModal.find(".bm-window");
      var doneButton = bookedModal.find("button.add-child-to-class");
      bmWindow.css({ visibility: "hidden" });
      bookedModal.removeClass("bm-loading");
      $(document).trigger("booked-on-new-app");
      resize_booked_modal();
      bmWindow.hide();
      $(".booked-modal .bm-overlay").find(".booked-spinner").remove();
      setTimeout(function () {
        // we fill in the form with the existing data
        form = bookedModal.find("form.frm-show-form");
        form.find("input#firstname").val(child_to_edit.firstname || "");
        form.find("input#lastname").val(child_to_edit.lastname || "");
        form.find("input#dob").val(child_to_edit.dob || "");
        form.find("select#gender").val(child_to_edit.gender || "male");
        form.find("input#nursery").val(child_to_edit.nursery || "");
        form.find("textarea#notes").val(child_to_edit.notes || "");
        // update the modal so it relates to the correct child
        doneButton.attr("data-edit", "true");
        doneButton.attr("data-child-id", child_id);
        doneButton.attr("data-product-id", product_id);
        if (my_account) {
          doneButton.attr("data-my-account", my_account);
        }
        doneButton.attr("data-ajax-action", ajax_action);
        bmWindow.css({ visibility: "visible" });
        bmWindow.show();
        $(".bm-window #html_for_modal").css("display", "block");
      }, 50);
    }, 300);

    return false;
  };

  $(document).on("click", "a#edit-child-detail", showChildFormWithInfo);

  // Function to close the modal and save the child's detail on the "Add child to class" page
  $(document).on("click", "button.add-child-to-class", function (e) {
    e.preventDefault();

    // show that something is happening
    show_loader();

    var button = $(this),
      edit_mode = button.attr("data-edit"),
      child_id = button.attr("data-child-id"),
      my_account = button.attr("data-my-account") || false,
      product_id = button.attr("data-product-id"),
      ajax_action = button.attr("data-ajax-action");

    // hide the button to avoid multiple clicks
    button.css("display", "none");

    //get the modal - useful later
    var bookedModal = $(".booked-modal");

    // get the information entered in the form
    firstname = bookedModal.find("input#firstname").val();
    lastname = bookedModal.find("input#lastname").val();
    dob = bookedModal.find("input#dob").val();
    gender = bookedModal.find("select#gender").val();
    nursery = bookedModal.find("input#nursery").val();
    notes = bookedModal.find("textarea#notes").val();

    // very basic validation
    if (
      firstname === "" ||
      lastname == "" ||
      dob === "" ||
      gender === "" ||
      nursery === "" ||
      notes === ""
    ) {
      // remove any previous error div
      bookedModal.find("div.frm_error_style").remove();
      // show this error
      bookedModal
        .find("div.frm_forms.with_frm_style.frm_style_formidable-style")
        .prepend(
          '<div class="frm_error_style">Please enter all mandatory fields.</div>'
        );
      button.css("display", "initial");
      $("p.ajax-status").hide();
      return false;
    } else {
      // no errors, we hide any previously displayed errors
      bookedModal.find("div.frm_error_style").remove();
    }

    // post data to backend
    $.ajax({
      url: "/wp-admin/admin-ajax.php",
      method: "post",
      data: {
        action:
          my_account === "true" ? "my_account_save_child_detail" : ajax_action,
        product_id,
        child_id,
        firstname,
        lastname,
        dob,
        gender,
        nursery,
        notes,
      },
      success: function (response) {
        if (response.error != "undefined" && response.error) {
          // display the error
          // TODO : get the error message from the request
          bookedModal
            .find("div.frm_forms.with_frm_style.frm_style_formidable-style")
            .prepend(
              '<div class="frm_error_style">An error occured, we could not save this information.</div>'
            );
          // scroll to the error - especially useful on mobile
          scroll_to_error("div.booked-scrollable");
          //show the save button
          button.css("display", "initial");
          // hide loader
          hide_loader();
        } else {
          // update the main page with the child's detail
          if (!my_account || my_account === "false") {
            $(
              "p#child-name[data-child-id='" +
                child_id +
                "'][data-product-id='" +
                product_id +
                "']"
            ).html(firstname + " " + lastname + " - " + dob);
          } else {
            // on my account we display the full data
            $("p#child-name[data-child-id='" + child_id + "']").html(
              firstname +
                " " +
                lastname +
                " - " +
                dob +
                "<br />Gender: " +
                gender +
                " - Nursery: " +
                nursery +
                "<br />Notes : " +
                notes
            );
            // we also add a new row for adding a new child if the user was "adding" (not editing)
            if (!edit_mode) {
              add_new_child_row(child_id);
            }
          }
          // save the child info in a global var
          window.childDetail = window.childDetail || [];
          window.childDetail[child_id] = {
            product_id,
            firstname,
            lastname,
            dob,
            gender,
            nursery,
            notes,
          };
          if (edit_mode === "false") {
            // we're not in edit mode, so we need to change things around a bit

            // Change the link from "add child" to "edit"
            $(
              "a#add-child-detail[data-child-id='" +
                child_id +
                "'][data-product-id='" +
                product_id +
                "']"
            ).hide();
            $(
              "a#edit-child-detail[data-child-id='" +
                child_id +
                "'][data-product-id='" +
                product_id +
                "']"
            ).show();
            $(
              "a#remove-child-detail[data-child-id='" +
                child_id +
                "'][data-product-id='" +
                product_id +
                "']"
            ).show();
            // increment the hidden input that tracks how many children have been "entered"
            $("input#child_detail_entered").val(
              parseInt($("input#child_detail_entered").val()) + 1
            );
            // check if we can activate the add to cart button
            check_activate_add_to_cart();
          }
          // remove the loader
          hide_loader();
          // close the modal
          close_booked_modal();
        }
      },
      failure: function (response) {
        bookedModal
          .find("div.frm_forms.with_frm_style.frm_style_formidable-style")
          .prepend(
            '<div class="frm_error_style">An error occured, we could not save this information.</div>'
          );
        // remove the loader
        hide_loader();
        //show the save button
        button.show();
      },
    });
  });

  // Function to select and save in backend a child detail from the Select Dropdown on Add Child to Class page
  // when a parent is using previously saved information
  $(document).on("click", "a#select-child-detail", function (e) {
    e.preventDefault();

    // show that something is happening
    show_loader();

    var button = $(this),
      child_id = button.attr("data-child-id"),
      product_id = button.attr("data-product-id"),
      action = button.attr("data-ajax-action"),
      order_id = button.attr("data-order-id");

    // hide the button to avoid multiple clicks
    button.css("display", "none");

    // get the information entered in the form
    // based on which child was selected in the dropdown, we get the kid's info
    kidChosenId = $(
      '#select_saved_child_info[data-child-id="' + child_id + '"]'
    ).val();
    kidChosen = savedChildDetail[parseInt(kidChosenId)];
    // Did this yield any result (it should but always safe to check)
    if (!kidChosen) {
      // some error happened
      $('#select-child-error[data-child-id="' + child_id + '"]').show();
      return false;
    }
    firstname = kidChosen.firstname;
    lastname = kidChosen.lastname || kidChosen.lastName;
    dob = kidChosen.dob;
    gender = kidChosen.gender;
    nursery = kidChosen.nursery;
    notes = kidChosen.notes;

    // very basic validation
    if (firstname === "" || lastname == "" || dob === "") {
      // TODO show the "add kid detail button"
      $('#select-child-error[data-child-id="' + child_id + '"]').show();
      return false;
    } else {
      // no errors, we hide any previously displayed errors
      $('#select-child-error[data-child-id="' + child_id + '"]').hide();
    }

    // post data to backend
    $.ajax({
      url: "/wp-admin/admin-ajax.php",
      method: "post",
      data: {
        action,
        product_id,
        child_id,
        firstname,
        lastname,
        dob,
        gender,
        nursery,
        notes,
        order_id,
      },
      success: function (response) {
        if (response.error != "undefined" && response.error) {
          // display the error
          // TODO : get the error message from the request
          $('#select-child-error[data-child-id="' + child_id + '"]').show();
          //show the save button
          button.css("display", "initial");
        } else {
          // update the main page with the child's detail
          $(
            "p#child-name[data-child-id='" +
              child_id +
              "'][data-product-id='" +
              product_id +
              "']"
          ).html(firstname + " " + lastname + " - " + dob);
          // save the child info in a global var
          window.childDetail = window.childDetail || [];
          window.childDetail[child_id] = {
            product_id,
            firstname,
            lastname,
            dob,
            gender,
            nursery,
            notes,
          };

          // Change the link from "add child" to "edit"
          $(
            "a#select-child-detail[data-child-id='" +
              child_id +
              "'][data-product-id='" +
              product_id +
              "']"
          ).hide();
          $(
            "a#remove-child-detail[data-child-id='" +
              child_id +
              "'][data-product-id='" +
              product_id +
              "']"
          ).show();
          // increment the hidden input that tracks how many children have been "entered"
          $("input#child_detail_entered").val(
            parseInt($("input#child_detail_entered").val()) + 1
          );
          // check if we can activate the add to cart button
          check_activate_add_to_cart();

          // remove the loader
          hide_loader();
        }
      },
      failure: function (response) {
        bookedModal
          .find("div.frm_forms.with_frm_style.frm_style_formidable-style")
          .prepend(
            '<div class="frm_error_style">An error occured, we could not save this information.</div>'
          );
        // remove the loader
        hide_loader();
        //show the save button
        button.show();
      },
    });
  });

  // Function to reset the dropdown, remove the child choice when a parent decides to click "remove from class"
  $(document).on("click", "a#remove-child-detail", function (e) {
    e.preventDefault();

    var button = $(this),
      product_id = button.attr("data-product-id"),
      child_id = button.attr("data-child-id");

    // get the dropdown and reset it
    $("#select_saved_child_info[data-child-id='" + child_id + "']")
      .find("option:first")
      .attr("selected", "selected");

    // reset the value of the "Child 1" or whatever number we're on
    $("p#child-name[data-child-id='" + child_id + "']").html(
      "Child " + (parseInt(child_id) + 1)
    );
    // Change the link from "add child" to "edit"
    $(
      "a#select-child-detail[data-child-id='" +
        child_id +
        "'][data-product-id='" +
        product_id +
        "']"
    ).show();
    $(
      "a#add-child-detail[data-child-id='" +
        child_id +
        "'][data-product-id='" +
        product_id +
        "']"
    ).show();
    $(
      "a#remove-child-detail[data-child-id='" +
        child_id +
        "'][data-product-id='" +
        product_id +
        "']"
    ).hide();
    // decrement the hidden input that tracks how many children have been "entered"
    $("input#child_detail_entered").val(
      parseInt($("input#child_detail_entered").val()) - 1
    );
    // check if we can activate the add to cart button
    check_activate_add_to_cart();
  });

  maybePreventAddChild = function (e) {
    var no_of_children = $(".child-detail-row").size();
    var min_age_limit = $("#min_age_limit").val();
    var min_dob = moment().subtract(min_age_limit, "years");

    for (var i = 0; i < no_of_children; i++) {
      $elem = $("div[data-child-id='" + i.toString() + "'] p");
      var child_details_string = $elem.first().text();
      var child_age_str = child_details_string.substring(
        child_details_string.indexOf("- ") + 2
      ); //"01/10/2019"
      var child_age = moment(child_age_str, "DD/MM/YYYY");

      var child_old_enough = min_dob.isSameOrAfter(child_age);

      if (!child_old_enough) {
        showChildTooYoungModal(e, min_age_limit);
        return true;
      }

      return false;
    }
  };

  // Function to add a class to cart when on the "Add child to class"
  addChildAndClassCart = function (e) {
    e.preventDefault();

    if (maybePreventAddChild(e)) return;

    var button = $(this),
      quantity = button.attr("data-validate-button"),
      href = button.attr("data-href"),
      product_id = button.attr("data-product-id");

    // hide previous errors
    button.parent().find("div.alert").remove();

    // If the client hasn't entered all the kids detail, we return false
    if (button.hasClass("inactive")) {
      // the form should be rejected
      button
        .parent()
        .prepend(
          '<div class="alert alert-warning">Please enter the detail for all children.</div>'
        );
      return false;
    } else {
      // the button is active, let's remove any previous errors
      button.parent().find("div.frm_error_style").remove();
    }

    // show that something is happening
    show_loader();

    // hide both "Add&Checkout" and "Add and back" buttons to avoid multiple clicks
    $("button#add-to-cart-btn").hide();
    $("button#add-to-cart-and-go-back-btn").hide();

    // post data to backend
    $.ajax({
      url: "/wp-admin/admin-ajax.php",
      method: "post",
      data: {
        action: "add_class_to_cart_with_detail",
        product_id: product_id,
        quantity: quantity,
      },
      success: function (response) {
        if (response.error != "undefined" && response.error) {
          // display the error
          // TODO : get the error message from the request
          button
            .parent()
            .prepend(
              '<div class="alert alert-danger">An error occured. ' +
                response.message +
                "</div>"
            );
          // re-show the button
          $("button#add-to-cart-btn").show();
          $("button#add-to-cart-and-go-back-btn").show();
          // hide the loader
          hide_loader();
        } else {
          // display something before we navigate away?
          window.location.href = href;
        }
      },
      failure: function (response) {
        button
          .parent()
          .prepend(
            '<div class="alert alert-danger">An error occured, we could not save this information.</div>'
          );
        // re-show the button
        $("button#add-to-cart-btn").show();
        $("button#add-to-cart-and-go-back-btn").show();
        // hide the loader
        hide_loader();
      },
    });
  };
  $(document).on("click", "button#add-to-cart-btn", addChildAndClassCart);
  $(document).on(
    "click",
    "button#add-to-cart-and-go-back-btn",
    addChildAndClassCart
  );

  // Function to swap out the days of the Week for MOBILE when a day is changed
  showDayOnMobile = function () {
    // remove the current content and show a spinner
    $("#bookedTimeslotsWrap").css("opacity", "0.2");
    $("#bookedTimeslotsWrap").spin("booked");
    var selected_day = this.value || $("select[name='weekday']")[0].value;
    setTimeout(function () {
      //we hide all the days
      $("td[id^=column-]").each(function () {
        if (this.id === selected_day) {
          $(this).removeClass().addClass("show-mobile-only");
        } else {
          $(this).removeClass().addClass("hide-mobile-only");
        }
      });
      $("#bookedTimeslotsWrap").css("opacity", "1");
      $("#bookedTimeslotsWrap .booked-spinner").remove();
    }, 250);
  };
  $(document).on("change", "select[name='weekday']", showDayOnMobile);

  // Close the modal on click on "close" or body
  $("body").on(
    "click",
    ".bm-overlay, .bm-window .close, .booked-form .cancel",
    function (e) {
      e.preventDefault();
      close_booked_modal();
      return false;
    }
  );

  //
  // Now for some functions linked to the single class function
  //

  $win.on("resize", function () {
    adjust_calendar_boxes();
    // resize_booked_modal();
  });

  $win.on("load", function () {
    // Adjust the calendar sizing on load
    adjust_calendar_boxes();

    $(".booked-calendar-wrap").each(function () {
      var thisCalendar = $(this);
      var calendar_month = thisCalendar
        .find("table.booked-calendar")
        .attr("data-calendar-date");
      thisCalendar.attr("data-default", calendar_month);
      init_tooltips(thisCalendar);
    });

    // Function to reload the calendar on switcher select change
    $(".booked_calendar_chooser").change(function (e) {
      e.preventDefault();

      var $selector = $(this),
        thisIsCalendar = $selector
          .parents(".booked-calendarSwitcher")
          .hasClass("calendar");

      if (!thisIsCalendar) {
        var thisCalendarWrap = $selector
            .parents(".booked-calendar-shortcode-wrap")
            .find(".booked-list-view"),
          thisDefaultDate = thisCalendarWrap.attr("data-default"),
          thisIsCalendar = $selector
            .parents(".booked-calendarSwitcher")
            .hasClass("calendar");

        if (typeof thisDefaultDate == "undefined") {
          thisDefaultDate = false;
        }

        thisCalendarWrap.addClass("booked-loading");

        var args = {
          action: "load_single_month_with_params",
          location: this.value,
          date: thisDefaultDate,
        };

        $(document).trigger(
          "booked-before-loading-appointment-list-booking-options"
        );
        thisCalendarWrap.spin("booked_top");

        $.ajax({
          url: "/wp-admin/admin-ajax.php",
          type: "post",
          data: args,
          success: function (html) {
            thisCalendarWrap.html(html);

            init_appt_list_date_picker();
            setTimeout(function () {
              thisCalendarWrap.removeClass("booked-loading");
            }, 1);
          },
        });
      } else {
        var thisCalendarWrap = $selector
            .parents(".booked-calendar-shortcode-wrap")
            .find(".booked-calendar-wrap"),
          thisDefaultDate = thisCalendarWrap.attr("data-default");
        if (typeof thisDefaultDate == "undefined") {
          thisDefaultDate = false;
        }

        var args = {
          action: "load_single_month_with_params",
          location: this.value,
          date: thisDefaultDate,
        };

        savingState(true, thisCalendarWrap);

        $.ajax({
          url: "/wp-admin/admin-ajax.php",
          type: "post",
          data: args,
          success: function (html) {
            thisCalendarWrap.html(html);

            adjust_calendar_boxes();
            bookedRemoveEmptyTRs();
            init_tooltips(thisCalendarWrap);
            $(window).trigger("booked-load-calendar", args, $selector);
          },
        });
      }

      return false;
    });

    // Calendar Next/Prev Click
    $(".booked-calendar-wrap").on(
      "click",
      ".page-right, .page-left, .monthName a",
      function (e) {
        e.preventDefault();
        // if we're on a wide screen, we'll need to clear displayed classes in the right hand side space
        var isDesktop = is_desktop();
        if (isDesktop) {
          $("#desktop_class_display").html("");
        }

        var $button = $(this),
          gotoMonth = $button.attr("data-goto"),
          thisCalendarWrap = $button.parents(".booked-calendar-wrap");

        var args = {
          action: "load_single_month_with_params",
          location: $("#booked_calendar_chooser_1").val(),
          gotoMonth: gotoMonth,
        };

        savingState(true, thisCalendarWrap);

        $.ajax({
          url: "/wp-admin/admin-ajax.php",
          type: "post",
          data: args,
          success: function (html) {
            thisCalendarWrap.html(html);

            adjust_calendar_boxes();
            bookedRemoveEmptyTRs();
            init_tooltips(thisCalendarWrap);
            $(window).trigger("booked-load-calendar", args, $button);
          },
        });

        return false;
      }
    );

    // Single Calendar Date Click - show classes available
    $(".booked-calendar-wrap").on("click", "tr.week td", function (e) {
      e.preventDefault();

      var isDesktop = is_desktop();

      var $thisDate = $(this),
        booked_calendar_table = $thisDate.parents("table.booked-calendar"),
        $thisRow = $thisDate.parent(),
        // date = $thisDate.attr("data-date"),
        // location = booked_calendar_table.attr("data-location"),
        // term = booked_calendar_table.attr("data-term"),
        colspanSetting = $thisRow.find("td").length,
        htmlToDisplay = $thisDate.find("#day_detail").html();

      if (
        $thisDate.hasClass("blur") ||
        ($thisDate.hasClass("booked") && !booked_js_vars.publicAppointments) ||
        $thisDate.hasClass("prev-date")
      ) {
        // Do nothing.
      } else if ($thisDate.hasClass("active")) {
        // if (!isDesktop) {
        $thisDate.removeClass("active");
        $("tr.entryBlock").remove();

        var calendarHeight = booked_calendar_table.height();
        booked_calendar_table.parent().height(calendarHeight);
        // }
      } else {
        $("tr.week td").removeClass("active");
        $thisDate.addClass("active");
        if (!isDesktop) {
          $("tr.entryBlock").remove();
          $thisRow.after(
            '<tr class="entryBlock booked-loading"><td colspan="' +
              colspanSetting +
              '"></td></tr>'
          );
          $("tr.entryBlock").find("td").spin("booked");
        } else {
          $("#desktop_class_display").animate({ opacity: 0.2 });
          $("#desktop_class_display").spin("booked");
        }

        // booked_load_calendar_date_booking_options = {
        //   action: "gfr_classes_for_calendar_date",
        //   date: date,
        //   location: location,
        //   term: term
        // };
        $(document).trigger("booked-before-loading-calendar-booking-options");

        var calendarHeight = booked_calendar_table.height();
        booked_calendar_table.parent().height(calendarHeight);
        if (isDesktop) {
          setTimeout(function () {
            $("#desktop_class_display").html(htmlToDisplay);
            $("#desktop_class_display").animate({ opacity: 1 });

            $("tr.entryBlock").removeClass("booked-loading");
            $("tr.entryBlock").find(".booked-appt-list").fadeIn(300);
            $("tr.entryBlock").find(".booked-appt-list").addClass("shown");
            adjust_calendar_boxes();
          }, 500);
        } else {
          setTimeout(function () {
            $("tr.entryBlock").find("td").html(htmlToDisplay);

            $("tr.entryBlock").removeClass("booked-loading");
            $("tr.entryBlock").find(".booked-appt-list").fadeIn(300);
            $("tr.entryBlock").find(".booked-appt-list").addClass("shown");
            adjust_calendar_boxes();
          }, 500);
        }
      }
      return;
    });
  });

  function is_desktop() {
    var isDesktop = false;
    // Check if we're on mobile or Desktop:
    if (window.innerWidth >= 768) {
      // we're on a large screen - let's display classes on the right hand side space
      isDesktop = true;
    }
    return isDesktop;
  }

  function reflect_choice_changes_qty_input(
    $input,
    total_price,
    total_count,
    product_id_list
  ) {
    // let's get the column showing what is selected
    var selected_product = $input
      .parent()
      .parent()
      .parent()
      .find("td#product-price");
    // the qty in the input currently selected
    var selected_qty = $input
      .parent()
      .parent()
      .parent()
      .find("input#single-class-quantity")
      .val();
    // the column with the total price
    var total_column = $input.parent().parent().parent().find("td#total-price");
    // and finally the "next add child detail" button
    var button = $input.parent().parent().parent().find("a#add_term_to_cart");

    // now we update the selected product column to show how many classes are booked and the price
    if (total_count === 0) {
      selected_product.html("please select a date");
    } else {
      selected_product.html(
        total_count + " classes - £" + Math.round(total_price * 100) / 100
      );
    }
    // update the data-product-price attribute
    selected_product.attr("data-product-price", total_price);

    // update the data-product-id attribute
    button.attr("data-product-id", product_id_list);

    // update the total price column based on the quantity (how many kids) are selected
    var complete_total_price = total_price * selected_qty;
    // round to two decimals if need be
    complete_total_price = Math.round(complete_total_price * 100) / 100;
    // we insert the price in the Total Column
    total_column.html("£" + complete_total_price);

    return true;
  }

  function show_loader() {
    $("p.ajax-status")
      .show()
      .html(
        '<p style="text-align: center"><i class="fa fa-spinner"></i>&nbsp;&nbsp;&nbsp; Please wait</p>'
      );
  }
  function hide_loader() {
    $("p.ajax-status").hide();
  }

  // this function is used on the "Add Child to class" page
  // It checks if the button "add to cart" can be activated
  // it does so by comparing the value of the input with the data-validate-button attribute
  function check_activate_add_to_cart() {
    var current_entered_children = $("input#child_detail_entered").val();
    var number_of_kids_to_enter = $("input#child_detail_entered").attr(
      "data-validate-button"
    );
    if (current_entered_children >= number_of_kids_to_enter) {
      $("button#add-to-cart-btn").removeClass("inactive");
      $("button#add-to-cart-and-go-back-btn").removeClass("inactive");
    } else {
      $("button#add-to-cart-btn").removeClass("active");
      $("button#add-to-cart-btn").addClass("inactive");
      $("button#add-to-cart-and-go-back-btn").removeClass("active");
      $("button#add-to-cart-and-go-back-btn").addClass("inactive");
    }
  }

  function adjust_calendar_boxes() {
    jQuery(".booked-calendar").each(function () {
      var windowWidth = jQuery(window).width();
      var smallCalendar = jQuery(this)
        .parents(".booked-calendar-wrap")
        .hasClass("small");
      var boxesWidth = jQuery(this).find("tbody tr.week td").width();
      var calendarHeight = jQuery(this).height();
      boxesHeight = boxesWidth * 0.8;
      jQuery(this).find("tbody tr.week td").height(boxesHeight);
      jQuery(this)
        .find("tbody tr.week td .date")
        .css("line-height", boxesHeight + "px");
      jQuery(this)
        .find("tbody tr.week td .date .number")
        .css("line-height", boxesHeight + "px");
      if (smallCalendar || windowWidth < 720) {
        jQuery(this)
          .find("tbody tr.week td .date .number")
          .css("line-height", boxesHeight + "px");
      } else {
        jQuery(this)
          .find("tbody tr.week td .date .number")
          .css("line-height", "");
      }

      var calendarHeight = jQuery(this).height();
      jQuery(this).parent().height(calendarHeight);
    });
  }

  // Create Booked Modal
  function create_booked_modal() {
    var windowHeight = jQuery(window).height();
    var windowWidth = jQuery(window).width();
    if (windowWidth > 720) {
      var maxModalHeight = windowHeight - 220;
    } else {
      var maxModalHeight = windowHeight;
    }

    jQuery("body input, body textarea, body select").blur();
    jQuery("body").addClass("booked-noScroll");
    jQuery(
      '<div class="booked-modal bm-loading"><div class="bm-overlay"></div><div class="bm-window"><div style="height:100px"></div></div></div>'
    ).appendTo("body");
    jQuery(".booked-modal .bm-overlay").spin("booked_white");
    jQuery(".booked-modal .bm-window").css({
      "max-height": maxModalHeight + "px",
    });
  }

  var previousRealModalHeight = 100;
  function resize_booked_modal() {
    var windowHeight = jQuery(window).height();
    var windowWidth = jQuery(window).width();

    var common43 = 43;

    if (jQuery(".booked-modal .bm-window .booked-scrollable").length) {
      var realModalHeight = jQuery(
        ".booked-modal .bm-window .booked-scrollable"
      )[0].scrollHeight;

      if (realModalHeight < 100) {
        realModalHeight = previousRealModalHeight;
      } else {
        previousRealModalHeight = realModalHeight;
      }
    } else {
      var realModalHeight = 0;
    }
    var minimumWindowHeight = realModalHeight + common43 + common43;
    var modalScrollableHeight = realModalHeight - common43;
    var maxModalHeight;
    var maxFormHeight;

    if (windowHeight < minimumWindowHeight) {
      modalScrollableHeight = windowHeight - common43 - common43;
    } else {
      modalScrollableHeight = realModalHeight;
    }

    if (windowWidth > 720) {
      maxModalHeight = modalScrollableHeight - 25;
      maxFormHeight = maxModalHeight - 15;
      var modalNegMargin = (maxModalHeight + 78) / 2;
    } else {
      maxModalHeight = windowHeight - common43;
      maxFormHeight = maxModalHeight - 60;
      var modalNegMargin = maxModalHeight / 2;
    }

    jQuery(".booked-modal").css({ "margin-top": "-" + modalNegMargin + "px" });
    jQuery(".booked-modal .bm-window").css({
      "max-height": maxModalHeight + "px",
    });
    jQuery(".booked-modal .bm-window .booked-scrollable").css({
      "max-height": maxFormHeight + "px",
    });

    add_datepicker_to_dob_field();
  }

  function add_datepicker_to_dob_field() {
    var picker = new Pikaday({
      field: $(".bm-window #dob")[0],
      format: "DD/MM/YYYY",
    });
  }

  function close_booked_modal() {
    var modal = jQuery(".booked-modal");
    modal.fadeOut(200);
    modal.addClass("bm-closing");
    jQuery("body").removeClass("booked-noScroll");
    setTimeout(function () {
      modal.remove();
    }, 300);
  }

  // initialise the toooltips displaying the classes info over the days in the calendar
  function init_tooltips(container) {
    jQuery(".tooltipster").tooltipster({
      theme: "tooltipster-light",
      animation: "grow",
      speed: 200,
      delay: 50,
      offsetY: -15,
    });
  }

  // Saving state updater
  function savingState(show, limit_to) {
    show = typeof show !== "undefined" ? show : true;
    limit_to = typeof limit_to !== "undefined" ? limit_to : false;

    if (limit_to) {
      var $savingStateDIV = limit_to.find(
        "li.active .savingState, .topSavingState.savingState, .calendarSavingState"
      );
      var $stuffToHide = limit_to.find(".monthName");
      var $stuffToTransparent = limit_to.find("table.booked-calendar tbody");
    } else {
      var $savingStateDIV = $(
        "li.active .savingState, .topSavingState.savingState, .calendarSavingState"
      );
      var $stuffToHide = $(".monthName");
      var $stuffToTransparent = $("table.booked-calendar tbody");
    }

    if (show) {
      $savingStateDIV.fadeIn(200);
      $stuffToHide.hide();
      $stuffToTransparent.animate({ opacity: 0.2 }, 100);
    } else {
      $savingStateDIV.hide();
      $stuffToHide.show();
      $stuffToTransparent.animate({ opacity: 1 }, 0);
    }
  }

  function bookedRemoveEmptyTRs() {
    $("table.booked-calendar")
      .find("tr.week")
      .each(function () {
        if ($(this).children().length == 0) {
          $(this).remove();
        }
      });
  }

  function add_new_child_row(child_id) {
    // create the new row
    new_id = parseInt(child_id) + 1;
    // update the child id references
    new_row = '<div class="child-detail-row">';
    new_row += '<div id="child-detail" data-child-id="' + new_id + '">';
    new_row +=
      '<p id="child-name" data-child-id="' +
      new_id +
      '" style="width: 60%">Click the button to add a child</p>';
    new_row +=
      '<a id="add-child-detail" data-ajax-action="my_account_save_child_detail" data-my-account="true" data-child-id="' +
      new_id +
      '" class="add-child-manually">Enter details</a>';
    new_row += "</div>";

    // set a new empty childDetail[new_id]
    window.childDetail = window.childDetail || [];
    window.childDetail[new_id] = {
      product_id: "",
      firstname: "",
      lastname: "",
      dob: "",
      gender: "",
      nursery: "",
      notes: "",
    };
    // now we actually insert that in the page
    $("div.child-detail-row").last().after(new_row);
    // insert the modal form to add a child's detail
    $("div.child-detail-row")
      .last()
      .append($("div.child-detail-row").first().find("div#html_for_modal"));
  }

  function scroll_to_error(selector) {
    // scroll to the error - especially useful on mobile
    $(selector).scrollTop(0);
  }

  $(document).ajaxStop(function () {
    savingState(false);
  });
})(jQuery, window, document);
