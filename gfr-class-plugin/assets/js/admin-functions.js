// Function to send the ajax request when select options are changed
(function($, window, document, undefined) {
  var $win = $(window);

  $win.on("load", function() {
    $.fn.spin.presets.booked = {
      lines: 10, // The number of lines to draw
      length: 7, // The length of each line
      width: 5, // The line thickness
      radius: 11, // The radius of the inner circle
      corners: 1, // Corner roundness (0..1)
      rotate: 0, // The rotation offset
      direction: 1, // 1: clockwise, -1: counterclockwise
      color: "#555", // #rgb or #rrggbb or array of colors
      speed: 1, // Rounds per second
      trail: 60, // Afterglow percentage
      shadow: false, // Whether to render a shadow
      hwaccel: false, // Whether to use hardware acceleration
      className: "booked-spinner", // The CSS class to assign to the spinner
      // zIndex: 2e9, // The z-index (defaults to 2000000000)
      top: "50%", // Top position relative to parent
      left: "50%" // Left position relative to parent
    };

    // Function to reload the admin week view on dropdown select change
    $(".booked_calendar_chooser").change(function(e) {
      e.preventDefault();

      console.log( "booked_calendar_chooser" );

      var $button = $(this),
        adminCalendarTable = $button.parents().find("table.admin-week-table");

      // Get the curren selected term and location
      var class_location = $("select#admin-location").val();
      var classname = $("select#admin-class-name").val();
      var gotoWeek = $("span.weekName").attr("data-date");
      var date = $("#datepicker-day").val();

      var args = {
        action: "admin_reload_admin_table",
        location: class_location,
        classname: classname,
        gotoWeek: gotoWeek,
        date: date,
      };

      console.log( args );
      
      // Show the Spinner
      adminCalendarTable.css("opacity", "0.2");
      adminCalendarTable.spin("booked");

      $.ajax({
        url: "/wp-admin/admin-ajax.php",
        type: "post",
        data: args,
        success: function(html) {
          adminCalendarTable.html(html);

          setTimeout(function() {
            $(".booked-modal .bm-overlay")
              .find(".booked-spinner")
              .remove();
            adminCalendarTable.css("opacity", "1");
          }, 1);
        }
      });
      return false;
    });

    // Admin Week view Next/Prev Click
    $(".admin-week-table").on(
      "click",
      ".page-right, .page-left, .monthName a",
      function(e) {
        e.preventDefault();

        var $button = $(this),
          gotoWeek = $button.attr("data-goto"),
          thisCalendarWrap = $button.parents(".admin-week-table");

        var args = {
          action: "admin_load_week_classes",
          location: $("#booked_calendar_chooser_1").val(),
          gotoWeek: gotoWeek
        };

        savingState(true, thisCalendarWrap);

        $.ajax({
          url: "/wp-admin/admin-ajax.php",
          type: "post",
          data: args,
          success: function(html) {
            thisCalendarWrap.html(html);
            $(window).trigger("booked-load-calendar", args, $button);
          }
        });

        return false;
      }
    );

    // Admin Week view Click "Back to Current Week"
    $(".admin-week-table").on("click", ".backToCurrentWeek", function(e) {
      e.preventDefault();

      var $button = $(this),
        gotoWeek = $button.attr("data-goto"),
        thisCalendarWrap = $button.parents(".admin-week-table");

      var args = {
        action: "admin_load_week_classes",
        location: $("#booked_calendar_chooser_1").val(),
        gotoWeek: gotoWeek
      };

      savingState(true, thisCalendarWrap);

      $.ajax({
        url: "/wp-admin/admin-ajax.php",
        type: "post",
        data: args,
        success: function(html) {
          thisCalendarWrap.html(html);
          $(window).trigger("booked-load-calendar", args, $button);
        }
      });

      return false;
    });
  });

  function is_desktop() {
    var isDesktop = false;
    // Check if we're on mobile or Desktop:
    if (window.innerWidth >= 768) {
      // we're on a large screen - let's display classes on the right hand side space
      isDesktop = true;
    }
    return isDesktop;
  }

  function show_loader() {
    $("p.ajax-status")
      .show()
      .html(
        '<p style="text-align: center"><i class="fa fa-spinner"></i>&nbsp;&nbsp;&nbsp; Please wait</p>'
      );
  }

  // Create Booked Modal
  function create_booked_modal() {
    var windowHeight = jQuery(window).height();
    var windowWidth = jQuery(window).width();
    if (windowWidth > 720) {
      var maxModalHeight = windowHeight - 295;
    } else {
      var maxModalHeight = windowHeight;
    }

    jQuery("body input, body textarea, body select").blur();
    jQuery("body").addClass("booked-noScroll");
    jQuery(
      '<div class="booked-modal bm-loading"><div class="bm-overlay"></div><div class="bm-window"><div style="height:100px"></div></div></div>'
    ).appendTo("body");
    jQuery(".booked-modal .bm-overlay").spin("booked_white");
    jQuery(".booked-modal .bm-window").css({
      "max-height": maxModalHeight + "px"
    });
  }
  var previousRealModalHeight = 100;
  function resize_booked_modal() {
    var windowHeight = jQuery(window).height();
    var windowWidth = jQuery(window).width();

    var common43 = 43;

    if (jQuery(".booked-modal .bm-window .booked-scrollable").length) {
      var realModalHeight = jQuery(
        ".booked-modal .bm-window .booked-scrollable"
      )[0].scrollHeight;

      if (realModalHeight < 100) {
        realModalHeight = previousRealModalHeight;
      } else {
        previousRealModalHeight = realModalHeight;
      }
    } else {
      var realModalHeight = 0;
    }
    var minimumWindowHeight = realModalHeight + common43 + common43;
    var modalScrollableHeight = realModalHeight - common43;
    var maxModalHeight;
    var maxFormHeight;

    if (windowHeight < minimumWindowHeight) {
      modalScrollableHeight = windowHeight - common43 - common43;
    } else {
      modalScrollableHeight = realModalHeight;
    }

    if (windowWidth > 720) {
      maxModalHeight = modalScrollableHeight - 25;
      maxFormHeight = maxModalHeight - 15;
      var modalNegMargin = (maxModalHeight + 78) / 2;
    } else {
      maxModalHeight = windowHeight - common43;
      maxFormHeight = maxModalHeight - 60;
      var modalNegMargin = maxModalHeight / 2;
    }

    jQuery(".booked-modal").css({ "margin-top": "-" + modalNegMargin + "px" });
    jQuery(".booked-modal .bm-window").css({
      "max-height": maxModalHeight + "px"
    });
    jQuery(".booked-modal .bm-window .booked-scrollable").css({
      "max-height": maxFormHeight + "px"
    });
  }

  function close_booked_modal() {
    var modal = jQuery(".booked-modal");
    modal.fadeOut(200);
    modal.addClass("bm-closing");
    jQuery("body").removeClass("booked-noScroll");
    setTimeout(function() {
      modal.remove();
    }, 300);
  }

  // initialise the toooltips displaying the classes info over the days in the calendar
  function init_tooltips(container) {
    jQuery(".tooltipster").tooltipster({
      theme: "tooltipster-light",
      animation: "grow",
      speed: 200,
      delay: 50,
      offsetY: -15
    });
  }

  // Saving state updater
  function savingState(show, limit_to) {
    show = typeof show !== "undefined" ? show : true;
    limit_to = typeof limit_to !== "undefined" ? limit_to : false;

    if (limit_to) {
      var $savingStateDIV = limit_to.find(
        "li.active .savingState, .topSavingState.savingState, .calendarSavingState"
      );
      var $stuffToHide = limit_to.find(".monthName");
      var $stuffToTransparent = limit_to.find("table.booked-calendar tbody");
    } else {
      var $savingStateDIV = $(
        "li.active .savingState, .topSavingState.savingState, .calendarSavingState"
      );
      var $stuffToHide = $(".monthName");
      var $stuffToTransparent = $("table.booked-calendar tbody");
    }

    if (show) {
      $savingStateDIV.fadeIn(200);
      $stuffToHide.hide();
      $stuffToTransparent.animate({ opacity: 0.2 }, 100);
    } else {
      $savingStateDIV.hide();
      $stuffToHide.show();
      $stuffToTransparent.animate({ opacity: 1 }, 0);
    }
  }

  function bookedRemoveEmptyTRs() {
    $("table.booked-calendar")
      .find("tr.week")
      .each(function() {
        if ($(this).children().length == 0) {
          $(this).remove();
        }
      });
  }

  $(document).ajaxStop(function() {
    savingState(false);
  });
})(jQuery, window, document);
