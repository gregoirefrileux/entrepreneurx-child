<?php


add_shortcode( 'add_child_class', 'gfr_add_child_class' );
add_shortcode( 'class_detail', 'gfr_show_class_detail' );

// register some ajax function
// the below is used on the "Add Child To Class" page, when a parent adds a child to their class
add_action('wp_ajax_add_child_detail', 'gfr_add_save_child_detail_to_session');
add_action('wp_ajax_nopriv_add_child_detail', 'gfr_add_save_child_detail_to_session');
// this one below is adding the class to cart, and attaching the child-detail meta
add_action('wp_ajax_add_class_to_cart_with_detail', 'gfr_add_class_to_cart_with_child_detail');
add_action('wp_ajax_nopriv_add_class_to_cart_with_detail', 'gfr_add_class_to_cart_with_child_detail');



function gfr_add_child_class( $atts ) {
    $params = shortcode_atts( array(
       'location' => 'all',
       'term' => 'novalue',
       'month' => 'nomonth',
       'year' => 'noyear'
    ), $atts );

    // enqueue the style on the page
    wp_enqueue_style( 'gfr-booking-style' );
    wp_enqueue_style( 'gfr-booking-responsive' ); 
    wp_enqueue_style( 'gfr-tooltipster' ); 
    wp_enqueue_style( 'gfr-tooltipster-theme' ); 
    wp_enqueue_script('gfr-booking-js');
    wp_enqueue_script( 'spin-js');
    wp_enqueue_script( 'spin-jquery');

    // Retrieve the number of spots to display from URL
    $spots = 1;
    if (isset($_GET['child_detail'])) {
        $spots = intval($_GET['child_detail']);
    }
 
    
    // Retrive the product currently being bought
    $product_id_array = [];
    if (isset($_GET['product_id'])) {
        $product_id_array = explode(",", $_GET['product_id']);
    } else {
        // it's not passed in the URL ???? let's try another trick
        // TODO : display some message to the parent
    }

    // First let's determine if the user is logged in
    $current_user = wp_get_current_user();
    $is_logged_in = $current_user->exists();

    $html = '';
    $html .='<div class="wrapper-add-child-to-class">
                <div class="add-child-content">
                    <h2>Please enter your child\'s detail</h2>
                    ' . show_login_to_retrieve_child_detail_box() . '';
    for($i = 0; $i < $spots ; $i++) {
            if ($is_logged_in) {
                $child_array = gfr_get_meta(get_current_user_id(), 'child-detail') ;

                if ($child_array && is_array($child_array) && sizeof($child_array) > 0) {
                    // we have some pre-saved info about their kids, so we show the module that lets you select pre filled info
                    $html .= show_add_child_logged_in($product_id_array, $i, $child_array);
                } else {
                    // we show the same as if they were not logged in
                    $html .= show_add_child_not_logged_in($product_id_array, $i);
                }
            } else {
                $html .= show_add_child_not_logged_in($product_id_array, $i);
            }
    }
            // Now for the Add to cart button
            $html .='
                        <div class="add-to-cart">
                            <input type="hidden" id="child_detail_entered" value="0" data-validate-button="' . $spots . '">
                            <p class="ajax-status"></p>
                            <button id="add-to-cart-and-go-back-btn" class="inactive go-back-calendar" data-validate-button="' . $spots . '" data-product-id="' . implode(",", $product_id_array) . '" data-href="' . get_permalink( get_page_by_path( 'book-class' ) ) . '">   
                                Add & go back to Calendar
                            </button>
                            <button id="add-to-cart-btn" class="inactive" data-validate-button="' . $spots . '" data-product-id="' . implode(",", $product_id_array) . '" data-href="' . get_permalink( wc_get_page_id( 'cart' ) ) . '">   
                                Add & checkout
                            </button>
                        </div>';
            // and here is the "Good to know" block
            $html .='<div class="good-to-know">
                        <h2>Good to know</h2>
                        <div>
                            <div style="display: flex">   
                                <span>1. </span>
                                <p>You\'ll get an email with your class detail after purchase</p>
                            </div>
                            <div style="display: flex">
                                <span>2. </span>
                                <p>You will get weekly reminder emails before every class </p>
                            </div>
                        </div>
                    </div>
                    ' . show_child_edit_form_modal() . '
                </div>
            </div>';
    return $html;


}


/*
 *
 * Function to show a box telling parent that if they already have an account they should login 
 * 
 * returns: the html showing info block
 * 
 */
function show_login_to_retrieve_child_detail_box() {

    // First we get the login status of the user so we know what to show them
    $logged_in = get_current_user_id() === 0 ? false : true;

    $html ='';

    if (! $logged_in ) {
        // user is not logged in - we show the message box indicating they should login
        $html .= '<p class="alert alert-warning">If you already have an account with us, and have entered your child detail in the past,
        please <a href="/my-account?redirect_to=' . urlencode($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']) . '" title="Login">Login</a> to fill their detail automatically</p>';
    } else {
        // they are logged in - we try and get their kids detail and show that
        $child_array = gfr_get_meta(get_current_user_id(), 'child-detail') ;
        // var_dump($child_array);
        if($child_array && is_array($child_array) && sizeof($child_array) > 0 ) {
            $html .= '<p class="alert alert-success">
            We found the information below saved in your account. To re-use it, simply select the child you would like
            to book using the dropdown menu and press ok. You may also edit the details by clicking edit if anything has changed
            </p>';
            // we also load the kids detail in a global window object so that the popup can access it
            $js_child_array = json_encode($child_array);
            $html .= '<script type="text/javascript">
                var savedChildDetail = window.savedChildDetail || [];
                window.savedChildDetail = ' . $js_child_array .';
                </script>';
        } else {
            $html .= '<p class="alert alert-success">
            We could not find any child information saved in your account. Please fill the information using the 
            form below and we will save it in your account for the next booking.
            </p>';
        }
    }


    return $html;

}

/*
 *
 * Function to show the form to edit / add a child detail 
 * inside a modal window
 * 
 * returns: the html showing the child edit form
 * 
 */
function show_child_edit_form_modal() {

$modal_html = '<div id="html_for_modal" style="display:none">
    <div class="booked-form booked-scrollable" style="max-height: 474px;">
        <div class="frm_forms  with_frm_style frm_style_formidable-style">
            <form class="frm-show-form ">
                <div class="frm_form_fields ">
                    <div class="frm_form_field form-field  frm_required_field frm_none_container frm_first frm_half">
                        <label>First Name *</label>
                        <input id="firstname" placeholder="" autocomplete="off" type="text" value="" data-hj-whitelist>    
                    </div>
                    <div class="frm_form_field form-field  frm_required_field frm_none_container frm_half">
                        <label>Last Name *</label>
                        <input id="lastname" placeholder="" autocomplete="off" type="text" value="" data-hj-whitelist>    
                    </div>
                </div>
                <div class="frm_form_fields">
                    <div class="frm_form_field form-field  frm_required_field frm_none_container frm_first frm_half">
                        <label class="label-no-margin">Gender *</label>
                        <select id="gender">
                            <option value="">Please select a gender</option>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                        </select>
                    </div>
                    <div class="frm_form_field form-field  frm_required_field frm_none_container frm_half">
                        <label class="label-no-margin">Date of Birth *</label>
                        <input id="dob" placeholder="DD/MM/YYYY" autocomplete="off" type="text" value="" size=10 maxlength=10 onkeydown="return false" readonly="true">
                    </div>
                </div>
                <div class="frm_form_fields">
                    <label>Nursery/School your child is attending *</label>
                    <input id="nursery" placeholder="" autocomplete="off" type="text" value="">    
                </div>
                <div>
                    <label>Known Medical Conditions/Allergies *</label>
                    <textarea id="notes" placeholder="" autocomplete="off"></textarea>
                </div>
                <div class="frm_submit" style="text-align: center">
                    <p class="ajax-status"></p>
                    <button class="button add-child-to-class">
                        <span>Done</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
    <p class="booked-title-bar">Please enter your child\'s detail</p>
    <a href="#" class="close"><i class="fa fa-close"></i></a>
</div>'; 

    return $modal_html;

}



/*
 *
 * Function to show the row to add a brand new child detail
 * Used when the parent is not logged in, and so we can't fetch their kids' details from their profile
 * 
 * returns: the html showing row to add a child
 * 
 */
function show_add_child_not_logged_in($product_id_array, $i, $add_ajax_action = 'add_child_detail', $edit_ajax_action='') {

    $child_add_div = '';
    $child_add_div .= $i === 0 ? '<div class="child-detail-row">' : '<div class="add-child-detail-separator"></div><div class="child-detail-row">';
    $child_add_div .= '
                        <div id="child-detail" data-child-id="' . $i . '">
                            <p id="child-name" data-child-id="' . $i . '" data-product-id="' . implode(",", $product_id_array) . '" style="width: 50%">Child ' . ($i + 1) . ' - please enter details</p>
                            <a id="add-child-detail" data-ajax-action="' . $add_ajax_action . '"  data-product-id="' . implode(",", $product_id_array) . '" data-child-id="' . $i . '" class="add-child-manually">Enter child detail</a>
                            <a style="display: none" data-ajax-action="' . $edit_ajax_action . '" id="edit-child-detail" data-product-id="' . implode(",", $product_id_array) . '" data-child-id="' . $i . '"><i class="fa fa-edit"></i>  Edit</a>
                        </div>
                    </div>';
 

    return $child_add_div;
}

/*
 *
 * Function to show the row to add a brand new child detail
 * Used when the parent is not logged in, and so we can't fetch their kids' details from their profile
 * 
 * returns: the html showing row to add a child
 * 
 */
function show_add_child_logged_in($product_id_array, $i, $child_array, $select_ajax_action = 'add_child_detail', $add_ajax_action='add_child_detail', $remove_ajax_action='', $orderId = null) {

    $html ='';
    $html .= $i === 0 ? '<div class="child-detail-row">' : '<div class="add-child-detail-separator"></div><div class="child-detail-row">';
    $html .= '
                    <div id="child-detail" data-child-id="' . $i . '">
                        <p id="child-name" data-child-id="' . $i . '" data-product-id="' . implode(",", $product_id_array) . '" style="width: 35%">Child ' . ($i + 1) . '</p>
                        <p id="child-name-select" data-child-id="' . $i . '" style="width: 35%">
                            <select id="select_saved_child_info" data-child-id="' . $i . '">
                                <option>Please select a child</option>
                                ' . show_child_select_option($child_array) . '
                            </select></p>
                        <a id="select-child-detail" data-ajax-action="' . $select_ajax_action . '" data-product-id="' . implode(",", $product_id_array) . '" data-child-id="' . $i . '" data-order-id="' . $orderId . '"><i class="fa fa-plus"></i> Select for class</a>
                        <a style="display: none" id="remove-child-detail"  data-ajax-action="' . $remove_ajax_action . '"  data-product-id="' . implode(",", $product_id_array) . '" data-child-id="' . $i . '"><i class="fa fa-trash"></i>  Remove</a>
                    </div>
                    <p> Or enter details manually: 
                        <a id="add-child-detail" data-ajax-action="' . $add_ajax_action . '" data-product-id="' . implode(",", $product_id_array) . '" data-child-id="' . $i . '" class="add-child-manually">Enter new details</a>
                    </p>    
                    <p id="select-child-error" style="display: none" class="alert alert-error" data-child-id="' . $i . '">The information saved is incomplete. Please click "Enter manually" to enter the required fields</p>
                </div>';
 
    return $html;
}

function show_child_select_option($child_array) {
    $options = '';
    foreach($child_array as $id => $child) { 
        $options .= '<option value="' . $id . '">' . $child['firstname'] . '</option>';
    };
    return $options;
}

/*
 *
 * Function to show the class currently being booked by a parent, on the page where they add their kids' detail
 * 
 * returns: the html showing the weeks of the month, and for each day how many classes are available
 * 
 */
function gfr_show_class_detail( $atts ) {

    // Retrieve the number of spots to display from URL
    $spots = 1;
    if (isset($_GET['child_detail'])) {
        $spots = intval($_GET['child_detail']);
    }
    
    // Retrieve the product currently being bought
    $product_id_array = [];
    $product_count = 1;
    if (isset($_GET['product_id'])) {
        $product_id_array = explode(",", $_GET['product_id']);
        $product_count = sizeof($product_id_array);
    } else {
        // it's not passed in the URL ???? let's try another trick
        // TO DO show message here too
        return '';
    }

    // get the product from WC
    $class_array = [];
    $single_class = false;
    foreach($product_id_array as $product_id) {
        array_push($class_array, wc_get_product( $product_id ));
    }
    if($product_count === 1) {
        // we only have 1 product
        $single_class = true;
    }
    // initialize $current_class
    // when $single_class = true it's going to be the only thing we care about
    // when we have multiple classes, they're meant to be all at the same time, location, etc. so having the first one is also helpful
    $current_class = $class_array[0];
    // is this a discounted product (i.e. a bunddle with a discount)
    $is_discounted = $current_class->get_sale_price() && $current_class->get_regular_price() != $current_class->get_sale_price();


    // Now let's build the HTML
    $html ='';

    $html .= '<div class="cart-summary-wrapper">
                <div class="cart-summary-gfr">
                    <h2>' . $current_class->get_attribute('class-name ') .'</h2>
                    <div class="cart-summary-detail" data-hj-whitelist>';
                    if ($single_class === true) {
                        $html .= display_single_class_cart_detail($current_class);
                    } else {
                        $html .=  display_multiple_classes_cart_detail($class_array);
                    }
                    $html .='
                    </div>
                    <div class="cart-summary-detail" data-hj-whitelist>
                        <table>
                            ';
                            for($i=0; $i < $spots; $i++) {
                                  // is this product discounted ? 
                                  if($is_discounted) {
                                    // we show the original price and the discounted price
                                    $html .= '<tr><td class="cart-summary-price-label">1 x Child </td><td>
                                        <p class="original-price">£'
                                        . $product_count * $current_class->get_regular_price() . '</p>'
                                        . '  £' .$product_count * $current_class->get_sale_price() . '</td></tr>';
                                  } else {
                                    $html .= '<tr><td class="cart-summary-price-label">1 x Child </td><td>£' . $product_count * $current_class->get_price() . '</td></tr>';
                                  }
                            }
                           $html .=  '</tr><tr class="total"><td>Total:</td><td>£' . $product_count * $spots * ($is_discounted ? $current_class->get_sale_price() : $current_class->get_price()). '</td></tr>
                        </table>
                    </div>
                </div>
            </div>';

    return $html;

}

/*
 *
 * Function to display the cart detail in the right hand side of the Add Child to Class
 * It is used when we only display 1 class on that page
 * Ity takes a single class in a WC_Product format
 * 
 * Returns some html
 * 
 */

function display_single_class_cart_detail($current_class) {

    $class_category_slugs = gfr_get_product_category_slugs($current_class);

    $single_class_html = '
     <p class="appointment-info">
        <i class="fa fa-user" style="font-size: large;"></i> Availability: '. $current_class->get_stock_quantity() . ' available<br />';
        if ( !in_array('holiday-bundle-classes', $class_category_slugs) && !in_array('term-class', $class_category_slugs)) {
            // this is a standard class, so we display the day and single date
            $single_class_html .='<i class="fa fa-calendar"></i>
            Class date: ' . $current_class->get_attribute('day-of-the-week ') . ' ' . date_i18n('dS M Y',strtotime($current_class->get_attribute('class-date '))) . '<br />
            <i class="fa fa-clock-o"></i> Class time: ' . $current_class->get_attribute('start-time') . ' - ' . $current_class->get_attribute('finish-time') . '<br />';
        } else if(in_array('holiday-bundle-classes', $class_category_slugs)) {
            // Showing the dates of a holiday bundle class - every day for 1 week
            $class_dates = explode(",", $current_class->get_attribute('class-date '));

            $single_class_html .='<i class="fa fa-calendar"></i>
            Class date: From '. date_i18n('dS M Y',strtotime($class_dates[0])) .
            ' to ' . date_i18n('dS M Y',strtotime($class_dates[sizeof($class_dates) - 1])) . '<br />';
            $single_class_html .= '<i class="fa fa-clock-o"></i>
            Class time:  between '. $current_class->get_attribute('start-time') . '
            and ' . $current_class->get_attribute('finish-time') . '<br />';
        } else if(in_array('term-class', $class_category_slugs)) {
            // Showing the dates of a term class
            $single_class_html .='<i class="fa fa-calendar"></i>
            Class date: Every ' . $current_class->get_attribute('day-of-the-week ') . ' between ' . $current_class->get_attribute('start-time') . '
            and ' . $current_class->get_attribute('finish-time') . '<br />';
        } 
        $single_class_html .='<i class="fa fa-map-marker"></i>
        Location: ' . $current_class->get_attribute('location ') . '
    </p>';

    return $single_class_html;

}

/*
 *
 * Function to display the cart detail in the right hand side of the Add Child to Class
 * It is used when we only display multiple classes on that page
 * Ity takes an array of single classes in a WC_Product format
 * 
 * Returns some html
 * 
 */
function display_multiple_classes_cart_detail($class_array) {

    $many_class_html = '
     <p class="appointment-info">
        <i class="fa fa-calendar"></i> Classes dates: ' . $class_array[0]->get_attribute('day-of-the-week ' . ' ');
        foreach ( $class_array as $single_class ) {
            // this is a standard class, so we display the day and single date
            $many_class_html .= date_i18n('dS M Y',strtotime($single_class->get_attribute('class-date '))) .  ', ';
        } 
        $many_class_html .='<br />
        <i class="fa fa-map-marker"></i> Location: ' . $class_array[0]->get_attribute('location ') . '
    </p>';

    return $many_class_html;

}

/*
 *
 * AJAX Function add the child's detail to the session so it can be accessed later
 * Takes in the product_id, as well as all the detail about the children
 * It creates a SESSION object lining the child detail and the product ID (so we can later merge these things in the cart_meta)
 * 
 * returns: success or failure
 * 
 */
function gfr_add_save_child_detail_to_session() {

    // Retrieve the data sent in the POST
    $product_id_list = (isset($_POST['product_id']) ? $_POST['product_id'] : null);
    $product_ids = explode(",", $product_id_list);
    $child_id = (isset($_POST['child_id']) ? $_POST['child_id'] : null);
    $firstname = (isset($_POST['firstname']) ? $_POST['firstname'] : null);
    $lastname = (isset($_POST['lastname']) ? $_POST['lastname'] : null);
    $dob = (isset($_POST['dob']) ? $_POST['dob'] : null);
    $notes = (isset($_POST['notes']) ? $_POST['notes'] : null);

    // Has the $_SESSION object been initialised?
    if ( !isset($_SESSION['child-detail']) ) {
        $_SESSION['child-detail'] = array();
    }

    // Check that we have all the data
    if ($product_id_list === null || $child_id === null || $firstname === null || $lastname === null || $dob === null) {
        // we're missing key data
        $data = array(
            'error'       => true,
            'message' => "Please provide all mandatory fields"
        );
    } else {
        // OK then, we try to add the product to cart with all the correct info
        $_SESSION['child-detail'][$child_id] = array (
                'firstname' => $firstname,
                'lastname' => $lastname,
                'dob' => $dob,
                'product_id' => sizeof($product_ids) === 1 ? (int)$product_ids[0] : $product_id_list,
                'notes' => $notes,
        );

        $data = array(
            'error'       => false,
        );

    }

    // return success or failure to the page
    wp_send_json( $data );

    wp_die();

}
/*
 *
 * AJAX Function add the child's detail to cart
 * Once a parent has added all their kids detail in the ADD Child to class page
 * They can add the class to cart
 * The function takes the product and qty, retrieves the SESSION object, and adds the product to cart + it adds the correct meta_data with the children details
 * 
 * returns: a json object with either an error or true if all went well
 * 
 */
function gfr_add_class_to_cart_with_child_detail() {

    // Retrieve the data sent in the POST
    $product_id_list = (isset($_POST['product_id']) ? $_POST['product_id'] : null);
    $quantity =  (isset($_POST['quantity']) ? $_POST['quantity'] : null);

    // Check that we have all the data
    if ($product_id_list === null || $quantity === null) {
        // we're missing key data
        $data = array(
            'error'       => true,
            'message' => "Please provide all mandatory fields"
        );
    } else {
        // OK then, we try to add the product to cart with all the correct info
        // Again we may have several products so we loop the list
        foreach( explode(",", $product_id_list) as $product_id ) {
            $passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );
            $product_status    = get_post_status( $product_id );

            if ( $passed_validation && WC()->cart->add_to_cart( $product_id, $quantity ) && 'publish' === $product_status ) {

                do_action( 'woocommerce_ajax_added_to_cart', $product_id );

                wc_add_to_cart_message( $product_id );

                $data = array(
                    'error'       => false,
                );
        
                
            } else {
                // If there was an error adding to the cart, redirect to the product page to show any errors
                $data = array(
                    'error'       => true,
                    'message' => "Sorry, one of your classes is out of stock"
                );

                wp_send_json( $data );

            }
        }

    }

    // we shouldn't really get here but if all fails, return something
    wp_send_json( $data );

    wp_die();

}



?>