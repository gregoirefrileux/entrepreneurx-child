jQuery(document).ready(function($) {

  $("#save_attendance").submit(function(e) {
    e.preventDefault();

    var data = {
      'action': 'save_attendance_action',
      'attendance_data': get_attendance_data(),
      'class_id': findGetParameter( "class-id" )
    };

    $.post(ajax_object.ajax_url, data, function(response) {
      location.reload();
    });
  });

  $("#toggle_attendance").change(function(){
    toggle_attendance($(this).is(":checked"));
  });

  function get_attendance_data() {
    var data = [];
    $rows = $('.admin-class-row');
    for ( var i = 0; i < $rows.length; i++ ) {
      var row = $($rows[i]);
      data.push({
        order_id: row.data('order-id'),
        child_key: row.data('child-key'),
        did_attend: get_did_attend(row) ? 1 : 0,
      });
    }
    return data;
  }

  function get_did_attend(row) {
    return $(row).find("#attendance").is(':checked');
  }

  function toggle_attendance( did_attend ) {
    $('input[name="attendance"]').each(function(){ this.checked = did_attend; });
  }

  function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
  }


});
