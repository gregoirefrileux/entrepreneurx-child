jQuery(document).ready(function($) {

  var waitlistFormHtml = `
    <div>
      <p style="margin: 0">You will receive an email if a space opens up.</p>
      <form id="join-waitlist-form">
        <input class="join-waitlist-email" type="email" placeholder="Enter your email" required=""><br>
        <input type="hidden" class="product-id" value="29043">
        <input class="modal-next-btn" type="submit" value="Submit">
      </form>
      <div class="server-response">
      </div>
    </div>
  `;

  $("body").on('submit', "#join-waitlist-form", function(e) {
    e.preventDefault();

    $submit_btn = $(this).find('input[type="submit"]')
    $server_response_div = $(this).parent().find('.server-response');

    var data = {
      'action': 'join_waitlist_action',
      'email' : $(this).find('.join-waitlist-email').val(),
      'product_id' : $(this).find('.product-id').val(),
    };

    $submit_btn.attr("disabled", true);
    $server_response_div.text('');

    $.post(ajax_object.ajax_url, data, function(response) {
      $server_response_div.text(response);
      setTimeout(function(){ $submit_btn.attr("disabled", false); }, 6000);
    });
  });

  function getWaitlistHFormHtml(productId) {
    return waitlistFormHtml.replace("29043", productId);
  }

  $("body").on('click', ".join-waitlist-inner", function(e) {
    var formHtml = getWaitlistHFormHtml( $(this).data('product-id') );
    $(formHtml).insertAfter($(this));
  });


});
