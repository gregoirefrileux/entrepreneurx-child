jQuery(document).ready(function($) {

  $("#go_to_next_week").submit(function(e) {
    e.preventDefault();

    var classID = findGetParameter( "class-id" );

    var data = {
      'action': 'go_to_next_week_action',
      'class_id': classID
    };

    $.post(ajax_object.ajax_url, data, function(response) {
      if ( response == -1 )
        return alert( "Error: unable to find a class for next week." );
      var newURL = updateURLParameter( window.location.href, 'class-id', response );
      window.location.href = newURL;
    });
  });

  function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
  }

  /**
   * http://stackoverflow.com/a/10997390/11236
   */
  function updateURLParameter(url, param, paramVal){
      var newAdditionalURL = "";
      var tempArray = url.split("?");
      var baseURL = tempArray[0];
      var additionalURL = tempArray[1];
      var temp = "";
      if (additionalURL) {
          tempArray = additionalURL.split("&");
          for (var i=0; i<tempArray.length; i++){
              if(tempArray[i].split('=')[0] != param){
                  newAdditionalURL += temp + tempArray[i];
                  temp = "&";
              }
          }
      }

      var rows_txt = temp + "" + param + "=" + paramVal;
      return baseURL + "?" + newAdditionalURL + rows_txt;
  }

});
