jQuery(document).ready(function($) {

  $(".email-all-parents").click(function(e) {
    e.preventDefault();

    var data = {
			'action': 'get_parents_emails_action',
			'class_ids': getClassIds()
		};

		$.post(ajax_object.ajax_url, data, function(response) {
      var emails = JSON.parse( response );
      openMailLink( emails );
		});
  });

  function getClassIds() {
    var result = [];
    $rows = $(".admin-class-row");
    for ( i = 0; i < $rows.length; i++ )
      result.push( $( $rows[ i ] ).data('class-id') );
    return result;
  }

  function openMailLink( emails ) {
    if ( emails.length == 0 )
      return;
    location.href = "mailto:" + emails.join(",");
  }

});
