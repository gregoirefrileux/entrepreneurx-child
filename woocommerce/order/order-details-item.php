<?php
/**
 * Order Item Details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details-item.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
	return;
}
?>
<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'woocommerce-table__line-item order_item', $item, $order ) ); ?>">

	<td class="woocommerce-table__product-name product-name">
		<?php
		$is_visible        = $product && $product->is_visible();
		$product_permalink = apply_filters( 'woocommerce_order_item_permalink', $is_visible ? $product->get_permalink( $item ) : '', $item, $order );

		// echo apply_filters( 'woocommerce_order_item_name', $product_permalink ? sprintf( '<a href="%s">%s</a>', $product_permalink, $item->get_name() ) : $item->get_name(), $item, $is_visible ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	
		// Greg modification: if the product is a standard class we display its "class name" attribute and "date" instead of the name

			// 36 is "Standard Class" and 37 is "Holiday Class" which behaves like a normal class
			if ( in_array(36, $product->get_category_ids()) || in_array(37, $product->get_category_ids()) ) {
				echo wp_kses_post( apply_filters( 'woocommerce_order_item_name', $product->get_attribute("pa_class-name")
					. "<br /><span class='cart-product-detail'>Date: " . apply_filters( 'woocommerce_cart_display_term_dates', $product->get_id())
					. " - Location: " . $product->get_attribute("pa_location")
					. "</span>", $item ) . '&nbsp;' );
			} else  if ( in_array(57, $product->get_category_ids()) ){
				// Showing the dates of classes for Term products - category Id = "57"
				echo wp_kses_post( apply_filters( 'woocommerce_order_item_name', 
					$product->get_attribute("pa_class-name") . " - " . $product->get_attribute("pa_term-class")
					. "<br /><span class='cart-product-detail'>Day and Start time: " . $product->get_attribute("pa_day-of-the-week") . " - " . $product->get_attribute("pa_start-time") . " - Location: " . $product->get_attribute("pa_location")
					. "<br /><span class='cart-product-detail'>Dates: " . apply_filters( 'woocommerce_cart_display_term_dates', $product->get_id())
					. "</span>", $item, $item_key ) . '&nbsp;' );
			} else {
				echo wp_kses_post( apply_filters( 'woocommerce_order_item_name', $product->get_name(), $item, $item_key ) . '&nbsp;' );
			}
		$qty          = $item->get_quantity();
		$refunded_qty = $order->get_qty_refunded_for_item( $item_id );

		if ( $refunded_qty ) {
			$qty_display = '<del>' . esc_html( $qty ) . '</del> <ins>' . esc_html( $qty - ( $refunded_qty * -1 ) ) . '</ins>';
		} else {
			$qty_display = esc_html( $qty );
		}

		echo apply_filters( 'woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf( '&times; %s', $qty_display ) . '</strong>', $item ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

		do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order, false );

		wc_display_item_meta( $item ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

		do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order, false );
		?>
	</td>

	<td class="woocommerce-table__product-total product-total">
		<?php echo $order->get_formatted_line_subtotal( $item ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
	</td>

</tr>

<?php if ( $show_purchase_note && $purchase_note ) : ?>

<tr class="woocommerce-table__product-purchase-note product-purchase-note">

	<td colspan="2"><?php echo wpautop( do_shortcode( wp_kses_post( $purchase_note ) ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></td>

</tr>

<?php endif; ?>
