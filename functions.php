<?php

/*
 *
 * This simple line is the single most important: it registers the custom plugin
 * that handles the entire booking system
 * DO NOT DELETE - EVER !!
 * One day we need to refactor this into a plugin
 */
require_once('gfr-class-plugin/booking.php');
require_once('gfr-class-plugin/attendance_meta.php');
require_once('gfr-class-plugin/includes/class-waiting-list-table.php');
require_once('gfr-class-plugin/includes/class-wc-waiting-list-admin-screen.php');

require_once('gfr-class-plugin/includes/class-product-stock.php');

/*
 *
 * Below is more common - simply register our custom CSS and JS files
 *
 */

function enqueue_child_theme_style() {
    wp_enqueue_style( 'dtbwp_css_child', get_stylesheet_directory_uri() . '/style.css', array(
        'dtbwp_style',
    ), 1.0 );
}
add_action( 'wp_enqueue_scripts', 'enqueue_child_theme_style', 9999 );


function gfr_adding_scripts() {
    wp_enqueue_script( 'moment', get_stylesheet_directory_uri() . '/js/moment.min.js', [], false, true );
    wp_enqueue_script( 'pikaday', get_stylesheet_directory_uri() . '/js/pikaday.min.js', [], false, true );
    wp_register_script('gfr_custom_script', get_stylesheet_directory_uri() . '/custom-js.js', array('jquery'),'1.1', true);
    wp_enqueue_script('gfr_custom_script');

}
add_action( 'wp_enqueue_scripts', 'gfr_adding_scripts', 999 );

function jdr_adding_styles() {
  wp_enqueue_style( 'pikaday', get_stylesheet_directory_uri() . '/css/pikaday.min.css', [], false, false );
  wp_enqueue_script('scroll-to-child-detail-error', get_stylesheet_directory_uri() . '/js/scroll-to-child-detail-error.js', array('jquery'), false, true);
  wp_enqueue_script('join-waitlist', get_stylesheet_directory_uri() . '/js/join-waitlist.js', array('jquery'), 2, true);
  wp_localize_script( 'join-waitlist', 'ajax_object', [ 'ajax_url' => admin_url( 'admin-ajax.php' ) ] );
}
add_action( 'wp_enqueue_scripts', 'jdr_adding_styles', 999 );

/*
 *
 * Customising the Admin interface, and show the linked products in bundle in smaller font so you can actually see more text
 *
 */


// Update CSS within in Admin
function admin_style() {
    wp_enqueue_style('admin-styles', get_stylesheet_directory_uri().'/admin.css');
    wp_enqueue_script('admin-script', get_stylesheet_directory_uri().'/admin.js');
    wp_enqueue_script('go-to-next-week', get_stylesheet_directory_uri() . '/js/go-to-next-week.js', array('jquery'), false, true);
    wp_localize_script( 'go-to-next-week', 'ajax_object', [
      'ajax_url' => admin_url( 'admin-ajax.php' ),
    ]);
    wp_enqueue_script('save-attendance', get_stylesheet_directory_uri() . '/js/save-attendance.js', array('jquery'), false, true);
    wp_localize_script( 'save-attendance', 'ajax_object', [
      'ajax_url' => admin_url( 'admin-ajax.php' ),
    ]);
    wp_enqueue_script( 'moment', get_stylesheet_directory_uri() . '/js/moment.min.js', [], false, true );
    wp_enqueue_script( 'email-all-parents', get_stylesheet_directory_uri() . '/js/email-all-parents.js', array('jquery'), false, true);
    wp_localize_script( 'email-all-parents', 'ajax_object', [
      'ajax_url' => admin_url( 'admin-ajax.php' ),
    ]);

    wp_enqueue_script( 'moment', get_stylesheet_directory_uri() . '/js/moment.min.js', [], false, true );
    wp_enqueue_script( 'pikaday', get_stylesheet_directory_uri() . '/js/pikaday.min.js', [], false, true );
    wp_enqueue_style( 'pikaday', get_stylesheet_directory_uri() . '/css/pikaday.min.css', [], false, false );
}

add_action('admin_enqueue_scripts', 'admin_style');

/*
 * Slightly dirty hack to make the LOG OUT LINK Work
 * We echo a hidden div in the page that we can then use in custmjs file
 *
 */
function gfr_echo_hidden_logout_link () {
    echo "<p style='display:none' data-hidden-gfr data-link='" . wc_logout_url() . " '/>";
}
add_action('wp_head', 'gfr_echo_hidden_logout_link');
/**
 * Custom redirect function for WooCommerce - helps to redirect to "Add Child to Class" page during the booking flow.
 *
 * @param $redirect
 * @param $user
 *
 * @return false|string
 */
function gfr_woocommerce_login_redirect( $redirect, $user = null ) {

    // is the "redirect_to" query string param is set ?
    if(strpos($redirect, "redirect_to") != false) {
        $url_to_return = substr($redirect, strpos($redirect, "=") +1);

        return 'https://' . urldecode($url_to_return);

    } else {
        // run the standard behavior
        return $redirect;
    }

}

add_filter( 'woocommerce_login_redirect', 'gfr_woocommerce_login_redirect' );


 /*
 *
 * Customising the Checkout page:
 * 1 - removing Company field
 * 2 - removing County field
 *
 */

 add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

function custom_override_checkout_fields( $fields ) {
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_state']);
    return $fields;
}


/*
 *
 * Function to do:
 * 1. Change the order status to "Completed" Automatically when the payment has been processed correctly
 *
 */

add_action( 'woocommerce_thankyou', 'wc_auto_complete_paid_order', 20, 1 );

function wc_auto_complete_paid_order( $order_id ) {
    if ( ! $order_id )
        return;

    // Get an instance of the WC_Product object
    $order = wc_get_order( $order_id );

    // No updated status for orders delivered with Bank wire, Cash on delivery and Cheque payment methods.
    if ( in_array( $order->get_payment_method(), array( 'bacs', 'cod', 'cheque', '' ) ) ) {
        return;
    // Updated status to "completed" for paid Orders with all others payment methods
    } else {
        $order->update_status( 'completed' );


    }
}

/*
 *
 * Customise the Cart page - remove the image thumbnail
 *
 *
 */

add_filter( 'woocommerce_cart_item_thumbnail', '__return_false' );


/*
 *
 * Function to remove all the unnecessary fields on the WP Admin > User screen
 *
 */
function remove_personal_options(){
    echo '<script type="text/javascript">jQuery(document).ready(function(jQuery) {

    jQuery(\'form#your-profile > h2:first\').remove(); // remove the "Personal Options" title
    jQuery(\'form#your-profile tr.user-rich-editing-wrap\').remove(); // remove the "Visual Editor" field
    jQuery(\'form#your-profile tr.user-admin-color-wrap\').remove(); // remove the "Admin Color Scheme" field
    jQuery(\'form#your-profile tr.user-comment-shortcuts-wrap\').remove(); // remove the "Keyboard Shortcuts" field
    jQuery(\'form#your-profile tr.user-syntax-highlighting-wrap\').remove(); // remove the "Keyboard Shortcuts" field
    jQuery(\'form#your-profile tr.user-admin-bar-front-wrap\').remove(); // remove the "Toolbar" field
    jQuery(\'form#your-profile tr.user-language-wrap\').remove(); // remove the "Language" field
    jQuery(\'table.form-table tr.user-url-wrap\').remove();// remove the "Website" field in the "Contact Info" section
    jQuery(\'table.form-table tr.user-facebook-wrap\').remove();// remove the "Website" field in the "Contact Info" section
    jQuery(\'table.form-table tr.user-instagram-wrap\').remove();// remove the "Website" field in the "Contact Info" section
    jQuery(\'table.form-table tr.user-linkedin-wrap\').remove();// remove the "Website" field in the "Contact Info" section
    jQuery(\'table.form-table tr.user-myspace-wrap\').remove();// remove the "Website" field in the "Contact Info" section
    jQuery(\'table.form-table tr.user-pinterest-wrap\').remove();// remove the "Website" field in the "Contact Info" section
    jQuery(\'table.form-table tr.user-soundcloud-wrap\').remove();// remove the "Website" field in the "Contact Info" section
    jQuery(\'table.form-table tr.user-tumblr-wrap\').remove();// remove the "Website" field in the "Contact Info" section
    jQuery(\'table.form-table tr.user-twitter-wrap\').remove();// remove the "Website" field in the "Contact Info" section
    jQuery(\'table.form-table tr.user-youtube-wrap\').remove();// remove the "Website" field in the "Contact Info" section
    jQuery(\'table.form-table tr.user-wikipedia-wrap\').remove();// remove the "Website" field in the "Contact Info" section
    });</script>';

}

add_action('admin_head','remove_personal_options');

/*
 *
 * Functions to rename the "User bio" field and replace it by Tarka Notes
 *
 */
add_filter( 'gettext', 'gfr_change_user_bio_label', 10, 2 );
function gfr_change_user_bio_label( $translation, $original )
{
    if ( 'Biographical Info' == $original ) {
        return 'Tarka Notes';
    }
    return $translation;
}


/* Children Information */
function additional_profile_field( $profileuser ) { ?>
  <h3>Child Information</h3>
  <?php
    $user_id = $profileuser->ID;
    $all_meta_for_user = get_user_meta( $user_id );
    $children_info = isset($all_meta_for_user['child-detail']) ? $all_meta_for_user['child-detail'][0] : null;
    if($children_info) {
      $child_info_unserialize = unserialize($children_info);
      $child_keys = array_keys($child_info_unserialize);
    } else {
      $child_info_unserialize = [];
      $child_keys = [];
    }
    // init loop
    $i = 0;
    while($i < 8) {
        $key = array_key_exists($i, $child_keys) ? $child_keys[$i] : $i;

        $firstname = !empty($child_info_unserialize[$key]['firstname']) ? $child_info_unserialize[$key]['firstname'] : '';
        $lastname = !empty($child_info_unserialize[$key]['lastname']) ? $child_info_unserialize[$key]['lastname'] : '';
        $dob = !empty($child_info_unserialize[$key]['dob']) ? $child_info_unserialize[$key]['dob'] : '';

        ?>
        <table class="form-table <?php if ($i == 0) {
            echo "tableshow";
        } elseif ($i == 1) {
            echo "tablehideone";
        } elseif ($i == 2) {
            echo "tablehidetwo";
        } elseif ($i == 3) {
            echo "tablehidethree";
        } elseif ($i == 4) {
            echo "tablehidefour";
        } elseif ($i == 5) {
            echo "tablehidefive";
        } elseif ($i == 6) {
            echo "tablehidesix";
        } elseif ($i == 7) {
            echo "tablehideseven";
        } ?>">
        <?php if ($i == 0) { ?>
          <tr>
              <th><label for="birth-date-day">First Name</label></th>
              <th><label for="birth-date-day">Last Name</label></th>
              <th><label for="birth-date-day">Birth date</label></th>
              <th><label for="birth-date-day">Last attendance date</label></th>
          </tr>
          <?php } ?>
          <tr>
              <td>
                  <input type="text" name="child-detail[<?php echo $key ?>][firstname]" value="<?php echo $firstname; ?>">
              </td>
              <td>
                  <input type="text" name="child-detail[<?php echo $key ?>][lastname]" value="<?php echo $lastname; ?>">
              </td>
              <td>
                  <input type="text" name="child-detail[<?php echo $key ?>][dob]" value="<?php echo $dob; ?>">
              </td>
              <td>
                <?php echo get_latest_attendance_date($user_id, $key); ?>
              </td>
          </tr>
        </table>
  <?php $i++;
    }?>
    <input type="button" name="add_more" id="add_more" value="Show More">
<?php }
add_action( 'show_user_profile', 'additional_profile_field' );
add_action( 'edit_user_profile', 'additional_profile_field' );

function save_additional_profile_field( $user_id ) {

    if ( ! current_user_can( 'edit_user', $user_id ) ) {
     return false;
    }

    if ( empty( $_POST['child-detail'] ) ) {
     return false;
    }

    update_user_meta( $user_id, 'child-detail', $_POST['child-detail'] );
}

add_action( 'personal_options_update', 'save_additional_profile_field' );
add_action( 'edit_user_profile_update', 'save_additional_profile_field' );

function get_next_weeks_class_id( $this_weeks_class_id ) {
  $product = wc_get_product( $this_weeks_class_id );
  $attributes = $product->get_attributes();

  $class_name = $attributes[ 'pa_class-name' ]->get_options()[0];
  $location = $attributes[ 'pa_location' ]->get_options()[0];
  $start_time = $attributes[ 'pa_start-time' ]->get_options()[0];
  $time = strtotime("+7 day", strtotime( $product->get_attribute( 'class-date' ) ) );
  $class_date = date('Y-m-d', $time);

  $products = get_posts([
    'post_type' => 'product',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'tax_query' => [
      'relation' => 'AND',
      [
        'taxonomy' => 'pa_class-name',
        'terms' =>  $class_name,
      ],
      [
        'taxonomy' => 'pa_class-date',
        'field' => 'name',
        'terms' => $class_date,
      ],
      [
        'taxonomy' => 'pa_start-time',
        'terms' => $start_time,
      ],
      [
        'taxonomy' => 'pa_location',
        'terms' => $location,
      ],
    ]
  ]);

  return count( $products ) == 0 ? -1 : $products[0]->ID;
}

function go_to_next_week_action() {
	$class_id = intval( $_POST['class_id'] );
  echo get_next_weeks_class_id( $class_id );
	wp_die();
}
add_action( 'wp_ajax_go_to_next_week_action', 'go_to_next_week_action' );
add_action( 'wp_ajax_nopriv_go_to_next_week_action', 'go_to_next_week_action' );


function get_parents_emails_action() {
  $emails = [];

  foreach( $_POST[ "class_ids" ] as $class_id )
    foreach( get_parents_emails( $class_id ) as $single_email )
      array_push( $emails, $single_email );

	echo json_encode( $emails );

	wp_die(); // this is required to terminate immediately and return a proper response
}
add_action( 'wp_ajax_get_parents_emails_action', 'get_parents_emails_action' );
add_action( 'wp_ajax_nopriv_get_parents_emails_action', 'get_parents_emails_action' );

function get_parents_emails( $class_id ) {
  $emails = [];
  foreach ( get_orders_ids_by_product_id( $class_id ) as $order_id ) {
    $order = wc_get_order( $order_id );
    array_push( $emails, $order->get_billing_email() );
  }
  return $emails;
}

add_action( 'wp_ajax_join_waitlist_action', 'join_waitlist_action' );
add_action( 'wp_ajax_nopriv_join_waitlist_action', 'join_waitlist_action' );

function join_waitlist_action() {
  echo wcwl_add_user_to_waitlist( $_POST['email'], $_POST['product_id'], '' );
	wp_die();
}


function get_waitlist_people() {
  global $wpdb;
  $class_waitlists = $wpdb->get_results("SELECT * FROM $wpdb->postmeta WHERE meta_key = 'woocommerce_waitlist'");

  $row_id = 1;

  $result = [];

  foreach ( $class_waitlists as $single_class_waitlist_meta ) {
    $class_id = $single_class_waitlist_meta->post_id;

    foreach ( unserialize( $single_class_waitlist_meta->meta_value) as $user_id => $join_date ) {
      $user = get_user_by( 'id', $user_id );
      $product = wc_get_product( $class_id );
      array_push( $result, [
        'ID'        => $row_id++,
        'parent_email'     => $user->user_email,
        'class_name'    => $product->get_attribute( 'pa_class-name' ),
        'class_date'    => $product->get_attribute( 'pa_class-date' ),
        // 'class_id'    => $single_class_waitlist_meta->post_id,
        'stock'  => gfr_admin_get_bookings( $class_id ),
        'join_date' => date('d M Y', $join_date)
      ] );
    }

  }

  return $result;
}


/**
 * This section deals with the "Nanny" field. The aim is to create and manage a new field that lets parents
 * add the name of their nanny so the name can be checked at pick up time by the instructor at the end of the classes
 *
 */

 // 1. Create the field and add it to the user profile

add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function extra_user_profile_fields( $user ) { ?>
    <h3><?php _e("Please enter the name of your approved Carer", "blank"); ?></h3>

    <table class="form-table">
    <tr>
        <th><label for="nanny"><?php _e("Approved Carer name"); ?></label></th>
        <td>
            <input type="text" name="nanny" id="nanny" value="<?php echo esc_attr( get_the_author_meta( 'nanny', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter the first name and last name of your child minder."); ?></span>
        </td>
    </tr>
    </table>
<?php }

// 2. Code to save the field to the DB
add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );

function save_extra_user_profile_fields( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) ) {
        return false;
    }
    update_user_meta( $user_id, 'nanny', $_POST['nanny'] );
}

// 3. Register the new "My Account - Child Minder" URL that lets users do this in the front end

add_action( 'init', 'my_account_add_nanny_url' );

function my_account_add_nanny_url() {
  // creates a /my-account/child-minder URL
  add_rewrite_endpoint( 'approved-carer', EP_ROOT | EP_PAGES );
}

// 4. Add the actual content shown at that new created URL
add_action( 'woocommerce_account_approved-carer_endpoint', 'child_minder_endpoint_content' );

function child_minder_endpoint_content() {
    get_template_part('./gfr-class-plugin/templates/my-account/page_child_child_minder_edit');
}

// 5. Now saves the info submittted by Ajax on the frontend
// 2. Code to save the field to the DB
add_action( 'wp_ajax_my_account_save_nanny_detail', 'gfr_fe_save_extra_user_profile_fields' );

function gfr_fe_save_extra_user_profile_fields() {

  $user_id = wp_get_current_user()->ID;

  if ( 0 ==  $user_id) {
    $data = array(
      'error'       => true,
      'message' => "Please log in"
    );
    // return success or failure to the page
    wp_send_json( $data );
    wp_die();
    }
    if(!$_POST['nanny'] || $_POST['nanny'] == '') {
      $data = array(
        'error'       => true,
        'message' => "Please provide all mandatory information"
      );
      // return success or failure to the page
      wp_send_json( $data );
      wp_die();
    }
    update_user_meta( $user_id, 'nanny', $_POST['nanny'] );
}

function var_error_log( $object=null ){
    ob_start();                    // start buffer capture
    var_dump( $object );           // dump the values
    $contents = ob_get_contents(); // put the buffer into a variable
    ob_end_clean();                // end capture
    error_log( $contents );        // log contents of the result of var_dump( $object )
}

/* The Instagram feed plugin bloats up regularly and need to be "purged"
This can be done manually in the WP Admin but that's annoying to have to do all the time
Thus, I have added an action to the PW CRON called "instagram_feed_clear_size_table" that runs every 3 days
This hook is then linked to the plugin function that does the cache clear
*/
add_action( 'instagram_feed_clear_size_table', 'gfr_insta_cache_clear' );

function gfr_insta_cache_clear() {
  global $sb_instagram_posts_manager;
	$sb_instagram_posts_manager->delete_all_sbi_instagram_posts();
}


add_action( 'woocommerce_email', 'unhook_those_pesky_emails' );
function unhook_those_pesky_emails( $email_class ) {
	remove_action( 'woocommerce_product_on_backorder_notification', array( $email_class, 'backorder' ) );
}

add_action( 'save_post_shop_order', 'reduce_stock_for_pending_order' );
function reduce_stock_for_pending_order( $order_id ) {
  $order = wc_get_order( $order_id );
  if( 'pending' === $order->get_status() ) {
    wc_maybe_reduce_stock_levels( $order_id );
  }
}

/**
 * This section deals with exporting the Child info in a cleaner way
 */
add_filter( 'acui_export_data', 'prefix_acui_export_data', 10, 5 );
function prefix_acui_export_data( $row, $user, $datetime_format, $columns, $order_fields_alphabetically ) { 
	
  $child_detail = maybe_unserialize($row['child-detail']);
  if ($child_detail) {
    $csv_friendly_data = "";
    for ($i=0; $i < sizeof($child_detail); $i++) {
        $child = $child_detail[$i];
        if ((isset($child['firstname']) && $child['firstname']) || (isset($child['lastname']) && $child['lastname'])) {
            $csv_friendly_data .= $child['firstname'] . "|" . $child['lastname'] . "|" . $child['dob'] . "|" . $child['notes'] . ";";
        } else {
          // we sometimes have rows with empty child detail which we don't want to export
          // do nothing
        }
    }
    $row['child-detail'] = $csv_friendly_data;
  }

	return $row;
}

